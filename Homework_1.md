```python
import pandas as pd
```


```python
#read the file 
bus = pd.read_csv('Potentail_Bust_Stops.csv')
```


```python
#examine the first few rows 
bus.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Street_One</th>
      <th>Street_Two</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MISSION ST</td>
      <td>ITALY AVE</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MISSION ST</td>
      <td>NEW MONTGOMERY ST</td>
    </tr>
    <tr>
      <th>2</th>
      <td>MISSION ST</td>
      <td>01ST ST</td>
    </tr>
    <tr>
      <th>3</th>
      <td>MISSION ST</td>
      <td>20TH ST</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MISSION ST</td>
      <td>FREMONT ST</td>
    </tr>
  </tbody>
</table>
</div>




```python
type(bus['Street_One'])
```




    pandas.core.series.Series




```python
#combine the street_one and street_two to make intersection column
bus['intersection'] = bus.Street_One + ' & ' + bus.Street_Two + ', ' + 'San Franciso, CA, USA'
```


```python
bus.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Street_One</th>
      <th>Street_Two</th>
      <th>intersection</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MISSION ST</td>
      <td>ITALY AVE</td>
      <td>MISSION ST &amp; ITALY AVE, San Franciso, CA, USA</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MISSION ST</td>
      <td>NEW MONTGOMERY ST</td>
      <td>MISSION ST &amp; NEW MONTGOMERY ST, San Franciso, ...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>MISSION ST</td>
      <td>01ST ST</td>
      <td>MISSION ST &amp; 01ST ST, San Franciso, CA, USA</td>
    </tr>
    <tr>
      <th>3</th>
      <td>MISSION ST</td>
      <td>20TH ST</td>
      <td>MISSION ST &amp; 20TH ST, San Franciso, CA, USA</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MISSION ST</td>
      <td>FREMONT ST</td>
      <td>MISSION ST &amp; FREMONT ST, San Franciso, CA, USA</td>
    </tr>
  </tbody>
</table>
</div>




```python
#check to see how many rows and columns in the dataset
bus.shape
```




    (119, 3)




```python
#check the dataset information. There is no missing data.
bus.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 119 entries, 0 to 118
    Data columns (total 3 columns):
    Street_One      119 non-null object
    Street_Two      119 non-null object
    intersection    119 non-null object
    dtypes: object(3)
    memory usage: 2.9+ KB
    


```python
 # using Google map api to find the coordinates of intersection. Import multiple google API keys for enhacing the performance. 
from geopy.geocoders import GoogleV3
import os, random
geo_keys = ["API key1, API key2,..."] # put in your API keys as strings
g = GoogleV3(api_key=random.choice(geo_keys), timeout = 3) 
 # set timeout to increase chance of receiving data from server
```


```python
#test the geocode with one address
location_test = g.geocode("MISSION ST & BEALE ST",
          components={"city": "San Francisco", "state": "CA", "country": "USA"},
          exactly_one=True)

location_test
```




    Location(Mission St & Beale St, San Francisco, CA 94105, USA, (37.7911592, -122.3958262, 0.0))




```python
#the below code is used to find the latitudes and longtitudes of each intersection

loc_latitude = []
loc_longtitude = []
loc_address = []

for intersection in bus.intersection:
    try:
        inputAddress = intersection
        location = g.geocode(inputAddress, timeout=15)
        loc_latitude.append(location.latitude)
        loc_longtitude.append(location.longitude)
        loc_address.append(inputAddress)
    except:
        loc_latitude.append('None')
        loc_longtitude.append('None')
        loc_address.append(inputAddress)

bus_geocodes = pd.DataFrame({'Street_One':bus.Street_One, 'Street_Two':bus.Street_Two, 'intersection_address':loc_address, 
                             'latitude':loc_latitude, 'longtitude':loc_longtitude})


```


```python
#check the new dataset. It seems that it was able to find all the coordinates. If not, either re-run or make a subset of missing coordinates and run on it.
bus_geocodes
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Street_One</th>
      <th>Street_Two</th>
      <th>intersection_address</th>
      <th>latitude</th>
      <th>longtitude</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MISSION ST</td>
      <td>ITALY AVE</td>
      <td>MISSION ST &amp; ITALY AVE, San Franciso, CA, USA</td>
      <td>37.718478</td>
      <td>-122.439536</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MISSION ST</td>
      <td>NEW MONTGOMERY ST</td>
      <td>MISSION ST &amp; NEW MONTGOMERY ST, San Franciso, ...</td>
      <td>37.787456</td>
      <td>-122.400523</td>
    </tr>
    <tr>
      <th>2</th>
      <td>MISSION ST</td>
      <td>01ST ST</td>
      <td>MISSION ST &amp; 01ST ST, San Franciso, CA, USA</td>
      <td>37.789954</td>
      <td>-122.397514</td>
    </tr>
    <tr>
      <th>3</th>
      <td>MISSION ST</td>
      <td>20TH ST</td>
      <td>MISSION ST &amp; 20TH ST, San Franciso, CA, USA</td>
      <td>37.758843</td>
      <td>-122.418998</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MISSION ST</td>
      <td>FREMONT ST</td>
      <td>MISSION ST &amp; FREMONT ST, San Franciso, CA, USA</td>
      <td>37.790455</td>
      <td>-122.396726</td>
    </tr>
    <tr>
      <th>5</th>
      <td>MISSION ST</td>
      <td>13TH ST</td>
      <td>MISSION ST &amp; 13TH ST, San Franciso, CA, USA</td>
      <td>37.769954</td>
      <td>-122.419962</td>
    </tr>
    <tr>
      <th>6</th>
      <td>MISSION ST</td>
      <td>ERIE ST</td>
      <td>MISSION ST &amp; ERIE ST, San Franciso, CA, USA</td>
      <td>37.769063</td>
      <td>-122.420072</td>
    </tr>
    <tr>
      <th>7</th>
      <td>MISSION ST</td>
      <td>BEALE ST</td>
      <td>MISSION ST &amp; BEALE ST, San Franciso, CA, USA</td>
      <td>37.791159</td>
      <td>-122.395826</td>
    </tr>
    <tr>
      <th>8</th>
      <td>MISSION ST</td>
      <td>FAIR AVE</td>
      <td>MISSION ST &amp; FAIR AVE, San Franciso, CA, USA</td>
      <td>37.745604</td>
      <td>-122.419898</td>
    </tr>
    <tr>
      <th>9</th>
      <td>MISSION ST</td>
      <td>SAINT MARYS AVE</td>
      <td>MISSION ST &amp; SAINT MARYS AVE, San Franciso, CA...</td>
      <td>37.733953</td>
      <td>-122.426142</td>
    </tr>
    <tr>
      <th>10</th>
      <td>MISSION ST</td>
      <td>SENECA AVE</td>
      <td>MISSION ST &amp; SENECA AVE, San Franciso, CA, USA</td>
      <td>37.717675</td>
      <td>-122.440149</td>
    </tr>
    <tr>
      <th>11</th>
      <td>MISSION ST</td>
      <td>ANTHONY ST</td>
      <td>MISSION ST &amp; ANTHONY ST, San Franciso, CA, USA</td>
      <td>37.788374</td>
      <td>-122.399362</td>
    </tr>
    <tr>
      <th>12</th>
      <td>MISSION ST</td>
      <td>JESSIE EAST ST</td>
      <td>MISSION ST &amp; JESSIE EAST ST, San Franciso, CA,...</td>
      <td>37.783090</td>
      <td>-122.406038</td>
    </tr>
    <tr>
      <th>13</th>
      <td>MISSION ST</td>
      <td>SILVER AVE</td>
      <td>MISSION ST &amp; SILVER AVE, San Franciso, CA, USA</td>
      <td>37.728749</td>
      <td>-122.431320</td>
    </tr>
    <tr>
      <th>14</th>
      <td>MISSION ST</td>
      <td>MOUNT VERNON AVE</td>
      <td>MISSION ST &amp; MOUNT VERNON AVE, San Franciso, C...</td>
      <td>37.714607</td>
      <td>-122.442670</td>
    </tr>
    <tr>
      <th>15</th>
      <td>MISSION ST</td>
      <td>30TH ST</td>
      <td>MISSION ST &amp; 30TH ST, San Franciso, CA, USA</td>
      <td>37.742387</td>
      <td>-122.421982</td>
    </tr>
    <tr>
      <th>16</th>
      <td>MISSION ST</td>
      <td>MAIN ST</td>
      <td>MISSION ST &amp; MAIN ST, San Franciso, CA, USA</td>
      <td>37.791848</td>
      <td>-122.394955</td>
    </tr>
    <tr>
      <th>17</th>
      <td>MISSION ST</td>
      <td>TRUMBULL ST</td>
      <td>MISSION ST &amp; TRUMBULL ST, San Franciso, CA, USA</td>
      <td>37.730758</td>
      <td>-122.429242</td>
    </tr>
    <tr>
      <th>18</th>
      <td>MISSION ST</td>
      <td>15TH ST</td>
      <td>MISSION ST &amp; 15TH ST, San Franciso, CA, USA</td>
      <td>37.767138</td>
      <td>-122.420603</td>
    </tr>
    <tr>
      <th>19</th>
      <td>MISSION ST</td>
      <td>KINGSTON ST</td>
      <td>MISSION ST &amp; KINGSTON ST, San Franciso, CA, USA</td>
      <td>37.741751</td>
      <td>-122.422392</td>
    </tr>
    <tr>
      <th>20</th>
      <td>MISSION ST</td>
      <td>TINGLEY ST</td>
      <td>MISSION ST &amp; TINGLEY ST, San Franciso, CA, USA</td>
      <td>37.728252</td>
      <td>-122.431806</td>
    </tr>
    <tr>
      <th>21</th>
      <td>MISSION ST</td>
      <td>THERESA ST</td>
      <td>MISSION ST &amp; THERESA ST, San Franciso, CA, USA</td>
      <td>37.727717</td>
      <td>-122.432404</td>
    </tr>
    <tr>
      <th>22</th>
      <td>MISSION ST</td>
      <td>PARK ST</td>
      <td>MISSION ST &amp; PARK ST, San Franciso, CA, USA</td>
      <td>37.736750</td>
      <td>-122.424248</td>
    </tr>
    <tr>
      <th>23</th>
      <td>MISSION ST</td>
      <td>OLIVER ST</td>
      <td>MISSION ST &amp; OLIVER ST, San Franciso, CA, USA</td>
      <td>37.709556</td>
      <td>-122.450519</td>
    </tr>
    <tr>
      <th>24</th>
      <td>MISSION ST</td>
      <td>STEUART ST</td>
      <td>MISSION ST &amp; STEUART ST, San Franciso, CA, USA</td>
      <td>37.793232</td>
      <td>-122.393205</td>
    </tr>
    <tr>
      <th>25</th>
      <td>MISSION ST</td>
      <td>HURON AVE</td>
      <td>MISSION ST &amp; HURON AVE, San Franciso, CA, USA</td>
      <td>37.708305</td>
      <td>-122.454194</td>
    </tr>
    <tr>
      <th>26</th>
      <td>MISSION ST</td>
      <td>SYCAMORE ST</td>
      <td>MISSION ST &amp; SYCAMORE ST, San Franciso, CA, USA</td>
      <td>37.762714</td>
      <td>-122.419471</td>
    </tr>
    <tr>
      <th>27</th>
      <td>MISSION ST</td>
      <td>02ND ST</td>
      <td>MISSION ST &amp; 02ND ST, San Franciso, CA, USA</td>
      <td>37.787836</td>
      <td>-122.400201</td>
    </tr>
    <tr>
      <th>28</th>
      <td>MISSION ST</td>
      <td>MINT ST</td>
      <td>MISSION ST &amp; MINT ST, San Franciso, CA, USA</td>
      <td>37.782212</td>
      <td>-122.407166</td>
    </tr>
    <tr>
      <th>29</th>
      <td>MISSION ST</td>
      <td>GENEVA AVE</td>
      <td>MISSION ST &amp; GENEVA AVE, San Franciso, CA, USA</td>
      <td>37.716407</td>
      <td>-122.441087</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>89</th>
      <td>MISSION ST</td>
      <td>07TH ST</td>
      <td>MISSION ST &amp; 07TH ST, San Franciso, CA, USA</td>
      <td>37.778990</td>
      <td>-122.412203</td>
    </tr>
    <tr>
      <th>90</th>
      <td>MISSION ST</td>
      <td>NORTON ST</td>
      <td>MISSION ST &amp; NORTON ST, San Franciso, CA, USA</td>
      <td>37.724310</td>
      <td>-122.435190</td>
    </tr>
    <tr>
      <th>91</th>
      <td>MISSION ST</td>
      <td>RUTH ST</td>
      <td>MISSION ST &amp; RUTH ST, San Franciso, CA, USA</td>
      <td>37.723050</td>
      <td>-122.436068</td>
    </tr>
    <tr>
      <th>92</th>
      <td>MISSION ST</td>
      <td>BRAZIL AVE</td>
      <td>MISSION ST &amp; BRAZIL AVE, San Franciso, CA, USA</td>
      <td>37.724688</td>
      <td>-122.434824</td>
    </tr>
    <tr>
      <th>93</th>
      <td>MISSION ST</td>
      <td>GUTTENBERG ST</td>
      <td>MISSION ST &amp; GUTTENBERG ST, San Franciso, CA, USA</td>
      <td>37.712783</td>
      <td>-122.444602</td>
    </tr>
    <tr>
      <th>94</th>
      <td>MISSION ST</td>
      <td>23RD ST</td>
      <td>MISSION ST &amp; 23RD ST, San Franciso, CA, USA</td>
      <td>37.753845</td>
      <td>-122.418619</td>
    </tr>
    <tr>
      <th>95</th>
      <td>MISSION ST</td>
      <td>RANDALL ST</td>
      <td>MISSION ST &amp; RANDALL ST, San Franciso, CA, USA</td>
      <td>37.739779</td>
      <td>-122.423676</td>
    </tr>
    <tr>
      <th>96</th>
      <td>MISSION ST</td>
      <td>PERSIA AVE</td>
      <td>MISSION ST &amp; PERSIA AVE, San Franciso, CA, USA</td>
      <td>37.723153</td>
      <td>-122.436002</td>
    </tr>
    <tr>
      <th>97</th>
      <td>MISSION ST</td>
      <td>16TH ST</td>
      <td>MISSION ST &amp; 16TH ST, San Franciso, CA, USA</td>
      <td>37.765057</td>
      <td>-122.419698</td>
    </tr>
    <tr>
      <th>98</th>
      <td>MISSION ST</td>
      <td>YERBA BUENA LN</td>
      <td>MISSION ST &amp; YERBA BUENA LN, San Franciso, CA,...</td>
      <td>37.785138</td>
      <td>-122.403506</td>
    </tr>
    <tr>
      <th>99</th>
      <td>MISSION ST</td>
      <td>LAURA ST</td>
      <td>MISSION ST &amp; LAURA ST, San Franciso, CA, USA</td>
      <td>37.709688</td>
      <td>-122.450124</td>
    </tr>
    <tr>
      <th>100</th>
      <td>MISSION ST</td>
      <td>18TH ST</td>
      <td>MISSION ST &amp; 18TH ST, San Franciso, CA, USA</td>
      <td>37.761877</td>
      <td>-122.419797</td>
    </tr>
    <tr>
      <th>101</th>
      <td>MISSION ST</td>
      <td>LEO ST</td>
      <td>MISSION ST &amp; LEO ST, San Franciso, CA, USA</td>
      <td>37.722464</td>
      <td>-122.436509</td>
    </tr>
    <tr>
      <th>102</th>
      <td>MISSION ST</td>
      <td>POWERS AVE</td>
      <td>MISSION ST &amp; POWERS AVE, San Franciso, CA, USA</td>
      <td>37.746186</td>
      <td>-122.419524</td>
    </tr>
    <tr>
      <th>103</th>
      <td>MISSION ST</td>
      <td>SANTA ROSA AVE</td>
      <td>MISSION ST &amp; SANTA ROSA AVE, San Franciso, CA,...</td>
      <td>37.725773</td>
      <td>-122.434004</td>
    </tr>
    <tr>
      <th>104</th>
      <td>MISSION ST</td>
      <td>FLORENTINE AVE</td>
      <td>MISSION ST &amp; FLORENTINE AVE, San Franciso, CA,...</td>
      <td>37.713386</td>
      <td>-122.443966</td>
    </tr>
    <tr>
      <th>105</th>
      <td>MISSION ST</td>
      <td>FOOTE AVE</td>
      <td>MISSION ST &amp; FOOTE AVE, San Franciso, CA, USA</td>
      <td>37.712634</td>
      <td>-122.444770</td>
    </tr>
    <tr>
      <th>106</th>
      <td>MISSION ST</td>
      <td>BOSWORTH ST</td>
      <td>MISSION ST &amp; BOSWORTH ST, San Franciso, CA, USA</td>
      <td>37.733685</td>
      <td>-122.426396</td>
    </tr>
    <tr>
      <th>107</th>
      <td>MISSION ST</td>
      <td>EXCELSIOR AVE</td>
      <td>MISSION ST &amp; EXCELSIOR AVE, San Franciso, CA, USA</td>
      <td>37.726404</td>
      <td>-122.433412</td>
    </tr>
    <tr>
      <th>108</th>
      <td>MISSION ST</td>
      <td>WASHBURN ST</td>
      <td>MISSION ST &amp; WASHBURN ST, San Franciso, CA, USA</td>
      <td>37.775900</td>
      <td>-122.415153</td>
    </tr>
    <tr>
      <th>109</th>
      <td>MISSION ST</td>
      <td>MORSE ST</td>
      <td>MISSION ST &amp; MORSE ST, San Franciso, CA, USA</td>
      <td>37.710424</td>
      <td>-122.448320</td>
    </tr>
    <tr>
      <th>110</th>
      <td>MISSION ST</td>
      <td>ANGELOS ALY</td>
      <td>MISSION ST &amp; ANGELOS ALY, San Franciso, CA, USA</td>
      <td>37.778130</td>
      <td>-122.412328</td>
    </tr>
    <tr>
      <th>111</th>
      <td>MISSION ST</td>
      <td>BROOK ST</td>
      <td>MISSION ST &amp; BROOK ST, San Franciso, CA, USA</td>
      <td>37.740649</td>
      <td>-122.423105</td>
    </tr>
    <tr>
      <th>112</th>
      <td>MISSION ST</td>
      <td>09TH ST</td>
      <td>MISSION ST &amp; 09TH ST, San Franciso, CA, USA</td>
      <td>37.776031</td>
      <td>-122.414743</td>
    </tr>
    <tr>
      <th>113</th>
      <td>MISSION ST</td>
      <td>OTTAWA AVE</td>
      <td>MISSION ST &amp; OTTAWA AVE, San Franciso, CA, USA</td>
      <td>37.713605</td>
      <td>-122.443736</td>
    </tr>
    <tr>
      <th>114</th>
      <td>MISSION ST</td>
      <td>NIAGARA AVE</td>
      <td>MISSION ST &amp; NIAGARA AVE, San Franciso, CA, USA</td>
      <td>37.715587</td>
      <td>-122.441727</td>
    </tr>
    <tr>
      <th>115</th>
      <td>MISSION ST</td>
      <td>ACTON ST</td>
      <td>MISSION ST &amp; ACTON ST, San Franciso, CA, USA</td>
      <td>37.708785</td>
      <td>-122.452769</td>
    </tr>
    <tr>
      <th>116</th>
      <td>MISSION ST</td>
      <td>05TH ST</td>
      <td>MISSION ST &amp; 05TH ST, San Franciso, CA, USA</td>
      <td>37.782823</td>
      <td>-122.406759</td>
    </tr>
    <tr>
      <th>117</th>
      <td>MISSION ST</td>
      <td>24TH ST</td>
      <td>MISSION ST &amp; 24TH ST, San Franciso, CA, USA</td>
      <td>37.752246</td>
      <td>-122.418462</td>
    </tr>
    <tr>
      <th>118</th>
      <td>MISSION ST</td>
      <td>GRACE ST</td>
      <td>MISSION ST &amp; GRACE ST, San Franciso, CA, USA</td>
      <td>37.775610</td>
      <td>-122.415518</td>
    </tr>
  </tbody>
</table>
<p>119 rows × 5 columns</p>
</div>




```python
#put bus_geocodes dataset into a dataframe
df = pd.DataFrame(bus_geocodes)
```


```python
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Street_One</th>
      <th>Street_Two</th>
      <th>intersection_address</th>
      <th>latitude</th>
      <th>longtitude</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MISSION ST</td>
      <td>ITALY AVE</td>
      <td>MISSION ST &amp; ITALY AVE, San Franciso, CA, USA</td>
      <td>37.718478</td>
      <td>-122.439536</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MISSION ST</td>
      <td>NEW MONTGOMERY ST</td>
      <td>MISSION ST &amp; NEW MONTGOMERY ST, San Franciso, ...</td>
      <td>37.787456</td>
      <td>-122.400523</td>
    </tr>
    <tr>
      <th>2</th>
      <td>MISSION ST</td>
      <td>01ST ST</td>
      <td>MISSION ST &amp; 01ST ST, San Franciso, CA, USA</td>
      <td>37.789954</td>
      <td>-122.397514</td>
    </tr>
    <tr>
      <th>3</th>
      <td>MISSION ST</td>
      <td>20TH ST</td>
      <td>MISSION ST &amp; 20TH ST, San Franciso, CA, USA</td>
      <td>37.758843</td>
      <td>-122.418998</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MISSION ST</td>
      <td>FREMONT ST</td>
      <td>MISSION ST &amp; FREMONT ST, San Franciso, CA, USA</td>
      <td>37.790455</td>
      <td>-122.396726</td>
    </tr>
    <tr>
      <th>5</th>
      <td>MISSION ST</td>
      <td>13TH ST</td>
      <td>MISSION ST &amp; 13TH ST, San Franciso, CA, USA</td>
      <td>37.769954</td>
      <td>-122.419962</td>
    </tr>
    <tr>
      <th>6</th>
      <td>MISSION ST</td>
      <td>ERIE ST</td>
      <td>MISSION ST &amp; ERIE ST, San Franciso, CA, USA</td>
      <td>37.769063</td>
      <td>-122.420072</td>
    </tr>
    <tr>
      <th>7</th>
      <td>MISSION ST</td>
      <td>BEALE ST</td>
      <td>MISSION ST &amp; BEALE ST, San Franciso, CA, USA</td>
      <td>37.791159</td>
      <td>-122.395826</td>
    </tr>
    <tr>
      <th>8</th>
      <td>MISSION ST</td>
      <td>FAIR AVE</td>
      <td>MISSION ST &amp; FAIR AVE, San Franciso, CA, USA</td>
      <td>37.745604</td>
      <td>-122.419898</td>
    </tr>
    <tr>
      <th>9</th>
      <td>MISSION ST</td>
      <td>SAINT MARYS AVE</td>
      <td>MISSION ST &amp; SAINT MARYS AVE, San Franciso, CA...</td>
      <td>37.733953</td>
      <td>-122.426142</td>
    </tr>
    <tr>
      <th>10</th>
      <td>MISSION ST</td>
      <td>SENECA AVE</td>
      <td>MISSION ST &amp; SENECA AVE, San Franciso, CA, USA</td>
      <td>37.717675</td>
      <td>-122.440149</td>
    </tr>
    <tr>
      <th>11</th>
      <td>MISSION ST</td>
      <td>ANTHONY ST</td>
      <td>MISSION ST &amp; ANTHONY ST, San Franciso, CA, USA</td>
      <td>37.788374</td>
      <td>-122.399362</td>
    </tr>
    <tr>
      <th>12</th>
      <td>MISSION ST</td>
      <td>JESSIE EAST ST</td>
      <td>MISSION ST &amp; JESSIE EAST ST, San Franciso, CA,...</td>
      <td>37.783090</td>
      <td>-122.406038</td>
    </tr>
    <tr>
      <th>13</th>
      <td>MISSION ST</td>
      <td>SILVER AVE</td>
      <td>MISSION ST &amp; SILVER AVE, San Franciso, CA, USA</td>
      <td>37.728749</td>
      <td>-122.431320</td>
    </tr>
    <tr>
      <th>14</th>
      <td>MISSION ST</td>
      <td>MOUNT VERNON AVE</td>
      <td>MISSION ST &amp; MOUNT VERNON AVE, San Franciso, C...</td>
      <td>37.714607</td>
      <td>-122.442670</td>
    </tr>
    <tr>
      <th>15</th>
      <td>MISSION ST</td>
      <td>30TH ST</td>
      <td>MISSION ST &amp; 30TH ST, San Franciso, CA, USA</td>
      <td>37.742387</td>
      <td>-122.421982</td>
    </tr>
    <tr>
      <th>16</th>
      <td>MISSION ST</td>
      <td>MAIN ST</td>
      <td>MISSION ST &amp; MAIN ST, San Franciso, CA, USA</td>
      <td>37.791848</td>
      <td>-122.394955</td>
    </tr>
    <tr>
      <th>17</th>
      <td>MISSION ST</td>
      <td>TRUMBULL ST</td>
      <td>MISSION ST &amp; TRUMBULL ST, San Franciso, CA, USA</td>
      <td>37.730758</td>
      <td>-122.429242</td>
    </tr>
    <tr>
      <th>18</th>
      <td>MISSION ST</td>
      <td>15TH ST</td>
      <td>MISSION ST &amp; 15TH ST, San Franciso, CA, USA</td>
      <td>37.767138</td>
      <td>-122.420603</td>
    </tr>
    <tr>
      <th>19</th>
      <td>MISSION ST</td>
      <td>KINGSTON ST</td>
      <td>MISSION ST &amp; KINGSTON ST, San Franciso, CA, USA</td>
      <td>37.741751</td>
      <td>-122.422392</td>
    </tr>
    <tr>
      <th>20</th>
      <td>MISSION ST</td>
      <td>TINGLEY ST</td>
      <td>MISSION ST &amp; TINGLEY ST, San Franciso, CA, USA</td>
      <td>37.728252</td>
      <td>-122.431806</td>
    </tr>
    <tr>
      <th>21</th>
      <td>MISSION ST</td>
      <td>THERESA ST</td>
      <td>MISSION ST &amp; THERESA ST, San Franciso, CA, USA</td>
      <td>37.727717</td>
      <td>-122.432404</td>
    </tr>
    <tr>
      <th>22</th>
      <td>MISSION ST</td>
      <td>PARK ST</td>
      <td>MISSION ST &amp; PARK ST, San Franciso, CA, USA</td>
      <td>37.736750</td>
      <td>-122.424248</td>
    </tr>
    <tr>
      <th>23</th>
      <td>MISSION ST</td>
      <td>OLIVER ST</td>
      <td>MISSION ST &amp; OLIVER ST, San Franciso, CA, USA</td>
      <td>37.709556</td>
      <td>-122.450519</td>
    </tr>
    <tr>
      <th>24</th>
      <td>MISSION ST</td>
      <td>STEUART ST</td>
      <td>MISSION ST &amp; STEUART ST, San Franciso, CA, USA</td>
      <td>37.793232</td>
      <td>-122.393205</td>
    </tr>
    <tr>
      <th>25</th>
      <td>MISSION ST</td>
      <td>HURON AVE</td>
      <td>MISSION ST &amp; HURON AVE, San Franciso, CA, USA</td>
      <td>37.708305</td>
      <td>-122.454194</td>
    </tr>
    <tr>
      <th>26</th>
      <td>MISSION ST</td>
      <td>SYCAMORE ST</td>
      <td>MISSION ST &amp; SYCAMORE ST, San Franciso, CA, USA</td>
      <td>37.762714</td>
      <td>-122.419471</td>
    </tr>
    <tr>
      <th>27</th>
      <td>MISSION ST</td>
      <td>02ND ST</td>
      <td>MISSION ST &amp; 02ND ST, San Franciso, CA, USA</td>
      <td>37.787836</td>
      <td>-122.400201</td>
    </tr>
    <tr>
      <th>28</th>
      <td>MISSION ST</td>
      <td>MINT ST</td>
      <td>MISSION ST &amp; MINT ST, San Franciso, CA, USA</td>
      <td>37.782212</td>
      <td>-122.407166</td>
    </tr>
    <tr>
      <th>29</th>
      <td>MISSION ST</td>
      <td>GENEVA AVE</td>
      <td>MISSION ST &amp; GENEVA AVE, San Franciso, CA, USA</td>
      <td>37.716407</td>
      <td>-122.441087</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>89</th>
      <td>MISSION ST</td>
      <td>07TH ST</td>
      <td>MISSION ST &amp; 07TH ST, San Franciso, CA, USA</td>
      <td>37.778990</td>
      <td>-122.412203</td>
    </tr>
    <tr>
      <th>90</th>
      <td>MISSION ST</td>
      <td>NORTON ST</td>
      <td>MISSION ST &amp; NORTON ST, San Franciso, CA, USA</td>
      <td>37.724310</td>
      <td>-122.435190</td>
    </tr>
    <tr>
      <th>91</th>
      <td>MISSION ST</td>
      <td>RUTH ST</td>
      <td>MISSION ST &amp; RUTH ST, San Franciso, CA, USA</td>
      <td>37.723050</td>
      <td>-122.436068</td>
    </tr>
    <tr>
      <th>92</th>
      <td>MISSION ST</td>
      <td>BRAZIL AVE</td>
      <td>MISSION ST &amp; BRAZIL AVE, San Franciso, CA, USA</td>
      <td>37.724688</td>
      <td>-122.434824</td>
    </tr>
    <tr>
      <th>93</th>
      <td>MISSION ST</td>
      <td>GUTTENBERG ST</td>
      <td>MISSION ST &amp; GUTTENBERG ST, San Franciso, CA, USA</td>
      <td>37.712783</td>
      <td>-122.444602</td>
    </tr>
    <tr>
      <th>94</th>
      <td>MISSION ST</td>
      <td>23RD ST</td>
      <td>MISSION ST &amp; 23RD ST, San Franciso, CA, USA</td>
      <td>37.753845</td>
      <td>-122.418619</td>
    </tr>
    <tr>
      <th>95</th>
      <td>MISSION ST</td>
      <td>RANDALL ST</td>
      <td>MISSION ST &amp; RANDALL ST, San Franciso, CA, USA</td>
      <td>37.739779</td>
      <td>-122.423676</td>
    </tr>
    <tr>
      <th>96</th>
      <td>MISSION ST</td>
      <td>PERSIA AVE</td>
      <td>MISSION ST &amp; PERSIA AVE, San Franciso, CA, USA</td>
      <td>37.723153</td>
      <td>-122.436002</td>
    </tr>
    <tr>
      <th>97</th>
      <td>MISSION ST</td>
      <td>16TH ST</td>
      <td>MISSION ST &amp; 16TH ST, San Franciso, CA, USA</td>
      <td>37.765057</td>
      <td>-122.419698</td>
    </tr>
    <tr>
      <th>98</th>
      <td>MISSION ST</td>
      <td>YERBA BUENA LN</td>
      <td>MISSION ST &amp; YERBA BUENA LN, San Franciso, CA,...</td>
      <td>37.785138</td>
      <td>-122.403506</td>
    </tr>
    <tr>
      <th>99</th>
      <td>MISSION ST</td>
      <td>LAURA ST</td>
      <td>MISSION ST &amp; LAURA ST, San Franciso, CA, USA</td>
      <td>37.709688</td>
      <td>-122.450124</td>
    </tr>
    <tr>
      <th>100</th>
      <td>MISSION ST</td>
      <td>18TH ST</td>
      <td>MISSION ST &amp; 18TH ST, San Franciso, CA, USA</td>
      <td>37.761877</td>
      <td>-122.419797</td>
    </tr>
    <tr>
      <th>101</th>
      <td>MISSION ST</td>
      <td>LEO ST</td>
      <td>MISSION ST &amp; LEO ST, San Franciso, CA, USA</td>
      <td>37.722464</td>
      <td>-122.436509</td>
    </tr>
    <tr>
      <th>102</th>
      <td>MISSION ST</td>
      <td>POWERS AVE</td>
      <td>MISSION ST &amp; POWERS AVE, San Franciso, CA, USA</td>
      <td>37.746186</td>
      <td>-122.419524</td>
    </tr>
    <tr>
      <th>103</th>
      <td>MISSION ST</td>
      <td>SANTA ROSA AVE</td>
      <td>MISSION ST &amp; SANTA ROSA AVE, San Franciso, CA,...</td>
      <td>37.725773</td>
      <td>-122.434004</td>
    </tr>
    <tr>
      <th>104</th>
      <td>MISSION ST</td>
      <td>FLORENTINE AVE</td>
      <td>MISSION ST &amp; FLORENTINE AVE, San Franciso, CA,...</td>
      <td>37.713386</td>
      <td>-122.443966</td>
    </tr>
    <tr>
      <th>105</th>
      <td>MISSION ST</td>
      <td>FOOTE AVE</td>
      <td>MISSION ST &amp; FOOTE AVE, San Franciso, CA, USA</td>
      <td>37.712634</td>
      <td>-122.444770</td>
    </tr>
    <tr>
      <th>106</th>
      <td>MISSION ST</td>
      <td>BOSWORTH ST</td>
      <td>MISSION ST &amp; BOSWORTH ST, San Franciso, CA, USA</td>
      <td>37.733685</td>
      <td>-122.426396</td>
    </tr>
    <tr>
      <th>107</th>
      <td>MISSION ST</td>
      <td>EXCELSIOR AVE</td>
      <td>MISSION ST &amp; EXCELSIOR AVE, San Franciso, CA, USA</td>
      <td>37.726404</td>
      <td>-122.433412</td>
    </tr>
    <tr>
      <th>108</th>
      <td>MISSION ST</td>
      <td>WASHBURN ST</td>
      <td>MISSION ST &amp; WASHBURN ST, San Franciso, CA, USA</td>
      <td>37.775900</td>
      <td>-122.415153</td>
    </tr>
    <tr>
      <th>109</th>
      <td>MISSION ST</td>
      <td>MORSE ST</td>
      <td>MISSION ST &amp; MORSE ST, San Franciso, CA, USA</td>
      <td>37.710424</td>
      <td>-122.448320</td>
    </tr>
    <tr>
      <th>110</th>
      <td>MISSION ST</td>
      <td>ANGELOS ALY</td>
      <td>MISSION ST &amp; ANGELOS ALY, San Franciso, CA, USA</td>
      <td>37.778130</td>
      <td>-122.412328</td>
    </tr>
    <tr>
      <th>111</th>
      <td>MISSION ST</td>
      <td>BROOK ST</td>
      <td>MISSION ST &amp; BROOK ST, San Franciso, CA, USA</td>
      <td>37.740649</td>
      <td>-122.423105</td>
    </tr>
    <tr>
      <th>112</th>
      <td>MISSION ST</td>
      <td>09TH ST</td>
      <td>MISSION ST &amp; 09TH ST, San Franciso, CA, USA</td>
      <td>37.776031</td>
      <td>-122.414743</td>
    </tr>
    <tr>
      <th>113</th>
      <td>MISSION ST</td>
      <td>OTTAWA AVE</td>
      <td>MISSION ST &amp; OTTAWA AVE, San Franciso, CA, USA</td>
      <td>37.713605</td>
      <td>-122.443736</td>
    </tr>
    <tr>
      <th>114</th>
      <td>MISSION ST</td>
      <td>NIAGARA AVE</td>
      <td>MISSION ST &amp; NIAGARA AVE, San Franciso, CA, USA</td>
      <td>37.715587</td>
      <td>-122.441727</td>
    </tr>
    <tr>
      <th>115</th>
      <td>MISSION ST</td>
      <td>ACTON ST</td>
      <td>MISSION ST &amp; ACTON ST, San Franciso, CA, USA</td>
      <td>37.708785</td>
      <td>-122.452769</td>
    </tr>
    <tr>
      <th>116</th>
      <td>MISSION ST</td>
      <td>05TH ST</td>
      <td>MISSION ST &amp; 05TH ST, San Franciso, CA, USA</td>
      <td>37.782823</td>
      <td>-122.406759</td>
    </tr>
    <tr>
      <th>117</th>
      <td>MISSION ST</td>
      <td>24TH ST</td>
      <td>MISSION ST &amp; 24TH ST, San Franciso, CA, USA</td>
      <td>37.752246</td>
      <td>-122.418462</td>
    </tr>
    <tr>
      <th>118</th>
      <td>MISSION ST</td>
      <td>GRACE ST</td>
      <td>MISSION ST &amp; GRACE ST, San Franciso, CA, USA</td>
      <td>37.775610</td>
      <td>-122.415518</td>
    </tr>
  </tbody>
</table>
<p>119 rows × 5 columns</p>
</div>




```python
#export the bus_geocodes.csv for later usages
df.to_csv("bus_geocodes.csv")
```


```python
bus_geocodes = pd.read_csv("bus_geocodes.csv")
```


```python
#the below codes are for making map and markers of intersections
import folium 
```


```python
bus_stop_map = folium.Map(location=[37.77, -122.42], zoom_start=12)
```


```python
bus_stop_map
```




<div style="width:100%;"><div style="position:relative;width:100%;height:0;padding-bottom:60%;"><iframe src="data:text/html;charset=utf-8;base64,PCFET0NUWVBFIGh0bWw+CjxoZWFkPiAgICAKICAgIDxtZXRhIGh0dHAtZXF1aXY9ImNvbnRlbnQtdHlwZSIgY29udGVudD0idGV4dC9odG1sOyBjaGFyc2V0PVVURi04IiAvPgogICAgPHNjcmlwdD5MX1BSRUZFUl9DQU5WQVMgPSBmYWxzZTsgTF9OT19UT1VDSCA9IGZhbHNlOyBMX0RJU0FCTEVfM0QgPSBmYWxzZTs8L3NjcmlwdD4KICAgIDxzY3JpcHQgc3JjPSJodHRwczovL2Nkbi5qc2RlbGl2ci5uZXQvbnBtL2xlYWZsZXRAMS4yLjAvZGlzdC9sZWFmbGV0LmpzIj48L3NjcmlwdD4KICAgIDxzY3JpcHQgc3JjPSJodHRwczovL2FqYXguZ29vZ2xlYXBpcy5jb20vYWpheC9saWJzL2pxdWVyeS8xLjExLjEvanF1ZXJ5Lm1pbi5qcyI+PC9zY3JpcHQ+CiAgICA8c2NyaXB0IHNyYz0iaHR0cHM6Ly9tYXhjZG4uYm9vdHN0cmFwY2RuLmNvbS9ib290c3RyYXAvMy4yLjAvanMvYm9vdHN0cmFwLm1pbi5qcyI+PC9zY3JpcHQ+CiAgICA8c2NyaXB0IHNyYz0iaHR0cHM6Ly9jZG5qcy5jbG91ZGZsYXJlLmNvbS9hamF4L2xpYnMvTGVhZmxldC5hd2Vzb21lLW1hcmtlcnMvMi4wLjIvbGVhZmxldC5hd2Vzb21lLW1hcmtlcnMuanMiPjwvc2NyaXB0PgogICAgPGxpbmsgcmVsPSJzdHlsZXNoZWV0IiBocmVmPSJodHRwczovL2Nkbi5qc2RlbGl2ci5uZXQvbnBtL2xlYWZsZXRAMS4yLjAvZGlzdC9sZWFmbGV0LmNzcyIvPgogICAgPGxpbmsgcmVsPSJzdHlsZXNoZWV0IiBocmVmPSJodHRwczovL21heGNkbi5ib290c3RyYXBjZG4uY29tL2Jvb3RzdHJhcC8zLjIuMC9jc3MvYm9vdHN0cmFwLm1pbi5jc3MiLz4KICAgIDxsaW5rIHJlbD0ic3R5bGVzaGVldCIgaHJlZj0iaHR0cHM6Ly9tYXhjZG4uYm9vdHN0cmFwY2RuLmNvbS9ib290c3RyYXAvMy4yLjAvY3NzL2Jvb3RzdHJhcC10aGVtZS5taW4uY3NzIi8+CiAgICA8bGluayByZWw9InN0eWxlc2hlZXQiIGhyZWY9Imh0dHBzOi8vbWF4Y2RuLmJvb3RzdHJhcGNkbi5jb20vZm9udC1hd2Vzb21lLzQuNi4zL2Nzcy9mb250LWF3ZXNvbWUubWluLmNzcyIvPgogICAgPGxpbmsgcmVsPSJzdHlsZXNoZWV0IiBocmVmPSJodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy9MZWFmbGV0LmF3ZXNvbWUtbWFya2Vycy8yLjAuMi9sZWFmbGV0LmF3ZXNvbWUtbWFya2Vycy5jc3MiLz4KICAgIDxsaW5rIHJlbD0ic3R5bGVzaGVldCIgaHJlZj0iaHR0cHM6Ly9yYXdnaXQuY29tL3B5dGhvbi12aXN1YWxpemF0aW9uL2ZvbGl1bS9tYXN0ZXIvZm9saXVtL3RlbXBsYXRlcy9sZWFmbGV0LmF3ZXNvbWUucm90YXRlLmNzcyIvPgogICAgPHN0eWxlPmh0bWwsIGJvZHkge3dpZHRoOiAxMDAlO2hlaWdodDogMTAwJTttYXJnaW46IDA7cGFkZGluZzogMDt9PC9zdHlsZT4KICAgIDxzdHlsZT4jbWFwIHtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDtib3R0b206MDtyaWdodDowO2xlZnQ6MDt9PC9zdHlsZT4KICAgIAogICAgICAgICAgICA8c3R5bGU+ICNtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkgewogICAgICAgICAgICAgICAgcG9zaXRpb24gOiByZWxhdGl2ZTsKICAgICAgICAgICAgICAgIHdpZHRoIDogMTAwLjAlOwogICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAuMCU7CiAgICAgICAgICAgICAgICBsZWZ0OiAwLjAlOwogICAgICAgICAgICAgICAgdG9wOiAwLjAlOwogICAgICAgICAgICAgICAgfQogICAgICAgICAgICA8L3N0eWxlPgogICAgICAgIAo8L2hlYWQ+Cjxib2R5PiAgICAKICAgIAogICAgICAgICAgICA8ZGl2IGNsYXNzPSJmb2xpdW0tbWFwIiBpZD0ibWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5IiA+PC9kaXY+CiAgICAgICAgCjwvYm9keT4KPHNjcmlwdD4gICAgCiAgICAKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGJvdW5kcyA9IG51bGw7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgdmFyIG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSA9IEwubWFwKAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ21hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOScsCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Y2VudGVyOiBbMzcuNzcsLTEyMi40Ml0sCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB6b29tOiAxMiwKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heEJvdW5kczogYm91bmRzLAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXJzOiBbXSwKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdvcmxkQ29weUp1bXA6IGZhbHNlLAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JzOiBMLkNSUy5FUFNHMzg1NwogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTsKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHRpbGVfbGF5ZXJfOTQ5ODkwZTg0MmNhNDM4NGIwYTBkNWM3YWNhMzVmM2IgPSBMLnRpbGVMYXllcigKICAgICAgICAgICAgICAgICdodHRwczovL3tzfS50aWxlLm9wZW5zdHJlZXRtYXAub3JnL3t6fS97eH0ve3l9LnBuZycsCiAgICAgICAgICAgICAgICB7CiAgImF0dHJpYnV0aW9uIjogbnVsbCwKICAiZGV0ZWN0UmV0aW5hIjogZmFsc2UsCiAgIm1heFpvb20iOiAxOCwKICAibWluWm9vbSI6IDEsCiAgIm5vV3JhcCI6IGZhbHNlLAogICJzdWJkb21haW5zIjogImFiYyIKfQogICAgICAgICAgICAgICAgKS5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgIAo8L3NjcmlwdD4=" style="position:absolute;width:100%;height:100%;left:0;top:0;border:none !important;" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe></div></div>




```python
for index, row in bus_geocodes.iterrows():
    folium.Marker([row['latitude'], row['longtitude']],
              popup='{0}: {1} & {2}'.format(index, row['Street_One'], row['Street_Two'])).add_to(bus_stop_map)  

```


```python
bus_stop_map
```




<div style="width:100%;"><div style="position:relative;width:100%;height:0;padding-bottom:60%;"><iframe src="data:text/html;charset=utf-8;base64,PCFET0NUWVBFIGh0bWw+CjxoZWFkPiAgICAKICAgIDxtZXRhIGh0dHAtZXF1aXY9ImNvbnRlbnQtdHlwZSIgY29udGVudD0idGV4dC9odG1sOyBjaGFyc2V0PVVURi04IiAvPgogICAgPHNjcmlwdD5MX1BSRUZFUl9DQU5WQVMgPSBmYWxzZTsgTF9OT19UT1VDSCA9IGZhbHNlOyBMX0RJU0FCTEVfM0QgPSBmYWxzZTs8L3NjcmlwdD4KICAgIDxzY3JpcHQgc3JjPSJodHRwczovL2Nkbi5qc2RlbGl2ci5uZXQvbnBtL2xlYWZsZXRAMS4yLjAvZGlzdC9sZWFmbGV0LmpzIj48L3NjcmlwdD4KICAgIDxzY3JpcHQgc3JjPSJodHRwczovL2FqYXguZ29vZ2xlYXBpcy5jb20vYWpheC9saWJzL2pxdWVyeS8xLjExLjEvanF1ZXJ5Lm1pbi5qcyI+PC9zY3JpcHQ+CiAgICA8c2NyaXB0IHNyYz0iaHR0cHM6Ly9tYXhjZG4uYm9vdHN0cmFwY2RuLmNvbS9ib290c3RyYXAvMy4yLjAvanMvYm9vdHN0cmFwLm1pbi5qcyI+PC9zY3JpcHQ+CiAgICA8c2NyaXB0IHNyYz0iaHR0cHM6Ly9jZG5qcy5jbG91ZGZsYXJlLmNvbS9hamF4L2xpYnMvTGVhZmxldC5hd2Vzb21lLW1hcmtlcnMvMi4wLjIvbGVhZmxldC5hd2Vzb21lLW1hcmtlcnMuanMiPjwvc2NyaXB0PgogICAgPGxpbmsgcmVsPSJzdHlsZXNoZWV0IiBocmVmPSJodHRwczovL2Nkbi5qc2RlbGl2ci5uZXQvbnBtL2xlYWZsZXRAMS4yLjAvZGlzdC9sZWFmbGV0LmNzcyIvPgogICAgPGxpbmsgcmVsPSJzdHlsZXNoZWV0IiBocmVmPSJodHRwczovL21heGNkbi5ib290c3RyYXBjZG4uY29tL2Jvb3RzdHJhcC8zLjIuMC9jc3MvYm9vdHN0cmFwLm1pbi5jc3MiLz4KICAgIDxsaW5rIHJlbD0ic3R5bGVzaGVldCIgaHJlZj0iaHR0cHM6Ly9tYXhjZG4uYm9vdHN0cmFwY2RuLmNvbS9ib290c3RyYXAvMy4yLjAvY3NzL2Jvb3RzdHJhcC10aGVtZS5taW4uY3NzIi8+CiAgICA8bGluayByZWw9InN0eWxlc2hlZXQiIGhyZWY9Imh0dHBzOi8vbWF4Y2RuLmJvb3RzdHJhcGNkbi5jb20vZm9udC1hd2Vzb21lLzQuNi4zL2Nzcy9mb250LWF3ZXNvbWUubWluLmNzcyIvPgogICAgPGxpbmsgcmVsPSJzdHlsZXNoZWV0IiBocmVmPSJodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy9MZWFmbGV0LmF3ZXNvbWUtbWFya2Vycy8yLjAuMi9sZWFmbGV0LmF3ZXNvbWUtbWFya2Vycy5jc3MiLz4KICAgIDxsaW5rIHJlbD0ic3R5bGVzaGVldCIgaHJlZj0iaHR0cHM6Ly9yYXdnaXQuY29tL3B5dGhvbi12aXN1YWxpemF0aW9uL2ZvbGl1bS9tYXN0ZXIvZm9saXVtL3RlbXBsYXRlcy9sZWFmbGV0LmF3ZXNvbWUucm90YXRlLmNzcyIvPgogICAgPHN0eWxlPmh0bWwsIGJvZHkge3dpZHRoOiAxMDAlO2hlaWdodDogMTAwJTttYXJnaW46IDA7cGFkZGluZzogMDt9PC9zdHlsZT4KICAgIDxzdHlsZT4jbWFwIHtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDtib3R0b206MDtyaWdodDowO2xlZnQ6MDt9PC9zdHlsZT4KICAgIAogICAgICAgICAgICA8c3R5bGU+ICNtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkgewogICAgICAgICAgICAgICAgcG9zaXRpb24gOiByZWxhdGl2ZTsKICAgICAgICAgICAgICAgIHdpZHRoIDogMTAwLjAlOwogICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAuMCU7CiAgICAgICAgICAgICAgICBsZWZ0OiAwLjAlOwogICAgICAgICAgICAgICAgdG9wOiAwLjAlOwogICAgICAgICAgICAgICAgfQogICAgICAgICAgICA8L3N0eWxlPgogICAgICAgIAo8L2hlYWQ+Cjxib2R5PiAgICAKICAgIAogICAgICAgICAgICA8ZGl2IGNsYXNzPSJmb2xpdW0tbWFwIiBpZD0ibWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5IiA+PC9kaXY+CiAgICAgICAgCjwvYm9keT4KPHNjcmlwdD4gICAgCiAgICAKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGJvdW5kcyA9IG51bGw7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgdmFyIG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSA9IEwubWFwKAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ21hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOScsCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Y2VudGVyOiBbMzcuNzcsLTEyMi40Ml0sCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB6b29tOiAxMiwKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heEJvdW5kczogYm91bmRzLAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5ZXJzOiBbXSwKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdvcmxkQ29weUp1bXA6IGZhbHNlLAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JzOiBMLkNSUy5FUFNHMzg1NwogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTsKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHRpbGVfbGF5ZXJfOTQ5ODkwZTg0MmNhNDM4NGIwYTBkNWM3YWNhMzVmM2IgPSBMLnRpbGVMYXllcigKICAgICAgICAgICAgICAgICdodHRwczovL3tzfS50aWxlLm9wZW5zdHJlZXRtYXAub3JnL3t6fS97eH0ve3l9LnBuZycsCiAgICAgICAgICAgICAgICB7CiAgImF0dHJpYnV0aW9uIjogbnVsbCwKICAiZGV0ZWN0UmV0aW5hIjogZmFsc2UsCiAgIm1heFpvb20iOiAxOCwKICAibWluWm9vbSI6IDEsCiAgIm5vV3JhcCI6IGZhbHNlLAogICJzdWJkb21haW5zIjogImFiYyIKfQogICAgICAgICAgICAgICAgKS5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2M1OWZkZGY0OWNmOTQzMThiYThhYzQ4YzQ0OWMwMGQ2ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzE4NDc3ODk5OTk5OTk2LC0xMjIuNDM5NTM1NTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF80ODgxODFiNDgzMDg0ZWM4OGE2ZTEwNTRkMzExZTU3NSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8wMjU0YTZkZTg5NWQ0YWE1YmRmYmU5NjJlODc2YmQ0OCA9ICQoJzxkaXYgaWQ9Imh0bWxfMDI1NGE2ZGU4OTVkNGFhNWJkZmJlOTYyZTg3NmJkNDgiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjA6IE1JU1NJT04gU1QgJiBJVEFMWSBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzQ4ODE4MWI0ODMwODRlYzg4YTZlMTA1NGQzMTFlNTc1LnNldENvbnRlbnQoaHRtbF8wMjU0YTZkZTg5NWQ0YWE1YmRmYmU5NjJlODc2YmQ0OCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2M1OWZkZGY0OWNmOTQzMThiYThhYzQ4YzQ0OWMwMGQ2LmJpbmRQb3B1cChwb3B1cF80ODgxODFiNDgzMDg0ZWM4OGE2ZTEwNTRkMzExZTU3NSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl82OTNiMTgzN2ViZWQ0NjM1ODY2MTlkNTVhYTI3MDhlYyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc4NzQ1NjEsLTEyMi40MDA1MjM0MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2QwZTU0NzNlNzVlZjQ2YWU5NTRhYTVjZDk2MTIwMDJlID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzJhOWViNWQzYzI5MDQ1ZjJhMzU3MjYxM2EwMzRlZDMyID0gJCgnPGRpdiBpZD0iaHRtbF8yYTllYjVkM2MyOTA0NWYyYTM1NzI2MTNhMDM0ZWQzMiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTogTUlTU0lPTiBTVCAmIE5FVyBNT05UR09NRVJZIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9kMGU1NDczZTc1ZWY0NmFlOTU0YWE1Y2Q5NjEyMDAyZS5zZXRDb250ZW50KGh0bWxfMmE5ZWI1ZDNjMjkwNDVmMmEzNTcyNjEzYTAzNGVkMzIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl82OTNiMTgzN2ViZWQ0NjM1ODY2MTlkNTVhYTI3MDhlYy5iaW5kUG9wdXAocG9wdXBfZDBlNTQ3M2U3NWVmNDZhZTk1NGFhNWNkOTYxMjAwMmUpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfOTBkNzI0ODI4YmNiNDVkZjhkNGE5M2MyM2Q0MmEyZDEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODk5NTQzLC0xMjIuMzk3NTE0MDk5OTk5OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9hYTZjMmZjOGUwMDk0YjVkOTZhZWYzMmYxZTNlNWQ1NyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8xMDBlYzkxNjI1MWY0NjUyOWVjM2E4MDMxYzIwNjc4OCA9ICQoJzxkaXYgaWQ9Imh0bWxfMTAwZWM5MTYyNTFmNDY1MjllYzNhODAzMWMyMDY3ODgiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjI6IE1JU1NJT04gU1QgJiAwMVNUIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9hYTZjMmZjOGUwMDk0YjVkOTZhZWYzMmYxZTNlNWQ1Ny5zZXRDb250ZW50KGh0bWxfMTAwZWM5MTYyNTFmNDY1MjllYzNhODAzMWMyMDY3ODgpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl85MGQ3MjQ4MjhiY2I0NWRmOGQ0YTkzYzIzZDQyYTJkMS5iaW5kUG9wdXAocG9wdXBfYWE2YzJmYzhlMDA5NGI1ZDk2YWVmMzJmMWUzZTVkNTcpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNWQ1ZGMyMzdmNDdhNGRhZmFmNTY2ZDRkMzVlZDFmMTQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NTg4NDI5LC0xMjIuNDE4OTk4M10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzhlOTQzODNhNDFkYTRkNzc5YzliMTY0ZTdiZjk5M2RiID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzU3MTljYjMzNTBlYjQ1ZjJiNDgyYWUxMDhjOGRkZmRlID0gJCgnPGRpdiBpZD0iaHRtbF81NzE5Y2IzMzUwZWI0NWYyYjQ4MmFlMTA4YzhkZGZkZSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MzogTUlTU0lPTiBTVCAmIDIwVEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzhlOTQzODNhNDFkYTRkNzc5YzliMTY0ZTdiZjk5M2RiLnNldENvbnRlbnQoaHRtbF81NzE5Y2IzMzUwZWI0NWYyYjQ4MmFlMTA4YzhkZGZkZSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzVkNWRjMjM3ZjQ3YTRkYWZhZjU2NmQ0ZDM1ZWQxZjE0LmJpbmRQb3B1cChwb3B1cF84ZTk0MzgzYTQxZGE0ZDc3OWM5YjE2NGU3YmY5OTNkYik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl85NjRjM2VhM2I3OGM0ODAwYmIyY2FjMzU4MmQ2YWUwYSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc5MDQ1NDcwMDAwMDAwNSwtMTIyLjM5NjcyNjM5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYTNiNzhkNDFkMmJjNGQzNGJiOGM2M2M4YmMzZTBhMzYgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMWQ0ZjI0NWYyZTI4NDhkOTlhZWQ1NmM4ZmJiMmE5ZWYgPSAkKCc8ZGl2IGlkPSJodG1sXzFkNGYyNDVmMmUyODQ4ZDk5YWVkNTZjOGZiYjJhOWVmIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij40OiBNSVNTSU9OIFNUICYgRlJFTU9OVCBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYTNiNzhkNDFkMmJjNGQzNGJiOGM2M2M4YmMzZTBhMzYuc2V0Q29udGVudChodG1sXzFkNGYyNDVmMmUyODQ4ZDk5YWVkNTZjOGZiYjJhOWVmKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfOTY0YzNlYTNiNzhjNDgwMGJiMmNhYzM1ODJkNmFlMGEuYmluZFBvcHVwKHBvcHVwX2EzYjc4ZDQxZDJiYzRkMzRiYjhjNjNjOGJjM2UwYTM2KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzA1ZjZmNzQyNzg5ZDRhZTk5NGMyOWFhMGE4NTAyY2I2ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzY5OTUzNiwtMTIyLjQxOTk2MjNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8yZjBiYTIxMDI2MGY0MTg2OTU2ZjBhYzg3NmUwMmE5OCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF84NDIwMjFjYWU2MTk0NzY0YjEwYzA0MDk2MjNiYzY1ZCA9ICQoJzxkaXYgaWQ9Imh0bWxfODQyMDIxY2FlNjE5NDc2NGIxMGMwNDA5NjIzYmM2NWQiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjU6IE1JU1NJT04gU1QgJiAxM1RIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8yZjBiYTIxMDI2MGY0MTg2OTU2ZjBhYzg3NmUwMmE5OC5zZXRDb250ZW50KGh0bWxfODQyMDIxY2FlNjE5NDc2NGIxMGMwNDA5NjIzYmM2NWQpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8wNWY2Zjc0Mjc4OWQ0YWU5OTRjMjlhYTBhODUwMmNiNi5iaW5kUG9wdXAocG9wdXBfMmYwYmEyMTAyNjBmNDE4Njk1NmYwYWM4NzZlMDJhOTgpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNzJlZDNlMDBhM2I5NDk0MTkxNmNkYTdkYTM5MTVlYzEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NjkwNjMxLC0xMjIuNDIwMDcyM10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzA4ZWJkNjhjYzU3MTQyODk4YzFhNzM3YjE5ZjIzY2ZjID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzQxZjUwMDkyMzQ0YTQxYWE5ZWEzOWE5M2Q0OTEyMTRkID0gJCgnPGRpdiBpZD0iaHRtbF80MWY1MDA5MjM0NGE0MWFhOWVhMzlhOTNkNDkxMjE0ZCIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NjogTUlTU0lPTiBTVCAmIEVSSUUgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzA4ZWJkNjhjYzU3MTQyODk4YzFhNzM3YjE5ZjIzY2ZjLnNldENvbnRlbnQoaHRtbF80MWY1MDA5MjM0NGE0MWFhOWVhMzlhOTNkNDkxMjE0ZCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzcyZWQzZTAwYTNiOTQ5NDE5MTZjZGE3ZGEzOTE1ZWMxLmJpbmRQb3B1cChwb3B1cF8wOGViZDY4Y2M1NzE0Mjg5OGMxYTczN2IxOWYyM2NmYyk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9iYTFjZDdiMTM1Yzc0ODJjOTU3MzVjMDQ5YjEyN2IyNSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc5MTE1OTIsLTEyMi4zOTU4MjYyMDAwMDAwMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzQyZGNhNjFiMjYyNTQ0YWNhNGMxYjExN2ZkNGFlZjQxID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzkzMTdmNmNjYzY0ZTRlNmU4OGYwMjVkMjZkNThhMmZiID0gJCgnPGRpdiBpZD0iaHRtbF85MzE3ZjZjY2M2NGU0ZTZlODhmMDI1ZDI2ZDU4YTJmYiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NzogTUlTU0lPTiBTVCAmIEJFQUxFIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF80MmRjYTYxYjI2MjU0NGFjYTRjMWIxMTdmZDRhZWY0MS5zZXRDb250ZW50KGh0bWxfOTMxN2Y2Y2NjNjRlNGU2ZTg4ZjAyNWQyNmQ1OGEyZmIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9iYTFjZDdiMTM1Yzc0ODJjOTU3MzVjMDQ5YjEyN2IyNS5iaW5kUG9wdXAocG9wdXBfNDJkY2E2MWIyNjI1NDRhY2E0YzFiMTE3ZmQ0YWVmNDEpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNDM5ZjdjM2U4NDA3NDM1NWJkM2FmMGQ3NTNlZTAyMmYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDU2MDM0OTk5OTk5OTQsLTEyMi40MTk4OTc1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNGNmNWE5OGNkZTZmNDhiNmE5NjEwMDMxODBkZDhkZjggPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYjVmNWViYzAzNmFjNDI1NGJmZmQzNjNhZjljYmFmNjMgPSAkKCc8ZGl2IGlkPSJodG1sX2I1ZjVlYmMwMzZhYzQyNTRiZmZkMzYzYWY5Y2JhZjYzIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij44OiBNSVNTSU9OIFNUICYgRkFJUiBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzRjZjVhOThjZGU2ZjQ4YjZhOTYxMDAzMTgwZGQ4ZGY4LnNldENvbnRlbnQoaHRtbF9iNWY1ZWJjMDM2YWM0MjU0YmZmZDM2M2FmOWNiYWY2Myk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzQzOWY3YzNlODQwNzQzNTViZDNhZjBkNzUzZWUwMjJmLmJpbmRQb3B1cChwb3B1cF80Y2Y1YTk4Y2RlNmY0OGI2YTk2MTAwMzE4MGRkOGRmOCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9iYWJiNTdlZTE3NTc0MmY5OTc0NThkN2M4NzE5YjA2YSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczMzk1MzE5OTk5OTk5LC0xMjIuNDI2MTQyMDk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8zYTU0MzNiZmNhYzU0MDBmOGNkZWE0NzU5YmQ4ZGE2YyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9kZjdiMmFkMjRlMjY0YmFjOTYwNDAzNDZiNmVkOWRkNyA9ICQoJzxkaXYgaWQ9Imh0bWxfZGY3YjJhZDI0ZTI2NGJhYzk2MDQwMzQ2YjZlZDlkZDciIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjk6IE1JU1NJT04gU1QgJiBTQUlOVCBNQVJZUyBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzNhNTQzM2JmY2FjNTQwMGY4Y2RlYTQ3NTliZDhkYTZjLnNldENvbnRlbnQoaHRtbF9kZjdiMmFkMjRlMjY0YmFjOTYwNDAzNDZiNmVkOWRkNyk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2JhYmI1N2VlMTc1NzQyZjk5NzQ1OGQ3Yzg3MTliMDZhLmJpbmRQb3B1cChwb3B1cF8zYTU0MzNiZmNhYzU0MDBmOGNkZWE0NzU5YmQ4ZGE2Yyk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl83YjUyNjA1MDQ3Mzk0ZDdhODhjMWRiMWI1NmM1YjFlZSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxNzY3NTQsLTEyMi40NDAxNDkyMDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzA5MDYxNTM0YzAzYzRjYzhhNjEzMGY5ZTg2MzE0MTA0ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzhkZDAyZTljMjZhZjQ1ZWM4Zjc0MGNkMDI4OTE5ZDA3ID0gJCgnPGRpdiBpZD0iaHRtbF84ZGQwMmU5YzI2YWY0NWVjOGY3NDBjZDAyODkxOWQwNyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTA6IE1JU1NJT04gU1QgJiBTRU5FQ0EgQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8wOTA2MTUzNGMwM2M0Y2M4YTYxMzBmOWU4NjMxNDEwNC5zZXRDb250ZW50KGh0bWxfOGRkMDJlOWMyNmFmNDVlYzhmNzQwY2QwMjg5MTlkMDcpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl83YjUyNjA1MDQ3Mzk0ZDdhODhjMWRiMWI1NmM1YjFlZS5iaW5kUG9wdXAocG9wdXBfMDkwNjE1MzRjMDNjNGNjOGE2MTMwZjllODYzMTQxMDQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMTEzMDU1MGI0NjI1NDQ4NWEwOTUyYzJiM2IwMzliNTEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODgzNzQ0LC0xMjIuMzk5MzYyMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzAyN2ZiMmIxOWQ2ZDQzZTA5ZmYwN2FiZjBiMjE0ZjgzID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2U2OTczM2I5OTMyMzQyZmNiYTQ4OTVlMTU3YjE1YzljID0gJCgnPGRpdiBpZD0iaHRtbF9lNjk3MzNiOTkzMjM0MmZjYmE0ODk1ZTE1N2IxNWM5YyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTE6IE1JU1NJT04gU1QgJiBBTlRIT05ZIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8wMjdmYjJiMTlkNmQ0M2UwOWZmMDdhYmYwYjIxNGY4My5zZXRDb250ZW50KGh0bWxfZTY5NzMzYjk5MzIzNDJmY2JhNDg5NWUxNTdiMTVjOWMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8xMTMwNTUwYjQ2MjU0NDg1YTA5NTJjMmIzYjAzOWI1MS5iaW5kUG9wdXAocG9wdXBfMDI3ZmIyYjE5ZDZkNDNlMDlmZjA3YWJmMGIyMTRmODMpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMGQ1NTlkNDM1ZDZhNDFmNzk2MjI2OGUzNWFmZDUwZTQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODMwOTA0LC0xMjIuNDA2MDM3ODAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF83N2M2MmY5MGNiNDY0N2M1ODk2NGY2MWEyNzI1YTEyYSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8yOGUwOGViNmVjZjY0ZGE2OTE2NjdmYjUyMTgwNjFmMyA9ICQoJzxkaXYgaWQ9Imh0bWxfMjhlMDhlYjZlY2Y2NGRhNjkxNjY3ZmI1MjE4MDYxZjMiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjEyOiBNSVNTSU9OIFNUICYgSkVTU0lFIEVBU1QgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzc3YzYyZjkwY2I0NjQ3YzU4OTY0ZjYxYTI3MjVhMTJhLnNldENvbnRlbnQoaHRtbF8yOGUwOGViNmVjZjY0ZGE2OTE2NjdmYjUyMTgwNjFmMyk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzBkNTU5ZDQzNWQ2YTQxZjc5NjIyNjhlMzVhZmQ1MGU0LmJpbmRQb3B1cChwb3B1cF83N2M2MmY5MGNiNDY0N2M1ODk2NGY2MWEyNzI1YTEyYSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wZGYyZWI0YmVkYWI0YTgyODkwMDVlNzhjNjRiNTZkYSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyODc0ODUsLTEyMi40MzEzMTk3OTk5OTk5OF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2Y4MzZkYTY3NzZmNzRiMTlhMDc4YzNiMGQ5ZjY0NTg4ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzVkNjlkY2YyMzkwMTQ2ZDM4OWU2M2NkNzRkZWJmZWIyID0gJCgnPGRpdiBpZD0iaHRtbF81ZDY5ZGNmMjM5MDE0NmQzODllNjNjZDc0ZGViZmViMiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTM6IE1JU1NJT04gU1QgJiBTSUxWRVIgQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9mODM2ZGE2Nzc2Zjc0YjE5YTA3OGMzYjBkOWY2NDU4OC5zZXRDb250ZW50KGh0bWxfNWQ2OWRjZjIzOTAxNDZkMzg5ZTYzY2Q3NGRlYmZlYjIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8wZGYyZWI0YmVkYWI0YTgyODkwMDVlNzhjNjRiNTZkYS5iaW5kUG9wdXAocG9wdXBfZjgzNmRhNjc3NmY3NGIxOWEwNzhjM2IwZDlmNjQ1ODgpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNzEyOTFlOTE0NDdiNGVjYmE4MzU5YzE3NDYwNDM5YTYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTQ2MDY1LC0xMjIuNDQyNjY5N10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzkzNzNkNjFkNTA0NTRlN2NiNDBhZTY2YzA1Nzk1NGE0ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzcyNjEyNzk1MjE1NDQ0ZjJhZmNlMTUzNjVkMWU4Mzg4ID0gJCgnPGRpdiBpZD0iaHRtbF83MjYxMjc5NTIxNTQ0NGYyYWZjZTE1MzY1ZDFlODM4OCIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTQ6IE1JU1NJT04gU1QgJiBNT1VOVCBWRVJOT04gQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF85MzczZDYxZDUwNDU0ZTdjYjQwYWU2NmMwNTc5NTRhNC5zZXRDb250ZW50KGh0bWxfNzI2MTI3OTUyMTU0NDRmMmFmY2UxNTM2NWQxZTgzODgpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl83MTI5MWU5MTQ0N2I0ZWNiYTgzNTljMTc0NjA0MzlhNi5iaW5kUG9wdXAocG9wdXBfOTM3M2Q2MWQ1MDQ1NGU3Y2I0MGFlNjZjMDU3OTU0YTQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNmEzNDNhMjk0OTRhNGQ3ODlkYWZkMDk1YjU3YTg1NGIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDIzODY5LC0xMjIuNDIxOTgyMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzI2YWNmMzk5ZjYyNzQzNDNiNjdlYTgxNzY4MjgwMTJlID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2NiODRhMjk1MmNlMjQyMDhhMTE0M2I4YWQ1MmZlMmM1ID0gJCgnPGRpdiBpZD0iaHRtbF9jYjg0YTI5NTJjZTI0MjA4YTExNDNiOGFkNTJmZTJjNSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTU6IE1JU1NJT04gU1QgJiAzMFRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8yNmFjZjM5OWY2Mjc0MzQzYjY3ZWE4MTc2ODI4MDEyZS5zZXRDb250ZW50KGh0bWxfY2I4NGEyOTUyY2UyNDIwOGExMTQzYjhhZDUyZmUyYzUpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl82YTM0M2EyOTQ5NGE0ZDc4OWRhZmQwOTViNTdhODU0Yi5iaW5kUG9wdXAocG9wdXBfMjZhY2YzOTlmNjI3NDM0M2I2N2VhODE3NjgyODAxMmUpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMTkyYmUzZWJlOGE4NDcxOGE1YTRhZTlhZTVhMjFmNzkgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43OTE4NDg0LC0xMjIuMzk0OTU1M10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2RhMjdlNTg5YTM4ZDQ4ODZhMDA1NGJiNjc3YWUzMDM3ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2FmZjk5MmE2YjFlNDQ4NmZiZjA3ZWU0OTg3YTU2ZjlhID0gJCgnPGRpdiBpZD0iaHRtbF9hZmY5OTJhNmIxZTQ0ODZmYmYwN2VlNDk4N2E1NmY5YSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTY6IE1JU1NJT04gU1QgJiBNQUlOIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9kYTI3ZTU4OWEzOGQ0ODg2YTAwNTRiYjY3N2FlMzAzNy5zZXRDb250ZW50KGh0bWxfYWZmOTkyYTZiMWU0NDg2ZmJmMDdlZTQ5ODdhNTZmOWEpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8xOTJiZTNlYmU4YTg0NzE4YTVhNGFlOWFlNWEyMWY3OS5iaW5kUG9wdXAocG9wdXBfZGEyN2U1ODlhMzhkNDg4NmEwMDU0YmI2NzdhZTMwMzcpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYjA1NjI1ZmMwY2Y4NGM3NmIzNjIxNjUxMzY5ZjRiZTQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzA3NTgyMDAwMDAwMDQsLTEyMi40MjkyNDE5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMGMyNTFkNDI0MzkzNDdkYjkyYmY0ODBkM2RlZmFlMjAgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfODVmYTU3YmJmZDBjNGJjN2E5NTRmOWZmMzkyZmI0ZDcgPSAkKCc8ZGl2IGlkPSJodG1sXzg1ZmE1N2JiZmQwYzRiYzdhOTU0ZjlmZjM5MmZiNGQ3IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xNzogTUlTU0lPTiBTVCAmIFRSVU1CVUxMIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8wYzI1MWQ0MjQzOTM0N2RiOTJiZjQ4MGQzZGVmYWUyMC5zZXRDb250ZW50KGh0bWxfODVmYTU3YmJmZDBjNGJjN2E5NTRmOWZmMzkyZmI0ZDcpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9iMDU2MjVmYzBjZjg0Yzc2YjM2MjE2NTEzNjlmNGJlNC5iaW5kUG9wdXAocG9wdXBfMGMyNTFkNDI0MzkzNDdkYjkyYmY0ODBkM2RlZmFlMjApOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMWVjZjgwOWY1OTBlNDcyZmI3ZDljZjJjN2RiYzE0MjIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NjcxMzc3OTk5OTk5OSwtMTIyLjQyMDYwMzIwMDAwMDAyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfN2RkNzRkMzJmYjhiNDNjMDg4NDAwOWVkZGE4MGM1ZWYgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMTQxNzhlNGZkNzE1NGRlN2IyMjIzNjM2MjdjNzRiYjQgPSAkKCc8ZGl2IGlkPSJodG1sXzE0MTc4ZTRmZDcxNTRkZTdiMjIyMzYzNjI3Yzc0YmI0IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xODogTUlTU0lPTiBTVCAmIDE1VEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzdkZDc0ZDMyZmI4YjQzYzA4ODQwMDllZGRhODBjNWVmLnNldENvbnRlbnQoaHRtbF8xNDE3OGU0ZmQ3MTU0ZGU3YjIyMjM2MzYyN2M3NGJiNCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzFlY2Y4MDlmNTkwZTQ3MmZiN2Q5Y2YyYzdkYmMxNDIyLmJpbmRQb3B1cChwb3B1cF83ZGQ3NGQzMmZiOGI0M2MwODg0MDA5ZWRkYTgwYzVlZik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9iYTk3MDg1MjE5NDY0YWE5YmM3ZDlmMjc2NGEyNmM0ZSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0MTc1MDU5OTk5OTk5NiwtMTIyLjQyMjM5MTcwMDAwMDAyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMGVjZWZkYmVlYTgwNDIwYmI5MTdhY2E4MjdjNTE3NjQgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYzgzMjI0ZWIzMTFhNDkxMzg2NWQwY2U1NzhjMzFmZDIgPSAkKCc8ZGl2IGlkPSJodG1sX2M4MzIyNGViMzExYTQ5MTM4NjVkMGNlNTc4YzMxZmQyIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xOTogTUlTU0lPTiBTVCAmIEtJTkdTVE9OIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8wZWNlZmRiZWVhODA0MjBiYjkxN2FjYTgyN2M1MTc2NC5zZXRDb250ZW50KGh0bWxfYzgzMjI0ZWIzMTFhNDkxMzg2NWQwY2U1NzhjMzFmZDIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9iYTk3MDg1MjE5NDY0YWE5YmM3ZDlmMjc2NGEyNmM0ZS5iaW5kUG9wdXAocG9wdXBfMGVjZWZkYmVlYTgwNDIwYmI5MTdhY2E4MjdjNTE3NjQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMjJhOGFkOWVjZjcyNGQ2MTk0ZTdlOTRlNTI5YTIxNmYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjgyNTE5LC0xMjIuNDMxODA2MDAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9kMzg2OWM0MmUzZWU0NDgwOTc4ZjgxYzhkMjMyZjFkYyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF84ZjcwZjIxMzdlNjA0NzQxOTcyNzkwOWEwMDdkYzQ4ZCA9ICQoJzxkaXYgaWQ9Imh0bWxfOGY3MGYyMTM3ZTYwNDc0MTk3Mjc5MDlhMDA3ZGM0OGQiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjIwOiBNSVNTSU9OIFNUICYgVElOR0xFWSBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfZDM4NjljNDJlM2VlNDQ4MDk3OGY4MWM4ZDIzMmYxZGMuc2V0Q29udGVudChodG1sXzhmNzBmMjEzN2U2MDQ3NDE5NzI3OTA5YTAwN2RjNDhkKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMjJhOGFkOWVjZjcyNGQ2MTk0ZTdlOTRlNTI5YTIxNmYuYmluZFBvcHVwKHBvcHVwX2QzODY5YzQyZTNlZTQ0ODA5NzhmODFjOGQyMzJmMWRjKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzYyOWMyYmEwMGU5YTRjOWVhNTdmZWEwZDg2NjAzNTI3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI3NzE2NiwtMTIyLjQzMjQwMzc5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfZjMwNjU1MWI3MGY3NGJjMTg0ZWVkOGU1YTVhMWYxYmEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYzhmZmYxZjQwNDQyNGQxM2EyODM4M2MyY2E5MTBlOWEgPSAkKCc8ZGl2IGlkPSJodG1sX2M4ZmZmMWY0MDQ0MjRkMTNhMjgzODNjMmNhOTEwZTlhIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4yMTogTUlTU0lPTiBTVCAmIFRIRVJFU0EgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2YzMDY1NTFiNzBmNzRiYzE4NGVlZDhlNWE1YTFmMWJhLnNldENvbnRlbnQoaHRtbF9jOGZmZjFmNDA0NDI0ZDEzYTI4MzgzYzJjYTkxMGU5YSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzYyOWMyYmEwMGU5YTRjOWVhNTdmZWEwZDg2NjAzNTI3LmJpbmRQb3B1cChwb3B1cF9mMzA2NTUxYjcwZjc0YmMxODRlZWQ4ZTVhNWExZjFiYSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8yMWVmODAwNjk0Nzg0NWJkYTU1MzllZmQ5NWM2ZGEwOSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczNjc0OTYsLTEyMi40MjQyNDg0MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2EzZTA4YWI0YmJiOTQ5MmU5OWUwNmI4ZDBjNGUyZjRhID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzViZTdjOTEyZTdmNDQ4YjE5ZDc2OTAzNDI2MTY0NzIyID0gJCgnPGRpdiBpZD0iaHRtbF81YmU3YzkxMmU3ZjQ0OGIxOWQ3NjkwMzQyNjE2NDcyMiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MjI6IE1JU1NJT04gU1QgJiBQQVJLIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9hM2UwOGFiNGJiYjk0OTJlOTllMDZiOGQwYzRlMmY0YS5zZXRDb250ZW50KGh0bWxfNWJlN2M5MTJlN2Y0NDhiMTlkNzY5MDM0MjYxNjQ3MjIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8yMWVmODAwNjk0Nzg0NWJkYTU1MzllZmQ5NWM2ZGEwOS5iaW5kUG9wdXAocG9wdXBfYTNlMDhhYjRiYmI5NDkyZTk5ZTA2YjhkMGM0ZTJmNGEpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYWQxYWM1YjM2YWJkNDM4NGFjZjkxNzE2NTRmYzhlOGMgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MDk1NTYyLC0xMjIuNDUwNTE5M10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2IzNmRlZTQ3MmJlMTQ1MDk4NDNhNzY4MmExOTgyZjExID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2QxNzkzYTZhYzIyNjRkNzU5YjI1M2I4ZWIxMTBkMzBlID0gJCgnPGRpdiBpZD0iaHRtbF9kMTc5M2E2YWMyMjY0ZDc1OWIyNTNiOGViMTEwZDMwZSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MjM6IE1JU1NJT04gU1QgJiBPTElWRVIgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2IzNmRlZTQ3MmJlMTQ1MDk4NDNhNzY4MmExOTgyZjExLnNldENvbnRlbnQoaHRtbF9kMTc5M2E2YWMyMjY0ZDc1OWIyNTNiOGViMTEwZDMwZSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2FkMWFjNWIzNmFiZDQzODRhY2Y5MTcxNjU0ZmM4ZThjLmJpbmRQb3B1cChwb3B1cF9iMzZkZWU0NzJiZTE0NTA5ODQzYTc2ODJhMTk4MmYxMSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lMjU0NjhiNmVhODY0NGRkYWE4MTc2OWNlNzk5NzQ3NyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc5MzIzMTcwMDAwMDAxLC0xMjIuMzkzMjA0N10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzk3OTVlMjRhOGM2MDRmOTliNTM4MDg1NTM2OGMxODU5ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzgwOTI4YjMwMWYyNTRlM2I4NDk0MTE3ODU3NGI3ZGVjID0gJCgnPGRpdiBpZD0iaHRtbF84MDkyOGIzMDFmMjU0ZTNiODQ5NDExNzg1NzRiN2RlYyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MjQ6IE1JU1NJT04gU1QgJiBTVEVVQVJUIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF85Nzk1ZTI0YThjNjA0Zjk5YjUzODA4NTUzNjhjMTg1OS5zZXRDb250ZW50KGh0bWxfODA5MjhiMzAxZjI1NGUzYjg0OTQxMTc4NTc0YjdkZWMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9lMjU0NjhiNmVhODY0NGRkYWE4MTc2OWNlNzk5NzQ3Ny5iaW5kUG9wdXAocG9wdXBfOTc5NWUyNGE4YzYwNGY5OWI1MzgwODU1MzY4YzE4NTkpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZTRlY2Y0MDZjYjBiNDE3MWE4NjVlMzgyZjk4Yjc0YzIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MDgzMDUyMDAwMDAwMDUsLTEyMi40NTQxOTM2XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfOTU4MDVmYTg5YmY1NDc2YTg0MWNiZDU3MmZlODg5M2IgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMmE5ZjlhMTlhNzZmNDBmN2E5ODU5ZGU2NzE2MmZlMDggPSAkKCc8ZGl2IGlkPSJodG1sXzJhOWY5YTE5YTc2ZjQwZjdhOTg1OWRlNjcxNjJmZTA4IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4yNTogTUlTU0lPTiBTVCAmIEhVUk9OIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfOTU4MDVmYTg5YmY1NDc2YTg0MWNiZDU3MmZlODg5M2Iuc2V0Q29udGVudChodG1sXzJhOWY5YTE5YTc2ZjQwZjdhOTg1OWRlNjcxNjJmZTA4KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfZTRlY2Y0MDZjYjBiNDE3MWE4NjVlMzgyZjk4Yjc0YzIuYmluZFBvcHVwKHBvcHVwXzk1ODA1ZmE4OWJmNTQ3NmE4NDFjYmQ1NzJmZTg4OTNiKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzQ5MjY2NWUwODQ3MTQ0Mzk5YjBjNDYyZmI3ZDdiYWNjID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzYyNzEzODk5OTk5OTk0LC0xMjIuNDE5NDcwNV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzlkMTUyM2VmOGQxYjQ2NTY4OTIyNzFmYjRlYWJhNjk4ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2FmYjIxNmEzYzkxZjRmNzg4MzgyNzU5OTY2YWQ4NDQxID0gJCgnPGRpdiBpZD0iaHRtbF9hZmIyMTZhM2M5MWY0Zjc4ODM4Mjc1OTk2NmFkODQ0MSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MjY6IE1JU1NJT04gU1QgJiBTWUNBTU9SRSBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfOWQxNTIzZWY4ZDFiNDY1Njg5MjI3MWZiNGVhYmE2OTguc2V0Q29udGVudChodG1sX2FmYjIxNmEzYzkxZjRmNzg4MzgyNzU5OTY2YWQ4NDQxKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfNDkyNjY1ZTA4NDcxNDQzOTliMGM0NjJmYjdkN2JhY2MuYmluZFBvcHVwKHBvcHVwXzlkMTUyM2VmOGQxYjQ2NTY4OTIyNzFmYjRlYWJhNjk4KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2Q3MWRlOTU5NjAxMTRhNTY5NDgzNDJjMDljM2VlMTQ3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzg3ODM1Nzk5OTk5OTk2LC0xMjIuNDAwMjAxM10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzA3YTZlMjJhNWNlOTRlMmI4OTQ5MjNiZGZhNTljZmI3ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzVkMmI2ZmEwYTI3ODRlYmJiNWRhNmY0OGNkYTc2NmViID0gJCgnPGRpdiBpZD0iaHRtbF81ZDJiNmZhMGEyNzg0ZWJiYjVkYTZmNDhjZGE3NjZlYiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+Mjc6IE1JU1NJT04gU1QgJiAwMk5EIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8wN2E2ZTIyYTVjZTk0ZTJiODk0OTIzYmRmYTU5Y2ZiNy5zZXRDb250ZW50KGh0bWxfNWQyYjZmYTBhMjc4NGViYmI1ZGE2ZjQ4Y2RhNzY2ZWIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9kNzFkZTk1OTYwMTE0YTU2OTQ4MzQyYzA5YzNlZTE0Ny5iaW5kUG9wdXAocG9wdXBfMDdhNmUyMmE1Y2U5NGUyYjg5NDkyM2JkZmE1OWNmYjcpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNzc3OTUwNDI1MjU4NDhjODkyZWI5YmZhOGQ1MjVmMWIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODIyMTE3MDAwMDAwMDUsLTEyMi40MDcxNjU1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfY2M0NDZmNTVjNWUxNDllMmE3N2RkZDJiZjg2OTllZDEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMGE2NjlkZjA1NWY4NDg4M2I5MDdjY2E1OGExNWYyNmEgPSAkKCc8ZGl2IGlkPSJodG1sXzBhNjY5ZGYwNTVmODQ4ODNiOTA3Y2NhNThhMTVmMjZhIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4yODogTUlTU0lPTiBTVCAmIE1JTlQgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2NjNDQ2ZjU1YzVlMTQ5ZTJhNzdkZGQyYmY4Njk5ZWQxLnNldENvbnRlbnQoaHRtbF8wYTY2OWRmMDU1Zjg0ODgzYjkwN2NjYTU4YTE1ZjI2YSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzc3Nzk1MDQyNTI1ODQ4Yzg5MmViOWJmYThkNTI1ZjFiLmJpbmRQb3B1cChwb3B1cF9jYzQ0NmY1NWM1ZTE0OWUyYTc3ZGRkMmJmODY5OWVkMSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9iMzU3NjJhNGQyMTE0M2E3OWM5ZmEzMDA4MzhhY2QyZSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxNjQwNzM5OTk5OTk5NCwtMTIyLjQ0MTA4NjU5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNTMxNjA2MjkxOWU0NGI1ODk1NGVmZjVhOTI4NGY4ZWUgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMjRhMGVhMDI3NjIyNGFiMWExNzkxY2M5YjgwYmVjZGIgPSAkKCc8ZGl2IGlkPSJodG1sXzI0YTBlYTAyNzYyMjRhYjFhMTc5MWNjOWI4MGJlY2RiIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4yOTogTUlTU0lPTiBTVCAmIEdFTkVWQSBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzUzMTYwNjI5MTllNDRiNTg5NTRlZmY1YTkyODRmOGVlLnNldENvbnRlbnQoaHRtbF8yNGEwZWEwMjc2MjI0YWIxYTE3OTFjYzliODBiZWNkYik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2IzNTc2MmE0ZDIxMTQzYTc5YzlmYTMwMDgzOGFjZDJlLmJpbmRQb3B1cChwb3B1cF81MzE2MDYyOTE5ZTQ0YjU4OTU0ZWZmNWE5Mjg0ZjhlZSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9hZDc4ZDM0NjU0MDU0YWMyYjNmZjg2ZWQwNjI4NTU2ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0MTAzNzUsLTEyMi40MjI4NTMxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYzU4ZjVhMzM0YjE2NDVlYWJkOTBkNTRkZGIxOWFkZTUgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMmU1NjQ2MjA3MWM3NDQxMzhkYWNjMjMyZjUxMzc5MWMgPSAkKCc8ZGl2IGlkPSJodG1sXzJlNTY0NjIwNzFjNzQ0MTM4ZGFjYzIzMmY1MTM3OTFjIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4zMDogTUlTU0lPTiBTVCAmIENPUlRMQU5EIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYzU4ZjVhMzM0YjE2NDVlYWJkOTBkNTRkZGIxOWFkZTUuc2V0Q29udGVudChodG1sXzJlNTY0NjIwNzFjNzQ0MTM4ZGFjYzIzMmY1MTM3OTFjKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfYWQ3OGQzNDY1NDA1NGFjMmIzZmY4NmVkMDYyODU1NmYuYmluZFBvcHVwKHBvcHVwX2M1OGY1YTMzNGIxNjQ1ZWFiZDkwZDU0ZGRiMTlhZGU1KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzEyNWU3NjRmYzI0OTQ2NmU5NzhhYjNiOGRmYTMxMmQ4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzU1NDQ0NzAwMDAwMDA1LC0xMjIuNDE4Nzc2MDk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9kZTMxMjk5ZWQzMDk0Y2U3ODI1NzFlZTNjZjEwMTI0ZiA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF83OTBiYzQ5NGM1NTc0NmJlOTNiNjQxNGQ5ZDM4MmFmZSA9ICQoJzxkaXYgaWQ9Imh0bWxfNzkwYmM0OTRjNTU3NDZiZTkzYjY0MTRkOWQzODJhZmUiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjMxOiBNSVNTSU9OIFNUICYgMjJORCBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfZGUzMTI5OWVkMzA5NGNlNzgyNTcxZWUzY2YxMDEyNGYuc2V0Q29udGVudChodG1sXzc5MGJjNDk0YzU1NzQ2YmU5M2I2NDE0ZDlkMzgyYWZlKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMTI1ZTc2NGZjMjQ5NDY2ZTk3OGFiM2I4ZGZhMzEyZDguYmluZFBvcHVwKHBvcHVwX2RlMzEyOTllZDMwOTRjZTc4MjU3MWVlM2NmMTAxMjRmKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzhhNTM5YTRmN2IxMjQxMTE4YTcwOTg4OGU0NTFkMGNkID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzgwNzk1NiwtMTIyLjQwOTExMzQwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfY2QxZDc4YmMzMjg1NDUyZThiMTNlYWY2ZGNmNjFjZjEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMGNiYzM5MDZhMjQyNDc5NGExZGVmYTViMjU0YTFjOWMgPSAkKCc8ZGl2IGlkPSJodG1sXzBjYmMzOTA2YTI0MjQ3OTRhMWRlZmE1YjI1NGExYzljIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4zMjogTUlTU0lPTiBTVCAmIDA2VEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2NkMWQ3OGJjMzI4NTQ1MmU4YjEzZWFmNmRjZjYxY2YxLnNldENvbnRlbnQoaHRtbF8wY2JjMzkwNmEyNDI0Nzk0YTFkZWZhNWIyNTRhMWM5Yyk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzhhNTM5YTRmN2IxMjQxMTE4YTcwOTg4OGU0NTFkMGNkLmJpbmRQb3B1cChwb3B1cF9jZDFkNzhiYzMyODU0NTJlOGIxM2VhZjZkY2Y2MWNmMSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8zNzgyMzkzODM4Yjk0MzA5YjQyMTBkYzU3MmExNGJlNSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0Njc4MTQsLTEyMi40MTkxNDE3MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzFhN2UzNTEyZTRhZjQwZmZiYjU1ZDQzYWI5MDk5M2Y4ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzg5ZTEwYmVjYWMzMzRiZTVhYWQ5Y2JlZTA4ZTBmNWZiID0gJCgnPGRpdiBpZD0iaHRtbF84OWUxMGJlY2FjMzM0YmU1YWFkOWNiZWUwOGUwZjVmYiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MzM6IE1JU1NJT04gU1QgJiBQUkVDSVRBIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfMWE3ZTM1MTJlNGFmNDBmZmJiNTVkNDNhYjkwOTkzZjguc2V0Q29udGVudChodG1sXzg5ZTEwYmVjYWMzMzRiZTVhYWQ5Y2JlZTA4ZTBmNWZiKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMzc4MjM5MzgzOGI5NDMwOWI0MjEwZGM1NzJhMTRiZTUuYmluZFBvcHVwKHBvcHVwXzFhN2UzNTEyZTRhZjQwZmZiYjU1ZDQzYWI5MDk5M2Y4KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzdmZDlkNGMxZTExODQ3MGFhMzNkZmE2MmVhOGYxNTVhID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzYzNDQ0NiwtMTIyLjQxOTU0MjJdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8wODE1MTY1N2JiYmM0NTM4OGI0MTliNTcwY2IyZTRmOCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8zMTcxNDA1NzQwYjQ0ZjEwYWMyMmJiODllYWJmNDVkZCA9ICQoJzxkaXYgaWQ9Imh0bWxfMzE3MTQwNTc0MGI0NGYxMGFjMjJiYjg5ZWFiZjQ1ZGQiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjM0OiBNSVNTSU9OIFNUICYgMTdUSCBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfMDgxNTE2NTdiYmJjNDUzODhiNDE5YjU3MGNiMmU0Zjguc2V0Q29udGVudChodG1sXzMxNzE0MDU3NDBiNDRmMTBhYzIyYmI4OWVhYmY0NWRkKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfN2ZkOWQ0YzFlMTE4NDcwYWEzM2RmYTYyZWE4ZjE1NWEuYmluZFBvcHVwKHBvcHVwXzA4MTUxNjU3YmJiYzQ1Mzg4YjQxOWI1NzBjYjJlNGY4KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzljOTNlNjM4OWM2NTRjMmZiYWZiZWY5NTM3OWY5ODE3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI5MjMwNywtMTIyLjQzMDg0NDddLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8xM2Q4NmU4N2E2MWI0NTIyYWJjMzg1NzFiMzUxZDNkYyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF85ZTQwNWNkMzA0N2Q0MDM1YjdkNTRjYWM4ZjMwODIyYyA9ICQoJzxkaXYgaWQ9Imh0bWxfOWU0MDVjZDMwNDdkNDAzNWI3ZDU0Y2FjOGYzMDgyMmMiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjM1OiBNSVNTSU9OIFNUICYgQ0FTVExFIE1BTk9SIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfMTNkODZlODdhNjFiNDUyMmFiYzM4NTcxYjM1MWQzZGMuc2V0Q29udGVudChodG1sXzllNDA1Y2QzMDQ3ZDQwMzViN2Q1NGNhYzhmMzA4MjJjKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfOWM5M2U2Mzg5YzY1NGMyZmJhZmJlZjk1Mzc5Zjk4MTcuYmluZFBvcHVwKHBvcHVwXzEzZDg2ZTg3YTYxYjQ1MjJhYmMzODU3MWIzNTFkM2RjKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2MwMzExMTNmOWNhZjRkMDBhZjRhNjMzNjFiODUzZjNmID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzE0NDgsLTEyMi40NDI2NF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzUwOTJlMWJlZDE4NzQwNTQ5ODZlODlhMTljZDI5MjI2ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzVmYjU4YTAzNWUzMjQyNTJiZjRkZWYwZGM5ODYwODk3ID0gJCgnPGRpdiBpZD0iaHRtbF81ZmI1OGEwMzVlMzI0MjUyYmY0ZGVmMGRjOTg2MDg5NyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MzY6IE1JU1NJT04gU1QgJiBBTExJU09OIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF81MDkyZTFiZWQxODc0MDU0OTg2ZTg5YTE5Y2QyOTIyNi5zZXRDb250ZW50KGh0bWxfNWZiNThhMDM1ZTMyNDI1MmJmNGRlZjBkYzk4NjA4OTcpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9jMDMxMTEzZjljYWY0ZDAwYWY0YTYzMzYxYjg1M2YzZi5iaW5kUG9wdXAocG9wdXBfNTA5MmUxYmVkMTg3NDA1NDk4NmU4OWExOWNkMjkyMjYpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfM2JiYWRkMzUzYmQ3NDIyMmEwYTIyNWM4ZDdiNzI3YWUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDkwNDQsLTEyMi40MTgxNjU5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMTBjNDBjZWVlODdhNGUxNzkxM2Y4NjYxNGU1ODQxZjggPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfODZjMGJkMDNhNDhjNGY0MWJjNDIxZmRiZWEzYWRjZmYgPSAkKCc8ZGl2IGlkPSJodG1sXzg2YzBiZDAzYTQ4YzRmNDFiYzQyMWZkYmVhM2FkY2ZmIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4zNzogTUlTU0lPTiBTVCAmIDI2VEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzEwYzQwY2VlZTg3YTRlMTc5MTNmODY2MTRlNTg0MWY4LnNldENvbnRlbnQoaHRtbF84NmMwYmQwM2E0OGM0ZjQxYmM0MjFmZGJlYTNhZGNmZik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzNiYmFkZDM1M2JkNzQyMjJhMGEyMjVjOGQ3YjcyN2FlLmJpbmRQb3B1cChwb3B1cF8xMGM0MGNlZWU4N2E0ZTE3OTEzZjg2NjE0ZTU4NDFmOCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lMzc0OGZkZGQ2MzI0OGE4OWQ0MjdiZWIxODYxOTUyZSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0MzEzOSwtMTIyLjQyMTQ5NzFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF85MjcxZGVmNjI0MWE0YjVmYTE0ZWM5MWZhZGZmMjAzMyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF84ZmJmNWJhOGM1OTY0Y2VhODQ4NTMwNzRjMmM0YjJkYyA9ICQoJzxkaXYgaWQ9Imh0bWxfOGZiZjViYThjNTk2NGNlYTg0ODUzMDc0YzJjNGIyZGMiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjM4OiBNSVNTSU9OIFNUICYgVklSR0lOSUEgQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF85MjcxZGVmNjI0MWE0YjVmYTE0ZWM5MWZhZGZmMjAzMy5zZXRDb250ZW50KGh0bWxfOGZiZjViYThjNTk2NGNlYTg0ODUzMDc0YzJjNGIyZGMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9lMzc0OGZkZGQ2MzI0OGE4OWQ0MjdiZWIxODYxOTUyZS5iaW5kUG9wdXAocG9wdXBfOTI3MWRlZjYyNDFhNGI1ZmExNGVjOTFmYWRmZjIwMzMpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNmFiNTQ4M2Y1MDBlNDQ1ZmI2NmM5NjEzNjVmNjliODAgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjcxODIyLC0xMjIuNDMyOTM1Mjk5OTk5OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF83OTE1MjQ5ODlhZGQ0Mjc3OGYyNGIyN2I0YzdkOWFjMyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9iZTFmNzU1OTBjNjQ0MDc1OGRiYzJmZDEyMTc3YmQ4MiA9ICQoJzxkaXYgaWQ9Imh0bWxfYmUxZjc1NTkwYzY0NDA3NThkYmMyZmQxMjE3N2JkODIiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjM5OiBNSVNTSU9OIFNUICYgQ09UVEVSIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF83OTE1MjQ5ODlhZGQ0Mjc3OGYyNGIyN2I0YzdkOWFjMy5zZXRDb250ZW50KGh0bWxfYmUxZjc1NTkwYzY0NDA3NThkYmMyZmQxMjE3N2JkODIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl82YWI1NDgzZjUwMGU0NDVmYjY2Yzk2MTM2NWY2OWI4MC5iaW5kUG9wdXAocG9wdXBfNzkxNTI0OTg5YWRkNDI3NzhmMjRiMjdiNGM3ZDlhYzMpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMGE2Njc2MDM4MDY3NDkwZmEzNjE5YmYwM2E3MDJlY2IgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTA3NTM0LC0xMjIuNDQ3NzA3NF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzg2MjUyODkzNmY0MDQ3MzViODA2N2QwMDA2MThmYTBhID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzM3YzRiM2Y3MzBhNjQzMDQ4ZTU2YThiZWQyNjM5YjgzID0gJCgnPGRpdiBpZD0iaHRtbF8zN2M0YjNmNzMwYTY0MzA0OGU1NmE4YmVkMjYzOWI4MyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NDA6IE1JU1NJT04gU1QgJiBXSElQUExFIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfODYyNTI4OTM2ZjQwNDczNWI4MDY3ZDAwMDYxOGZhMGEuc2V0Q29udGVudChodG1sXzM3YzRiM2Y3MzBhNjQzMDQ4ZTU2YThiZWQyNjM5YjgzKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMGE2Njc2MDM4MDY3NDkwZmEzNjE5YmYwM2E3MDJlY2IuYmluZFBvcHVwKHBvcHVwXzg2MjUyODkzNmY0MDQ3MzViODA2N2QwMDA2MThmYTBhKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzExOGNjNzk0NzU5NzQzZGI4Yzg4ZjUxM2QwYTcwODkxID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzc3MzI5MzAwMDAwMDEsLTEyMi40MTM1MDM3XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfOWVlZGZhOTQyMDY3NDkwZGI3OTQwYmE2MDU2ZTIzNDAgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfOTRmNGM2MGE2NjU0NDIwYmFhY2UwMTNhNWViNDZhZDEgPSAkKCc8ZGl2IGlkPSJodG1sXzk0ZjRjNjBhNjY1NDQyMGJhYWNlMDEzYTVlYjQ2YWQxIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij40MTogTUlTU0lPTiBTVCAmIDA4VEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzllZWRmYTk0MjA2NzQ5MGRiNzk0MGJhNjA1NmUyMzQwLnNldENvbnRlbnQoaHRtbF85NGY0YzYwYTY2NTQ0MjBiYWFjZTAxM2E1ZWI0NmFkMSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzExOGNjNzk0NzU5NzQzZGI4Yzg4ZjUxM2QwYTcwODkxLmJpbmRQb3B1cChwb3B1cF85ZWVkZmE5NDIwNjc0OTBkYjc5NDBiYTYwNTZlMjM0MCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wMDgzZmYwZWMxMGM0ZDZkODI1NDg5OWFiMzkxNDVhYyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3MTAxLC0xMjIuNDE5NjYzNF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2FlNjRhOTI4NzU3NjQzMWY5OGRhYjAwMmIwNGE0ZjU2ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzIzOTY3NzBkMWUwNTQwYTRiODMxYjA5ODRjNjhiZTk1ID0gJCgnPGRpdiBpZD0iaHRtbF8yMzk2NzcwZDFlMDU0MGE0YjgzMWIwOTg0YzY4YmU5NSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NDI6IE1JU1NJT04gU1QgJiBQTFVNIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9hZTY0YTkyODc1NzY0MzFmOThkYWIwMDJiMDRhNGY1Ni5zZXRDb250ZW50KGh0bWxfMjM5Njc3MGQxZTA1NDBhNGI4MzFiMDk4NGM2OGJlOTUpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8wMDgzZmYwZWMxMGM0ZDZkODI1NDg5OWFiMzkxNDVhYy5iaW5kUG9wdXAocG9wdXBfYWU2NGE5Mjg3NTc2NDMxZjk4ZGFiMDAyYjA0YTRmNTYpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMzhiN2Y5NzNlZDY0NDAzYWFhNTZmMDIxYTI2ZjRlNGQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43OTM4NDMzLC0xMjIuMzkyNDQxOV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzgwODAxODM5NWZjZjQ1ZmNiNTU1MTQ0MWNlNDU3NGY5ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzdlMmNkODY3MmM5ZjQ4NDVhNjBkOWI2ZjVmMzAzODg4ID0gJCgnPGRpdiBpZD0iaHRtbF83ZTJjZDg2NzJjOWY0ODQ1YTYwZDliNmY1ZjMwMzg4OCIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NDM6IE1JU1NJT04gU1QgJiBUSEUgRU1CQVJDQURFUk88L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzgwODAxODM5NWZjZjQ1ZmNiNTU1MTQ0MWNlNDU3NGY5LnNldENvbnRlbnQoaHRtbF83ZTJjZDg2NzJjOWY0ODQ1YTYwZDliNmY1ZjMwMzg4OCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzM4YjdmOTczZWQ2NDQwM2FhYTU2ZjAyMWEyNmY0ZTRkLmJpbmRQb3B1cChwb3B1cF84MDgwMTgzOTVmY2Y0NWZjYjU1NTE0NDFjZTQ1NzRmOSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9mYmVhNWU5MjI0M2U0YmY0YWU5YThjNmU3OWY0MTA3NiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc2NDc5NjIwMDAwMDAwNiwtMTIyLjQxOTk4ODk5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMWZhZjg3MzU4ZjQxNDJiOWIzYzQ4YWZjYTk2OTBiOTIgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMzBhZjBiNTA2Mzk0NDk0YWFhYjg0NTQwNzRkOGQ4MzAgPSAkKCc8ZGl2IGlkPSJodG1sXzMwYWYwYjUwNjM5NDQ5NGFhYWI4NDU0MDc0ZDhkODMwIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij40NDogTUlTU0lPTiBTVCAmIDE5VEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzFmYWY4NzM1OGY0MTQyYjliM2M0OGFmY2E5NjkwYjkyLnNldENvbnRlbnQoaHRtbF8zMGFmMGI1MDYzOTQ0OTRhYWFiODQ1NDA3NGQ4ZDgzMCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2ZiZWE1ZTkyMjQzZTRiZjRhZTlhOGM2ZTc5ZjQxMDc2LmJpbmRQb3B1cChwb3B1cF8xZmFmODczNThmNDE0MmI5YjNjNDhhZmNhOTY5MGI5Mik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8xNTNiMmMyOGE4NjI0ODQyOGY3MmU3ZmQ1NGE2ZGE0MSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczODg0MzUsLTEyMi40MjM5NzkxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfZjFiOTBhYjQ2YjlmNDg0YmIzNjIyMTZiODBjODM5OWMgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfZmJkNmZhNDQxOGZjNGYwMGI4ZTY3MTdlNzQ2ZTllYzQgPSAkKCc8ZGl2IGlkPSJodG1sX2ZiZDZmYTQ0MThmYzRmMDBiOGU2NzE3ZTc0NmU5ZWM0IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij40NTogTUlTU0lPTiBTVCAmIEFQUExFVE9OIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfZjFiOTBhYjQ2YjlmNDg0YmIzNjIyMTZiODBjODM5OWMuc2V0Q29udGVudChodG1sX2ZiZDZmYTQ0MThmYzRmMDBiOGU2NzE3ZTc0NmU5ZWM0KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMTUzYjJjMjhhODYyNDg0MjhmNzJlN2ZkNTRhNmRhNDEuYmluZFBvcHVwKHBvcHVwX2YxYjkwYWI0NmI5ZjQ4NGJiMzYyMjE2YjgwYzgzOTljKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2Y4NGM4YjIzMjYxYTQ5YTNhOTAxOWNjZDQ0MjZkMjUyID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzIzOTAyLC0xMjIuNDM1NDAyMjAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF80Y2E0ZjkzMGZkMTk0ZmQwYTE5NzMxM2Y4YTcyNGNhNSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF85OGMyMjY2YzhhY2I0MjAzYWQ0YWFiOTE3MDhiODg4MyA9ICQoJzxkaXYgaWQ9Imh0bWxfOThjMjI2NmM4YWNiNDIwM2FkNGFhYjkxNzA4Yjg4ODMiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjQ2OiBNSVNTSU9OIFNUICYgT0NFQU4gQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF80Y2E0ZjkzMGZkMTk0ZmQwYTE5NzMxM2Y4YTcyNGNhNS5zZXRDb250ZW50KGh0bWxfOThjMjI2NmM4YWNiNDIwM2FkNGFhYjkxNzA4Yjg4ODMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9mODRjOGIyMzI2MWE0OWEzYTkwMTljY2Q0NDI2ZDI1Mi5iaW5kUG9wdXAocG9wdXBfNGNhNGY5MzBmZDE5NGZkMGExOTczMTNmOGE3MjRjYTUpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfOTRkOTlhMWU3MDI4NGJkN2I3MTFkYzllYjVjYzk0YmIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDI4MzEyMDAwMDAwMDUsLTEyMi40MjE2OTU0MDAwMDAwMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2I4M2QwMTI1NGY0MTQ2MmNhNjk5NWExOTlhYjBlYWE5ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2I2NDg0MjIwZjFjZjRmMzA5OTVhNGEyODg2OTIxZjc5ID0gJCgnPGRpdiBpZD0iaHRtbF9iNjQ4NDIyMGYxY2Y0ZjMwOTk1YTRhMjg4NjkyMWY3OSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NDc6IE1JU1NJT04gU1QgJiBHT0RFVVMgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2I4M2QwMTI1NGY0MTQ2MmNhNjk5NWExOTlhYjBlYWE5LnNldENvbnRlbnQoaHRtbF9iNjQ4NDIyMGYxY2Y0ZjMwOTk1YTRhMjg4NjkyMWY3OSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzk0ZDk5YTFlNzAyODRiZDdiNzExZGM5ZWI1Y2M5NGJiLmJpbmRQb3B1cChwb3B1cF9iODNkMDEyNTRmNDE0NjJjYTY5OTVhMTk5YWIwZWFhOSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lMGVkNzQyYTVhYmQ0MGY1ODZlNmRiODY3M2ExODM4ZSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxNTQ0NjQsLTEyMi40NDE4MzM2XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfZjNjMzhhZGQ1ZTRkNGQwMmFiM2ZhYjNhNGU1NGMzZWEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfZTQyNjZlYWZkZjBiNGJkNDk0YmEyYmY0MWZjNmQ5OGMgPSAkKCc8ZGl2IGlkPSJodG1sX2U0MjY2ZWFmZGYwYjRiZDQ5NGJhMmJmNDFmYzZkOThjIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij40ODogTUlTU0lPTiBTVCAmIFBPUEUgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2YzYzM4YWRkNWU0ZDRkMDJhYjNmYWIzYTRlNTRjM2VhLnNldENvbnRlbnQoaHRtbF9lNDI2NmVhZmRmMGI0YmQ0OTRiYTJiZjQxZmM2ZDk4Yyk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2UwZWQ3NDJhNWFiZDQwZjU4NmU2ZGI4NjczYTE4MzhlLmJpbmRQb3B1cChwb3B1cF9mM2MzOGFkZDVlNGQ0ZDAyYWIzZmFiM2E0ZTU0YzNlYSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wMDMxZmNiYTdkZTQ0MWEyYjEyOTA2ZjYwZDExODI5ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcwOTk1OTAwMDAwMDAwNSwtMTIyLjQ0OTM1NF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2E2NDk5NDU5YzkwZDRjOThiODJlYjQ2MGZkYjIwMDZmID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2ZmOWY5YjljZjFiZTRiOWRhNTFhMDM5NGI3NTNlZTNhID0gJCgnPGRpdiBpZD0iaHRtbF9mZjlmOWI5Y2YxYmU0YjlkYTUxYTAzOTRiNzUzZWUzYSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NDk6IE1JU1NJT04gU1QgJiBGQVJSQUdVVCBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2E2NDk5NDU5YzkwZDRjOThiODJlYjQ2MGZkYjIwMDZmLnNldENvbnRlbnQoaHRtbF9mZjlmOWI5Y2YxYmU0YjlkYTUxYTAzOTRiNzUzZWUzYSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzAwMzFmY2JhN2RlNDQxYTJiMTI5MDZmNjBkMTE4MjlmLmJpbmRQb3B1cChwb3B1cF9hNjQ5OTQ1OWM5MGQ0Yzk4YjgyZWI0NjBmZGIyMDA2Zik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl82OWEwODc2YmJhZGU0Nzk1OTYxMjJjNTU1ZTYzZTY2NyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3NDMzMjUsLTEyMi40MTcxMzcyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMDgzYWExMTE0ZDc4NDc2NjljOGUzOGU2YzkxZGNiOTMgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYWEyMGY3MGM2ODkzNGZmNzliODhhYmQxNDk4NjIzOTAgPSAkKCc8ZGl2IGlkPSJodG1sX2FhMjBmNzBjNjg5MzRmZjc5Yjg4YWJkMTQ5ODYyMzkwIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij41MDogTUlTU0lPTiBTVCAmIDExVEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzA4M2FhMTExNGQ3ODQ3NjY5YzhlMzhlNmM5MWRjYjkzLnNldENvbnRlbnQoaHRtbF9hYTIwZjcwYzY4OTM0ZmY3OWI4OGFiZDE0OTg2MjM5MCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzY5YTA4NzZiYmFkZTQ3OTU5NjEyMmM1NTVlNjNlNjY3LmJpbmRQb3B1cChwb3B1cF8wODNhYTExMTRkNzg0NzY2OWM4ZTM4ZTZjOTFkY2I5Myk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9hOGVmMTMwYmFlYmY0NDFjYTU1M2NjY2ZkYjdhNTkyMCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0MjIwNzQsLTEyMi40MjIwOTcyMDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzYxNWRlOWUzM2E1ZjQ3YWFiNWU4YjM0MTJjZDMzY2RkID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzMwYmU0NTE2MDgwNzQ4NTRhYWIyNWJiYjBmNTMyYjIyID0gJCgnPGRpdiBpZD0iaHRtbF8zMGJlNDUxNjA4MDc0ODU0YWFiMjViYmIwZjUzMmIyMiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NTE6IE1JU1NJT04gU1QgJiBFVUdFTklBIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfNjE1ZGU5ZTMzYTVmNDdhYWI1ZThiMzQxMmNkMzNjZGQuc2V0Q29udGVudChodG1sXzMwYmU0NTE2MDgwNzQ4NTRhYWIyNWJiYjBmNTMyYjIyKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfYThlZjEzMGJhZWJmNDQxY2E1NTNjY2NmZGI3YTU5MjAuYmluZFBvcHVwKHBvcHVwXzYxNWRlOWUzM2E1ZjQ3YWFiNWU4YjM0MTJjZDMzY2RkKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzBlZGQ2M2I2NDAzNjQxYmU4YzA5NWI4M2MxYWM0ZDBhID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzg0NjQsLTEyMi40MDM5M10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzc4MjQ3MDJkYWZkZTQ0YzA4OTMyNjMxYWVmMzhmYjhhID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzRjNDk1M2IwMGVjZDQ4ZGViMTMxYTFkZDNhZWY3YThkID0gJCgnPGRpdiBpZD0iaHRtbF80YzQ5NTNiMDBlY2Q0OGRlYjEzMWExZGQzYWVmN2E4ZCIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NTI6IE1JU1NJT04gU1QgJiAwNFRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF83ODI0NzAyZGFmZGU0NGMwODkzMjYzMWFlZjM4ZmI4YS5zZXRDb250ZW50KGh0bWxfNGM0OTUzYjAwZWNkNDhkZWIxMzFhMWRkM2FlZjdhOGQpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8wZWRkNjNiNjQwMzY0MWJlOGMwOTViODNjMWFjNGQwYS5iaW5kUG9wdXAocG9wdXBfNzgyNDcwMmRhZmRlNDRjMDg5MzI2MzFhZWYzOGZiOGEpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYTUxYzhlMDcyZjhkNGQ0ZDg1ODBiN2YzZDE4ZTZiNTQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjAwMzY3LC0xMjIuNDM4MzUwNDk5OTk5OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8yMWJkNmU2ODgwN2Q0ZGJhOTgzNzU4OWUzMTQ4OTFlNSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8zZDcxZjkzY2U0Zjk0MGQ2YjM1YTI0MGIzYmUzZmE4NyA9ICQoJzxkaXYgaWQ9Imh0bWxfM2Q3MWY5M2NlNGY5NDBkNmIzNWEyNDBiM2JlM2ZhODciIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjUzOiBNSVNTSU9OIFNUICYgRlJBTkNFIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfMjFiZDZlNjg4MDdkNGRiYTk4Mzc1ODllMzE0ODkxZTUuc2V0Q29udGVudChodG1sXzNkNzFmOTNjZTRmOTQwZDZiMzVhMjQwYjNiZTNmYTg3KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfYTUxYzhlMDcyZjhkNGQ0ZDg1ODBiN2YzZDE4ZTZiNTQuYmluZFBvcHVwKHBvcHVwXzIxYmQ2ZTY4ODA3ZDRkYmE5ODM3NTg5ZTMxNDg5MWU1KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzliOTk4YjA0N2Y0NjRhY2E4OTk1MGI3MDdkYmE2NzBiID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzc2NzEzNCwtMTIyLjQxNDEyMzldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9kNmI5ZjNmMWExNGY0YWMxYmRlZTc0ZTU5OTBlODkyNCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9jMGQ1MWEzMDg4MTc0NjI1OGZhMDNhM2VkNzU3MGE5ZiA9ICQoJzxkaXYgaWQ9Imh0bWxfYzBkNTFhMzA4ODE3NDYyNThmYTAzYTNlZDc1NzBhOWYiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjU0OiBNSVNTSU9OIFNUICYgTEFTS0lFIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9kNmI5ZjNmMWExNGY0YWMxYmRlZTc0ZTU5OTBlODkyNC5zZXRDb250ZW50KGh0bWxfYzBkNTFhMzA4ODE3NDYyNThmYTAzYTNlZDc1NzBhOWYpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl85Yjk5OGIwNDdmNDY0YWNhODk5NTBiNzA3ZGJhNjcwYi5iaW5kUG9wdXAocG9wdXBfZDZiOWYzZjFhMTRmNGFjMWJkZWU3NGU1OTkwZTg5MjQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNGFhMWJjYTgyNWM4NGFjMWJjZDY0MWFkMDdjOThiM2YgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDQwMzgsLTEyMi40MjA5MTUwOTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2I3ODU0M2EyNTI0YjQ4M2FiNDhjNTEyMjdkY2Q3MTRhID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2I2NjQ1MDBlZjI3MTQwZGY4NzBmNWRjNzc1YzA3NzM3ID0gJCgnPGRpdiBpZD0iaHRtbF9iNjY0NTAwZWYyNzE0MGRmODcwZjVkYzc3NWMwNzczNyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NTU6IE1JU1NJT04gU1QgJiAyOVRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9iNzg1NDNhMjUyNGI0ODNhYjQ4YzUxMjI3ZGNkNzE0YS5zZXRDb250ZW50KGh0bWxfYjY2NDUwMGVmMjcxNDBkZjg3MGY1ZGM3NzVjMDc3MzcpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl80YWExYmNhODI1Yzg0YWMxYmNkNjQxYWQwN2M5OGIzZi5iaW5kUG9wdXAocG9wdXBfYjc4NTQzYTI1MjRiNDgzYWI0OGM1MTIyN2RjZDcxNGEpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfODBlZDU5NjU3NGE4NGFiNWIwMDVmMjBjMWM0ZTFmYmEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjE1ODA4LC0xMjIuNDM3MTc2NTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF84NTUzZmE0MTlhMjQ0MGEwYjczMzkzODYzODBlNzk3MCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8xNDIzNGU2MjA0M2I0ZjVhYTc2Zjk2ZGZhYzM1YTRjZCA9ICQoJzxkaXYgaWQ9Imh0bWxfMTQyMzRlNjIwNDNiNGY1YWE3NmY5NmRmYWMzNWE0Y2QiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjU2OiBNSVNTSU9OIFNUICYgUlVTU0lBIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfODU1M2ZhNDE5YTI0NDBhMGI3MzM5Mzg2MzgwZTc5NzAuc2V0Q29udGVudChodG1sXzE0MjM0ZTYyMDQzYjRmNWFhNzZmOTZkZmFjMzVhNGNkKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfODBlZDU5NjU3NGE4NGFiNWIwMDVmMjBjMWM0ZTFmYmEuYmluZFBvcHVwKHBvcHVwXzg1NTNmYTQxOWEyNDQwYTBiNzMzOTM4NjM4MGU3OTcwKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2M4Y2IxNzdmZDYzMjRkMzdiYzc1MGNlYjBjNDAzYWZhID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzIxMTY5NTk5OTk5OTk2LC0xMjIuNDM3NDg4OTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF80NzJlYWIwMzdhNDc0NGZhYjJkMDYwYTQzNDgwMWU5ZiA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9jZmQzODIyYjM0MDE0YzI3OGY2NTg1NTMxNmVmNjI4MiA9ICQoJzxkaXYgaWQ9Imh0bWxfY2ZkMzgyMmIzNDAxNGMyNzhmNjU4NTUzMTZlZjYyODIiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjU3OiBNSVNTSU9OIFNUICYgT05PTkRBR0EgQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF80NzJlYWIwMzdhNDc0NGZhYjJkMDYwYTQzNDgwMWU5Zi5zZXRDb250ZW50KGh0bWxfY2ZkMzgyMmIzNDAxNGMyNzhmNjU4NTUzMTZlZjYyODIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9jOGNiMTc3ZmQ2MzI0ZDM3YmM3NTBjZWIwYzQwM2FmYS5iaW5kUG9wdXAocG9wdXBfNDcyZWFiMDM3YTQ3NDRmYWIyZDA2MGE0MzQ4MDFlOWYpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZTc2Mzg2YWUwNWQxNGQ2ZjhiMzM2ZWNlYmY0MTI5YjEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODY4MzkyLC0xMjIuNDAxMjc4NDk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8yZmY0NDA1Yjg2ZTM0Y2QzOTRjYjQ0NjU5NTI4MmIxMCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9iYjRjMTUxMTNmZTg0ODk2OGQ3YjY1M2RkNmVjODkwZSA9ICQoJzxkaXYgaWQ9Imh0bWxfYmI0YzE1MTEzZmU4NDg5NjhkN2I2NTNkZDZlYzg5MGUiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjU4OiBNSVNTSU9OIFNUICYgQU5OSUUgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzJmZjQ0MDViODZlMzRjZDM5NGNiNDQ2NTk1MjgyYjEwLnNldENvbnRlbnQoaHRtbF9iYjRjMTUxMTNmZTg0ODk2OGQ3YjY1M2RkNmVjODkwZSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2U3NjM4NmFlMDVkMTRkNmY4YjMzNmVjZWJmNDEyOWIxLmJpbmRQb3B1cChwb3B1cF8yZmY0NDA1Yjg2ZTM0Y2QzOTRjYjQ0NjU5NTI4MmIxMCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9iOGFjNzQxNjg4NWU0MmE5OWMzNzE4YWVhMWZlNzQ5ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc1Njc5ODcwMDAwMDAwNCwtMTIyLjQyMDM0NDc5OTk5OTk4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNDJjN2VhOTE0NmFiNDNlM2E3ZGIzNTA3ZGY4NTEzMTMgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYmQ1MWYyMDZjYTI1NDBhNDljYjk1ZDJmZjZkM2U1YWIgPSAkKCc8ZGl2IGlkPSJodG1sX2JkNTFmMjA2Y2EyNTQwYTQ5Y2I5NWQyZmY2ZDNlNWFiIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij41OTogTUlTU0lPTiBTVCAmIDIxU1QgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzQyYzdlYTkxNDZhYjQzZTNhN2RiMzUwN2RmODUxMzEzLnNldENvbnRlbnQoaHRtbF9iZDUxZjIwNmNhMjU0MGE0OWNiOTVkMmZmNmQzZTVhYik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2I4YWM3NDE2ODg1ZTQyYTk5YzM3MThhZWExZmU3NDlmLmJpbmRQb3B1cChwb3B1cF80MmM3ZWE5MTQ2YWI0M2UzYTdkYjM1MDdkZjg1MTMxMyk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9hN2Y0ODEwYzExMzY0ZmRiYTQ1MzJlZDM2YThlNWY3YyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc2MzA2MzcwMDAwMDAwNCwtMTIyLjQxOTUwNDRdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF80ZmUwNWYxYWI5NTY0MDczYTcxYjJjYjNiZTI1NDVlOCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF84YTQ0ZDc0NjA5YTQ0YzhmOGJkNmM3NmY0NGU0NjM4OCA9ICQoJzxkaXYgaWQ9Imh0bWxfOGE0NGQ3NDYwOWE0NGM4ZjhiZDZjNzZmNDRlNDYzODgiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjYwOiBNSVNTSU9OIFNUICYgQ0xBUklPTiBBTFk8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzRmZTA1ZjFhYjk1NjQwNzNhNzFiMmNiM2JlMjU0NWU4LnNldENvbnRlbnQoaHRtbF84YTQ0ZDc0NjA5YTQ0YzhmOGJkNmM3NmY0NGU0NjM4OCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2E3ZjQ4MTBjMTEzNjRmZGJhNDUzMmVkMzZhOGU1ZjdjLmJpbmRQb3B1cChwb3B1cF80ZmUwNWYxYWI5NTY0MDczYTcxYjJjYjNiZTI1NDVlOCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl84NTUwMjg3ZGI3YWY0NjVmYWNmYzQ4MDFhNWRhZWU4ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxMTYwNTYwMDAwMDAwNiwtMTIyLjQ0NjExNDk5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMDgxMWFjYTM4N2RmNDQ4OWIzZTcwMTVmYTExZmUzZTggPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMTk1MThlNGM0ZTBjNGI4N2E5MDc0YzYzMWRiMzk5YTYgPSAkKCc8ZGl2IGlkPSJodG1sXzE5NTE4ZTRjNGUwYzRiODdhOTA3NGM2MzFkYjM5OWE2IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij42MTogTUlTU0lPTiBTVCAmIE5BR0xFRSBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzA4MTFhY2EzODdkZjQ0ODliM2U3MDE1ZmExMWZlM2U4LnNldENvbnRlbnQoaHRtbF8xOTUxOGU0YzRlMGM0Yjg3YTkwNzRjNjMxZGIzOTlhNik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzg1NTAyODdkYjdhZjQ2NWZhY2ZjNDgwMWE1ZGFlZThmLmJpbmRQb3B1cChwb3B1cF8wODExYWNhMzg3ZGY0NDg5YjNlNzAxNWZhMTFmZTNlOCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl84ZTc4N2RlOGFjMDc0MzU0YTQwMWU0OWIyYjk4ZDY0ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyOTg4MywtMTIyLjQzMDI0MjJdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF85NGJmNWUxMDliM2I0YmU3YmVhNTNkOTFiYmU2ZGFkMyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9kZTliYWZlYjMzNWU0NDU5YmZiNWE4YTgyYTQzZTMyOCA9ICQoJzxkaXYgaWQ9Imh0bWxfZGU5YmFmZWIzMzVlNDQ1OWJmYjVhOGE4MmE0M2UzMjgiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjYyOiBNSVNTSU9OIFNUICYgQURNSVJBTCBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzk0YmY1ZTEwOWIzYjRiZTdiZWE1M2Q5MWJiZTZkYWQzLnNldENvbnRlbnQoaHRtbF9kZTliYWZlYjMzNWU0NDU5YmZiNWE4YTgyYTQzZTMyOCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzhlNzg3ZGU4YWMwNzQzNTRhNDAxZTQ5YjJiOThkNjRmLmJpbmRQb3B1cChwb3B1cF85NGJmNWUxMDliM2I0YmU3YmVhNTNkOTFiYmU2ZGFkMyk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl83ZmUzODJlMzIzZjc0ZDk3OTE3YWUzZTNjMTFjNjMyNyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczNTYyMDAwMDAwMDAwNCwtMTIyLjQyNDddLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF81NWIwYzI5ZjRiZmM0NzllODg0NTM3MTBmMWFjNzE5NSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF84NDU4MWE4M2MyNDg0ODc4YTg0NjVmYTBjYTlmZTE4ZCA9ICQoJzxkaXYgaWQ9Imh0bWxfODQ1ODFhODNjMjQ4NDg3OGE4NDY1ZmEwY2E5ZmUxOGQiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjYzOiBNSVNTSU9OIFNUICYgUklDSExBTkQgQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF81NWIwYzI5ZjRiZmM0NzllODg0NTM3MTBmMWFjNzE5NS5zZXRDb250ZW50KGh0bWxfODQ1ODFhODNjMjQ4NDg3OGE4NDY1ZmEwY2E5ZmUxOGQpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl83ZmUzODJlMzIzZjc0ZDk3OTE3YWUzZTNjMTFjNjMyNy5iaW5kUG9wdXAocG9wdXBfNTViMGMyOWY0YmZjNDc5ZTg4NDUzNzEwZjFhYzcxOTUpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfOGExZjc1MDk2NGVmNDM5NGJlNTk0NmE2ZjJiZTRhNmUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTE0Njc5OTk5OTk5OTYsLTEyMi40NDYyOTA5OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzQzYjgwMDU1ZWE0YzQ5YTRhOWFmNTM1YTUzMTk1NDQ4ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2YxOWIwOTBhYTEzNDRiMmJiOGU0NTViZjQ5YzhmYmMzID0gJCgnPGRpdiBpZD0iaHRtbF9mMTliMDkwYWExMzQ0YjJiYjhlNDU1YmY0OWM4ZmJjMyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NjQ6IE1JU1NJT04gU1QgJiBMT1dFTEwgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzQzYjgwMDU1ZWE0YzQ5YTRhOWFmNTM1YTUzMTk1NDQ4LnNldENvbnRlbnQoaHRtbF9mMTliMDkwYWExMzQ0YjJiYjhlNDU1YmY0OWM4ZmJjMyk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzhhMWY3NTA5NjRlZjQzOTRiZTU5NDZhNmYyYmU0YTZlLmJpbmRQb3B1cChwb3B1cF80M2I4MDA1NWVhNGM0OWE0YTlhZjUzNWE1MzE5NTQ0OCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8xOGQzMjNkNTlmYzY0NWMyOTJkMjdmYmVhYTZlMGUxOCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyNDAyNzg5OTk5OTk5NiwtMTIyLjQzNTMyOTU5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfZDZjMWU4NGUwN2ViNDk5Yjg4NjE0MDUzZjRkOGVkOWIgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfZjhlOGJjMDk1YmQ3NGVhODg3OTNmN2NlNjA1M2U0YTggPSAkKCc8ZGl2IGlkPSJodG1sX2Y4ZThiYzA5NWJkNzRlYTg4NzkzZjdjZTYwNTNlNGE4IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij42NTogTUlTU0lPTiBTVCAmIFNBTiBKVUFOIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfZDZjMWU4NGUwN2ViNDk5Yjg4NjE0MDUzZjRkOGVkOWIuc2V0Q29udGVudChodG1sX2Y4ZThiYzA5NWJkNzRlYTg4NzkzZjdjZTYwNTNlNGE4KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMThkMzIzZDU5ZmM2NDVjMjkyZDI3ZmJlYWE2ZTBlMTguYmluZFBvcHVwKHBvcHVwX2Q2YzFlODRlMDdlYjQ5OWI4ODYxNDA1M2Y0ZDhlZDliKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzMxNGNjMGVmMTg4NDQxOGVhOWUyNWFkZTgyZDE1MjFlID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzY4Mjg0MjAwMDAwMDA0LC0xMjIuNDIwMDA3NF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzBkOGU1OGY5YmVlNzQ5MzlhN2Q3MmIzYjVhNTRhMmZlID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzIzNGQzYTUzNTU5NzQwNWZiNWY2Yjg1MTMyN2Q5ZWNmID0gJCgnPGRpdiBpZD0iaHRtbF8yMzRkM2E1MzU1OTc0MDVmYjVmNmI4NTEzMjdkOWVjZiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NjY6IE1JU1NJT04gU1QgJiAxNFRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8wZDhlNThmOWJlZTc0OTM5YTdkNzJiM2I1YTU0YTJmZS5zZXRDb250ZW50KGh0bWxfMjM0ZDNhNTM1NTk3NDA1ZmI1ZjZiODUxMzI3ZDllY2YpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8zMTRjYzBlZjE4ODQ0MThlYTllMjVhZGU4MmQxNTIxZS5iaW5kUG9wdXAocG9wdXBfMGQ4ZTU4ZjliZWU3NDkzOWE3ZDcyYjNiNWE1NGEyZmUpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMDNiODA1ZWQ4OGMxNDgwYzlkZWJlMzMwZmFhYmEyNzEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTM5NTI4OTk5OTk5OTUsLTEyMi40NDMzNzUyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNGU4YmE1NTlhMGZlNDJiZmE3NjdmZmNkOGU0ZGQ2YTEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfODk0OTk0ZGFmNDhlNGU4ODkzYTJlZWRhZjg1YjAyYjggPSAkKCc8ZGl2IGlkPSJodG1sXzg5NDk5NGRhZjQ4ZTRlODg5M2EyZWVkYWY4NWIwMmI4IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij42NzogTUlTU0lPTiBTVCAmIENPTkNPUkQgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzRlOGJhNTU5YTBmZTQyYmZhNzY3ZmZjZDhlNGRkNmExLnNldENvbnRlbnQoaHRtbF84OTQ5OTRkYWY0OGU0ZTg4OTNhMmVlZGFmODViMDJiOCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzAzYjgwNWVkODhjMTQ4MGM5ZGViZTMzMGZhYWJhMjcxLmJpbmRQb3B1cChwb3B1cF80ZThiYTU1OWEwZmU0MmJmYTc2N2ZmY2Q4ZTRkZDZhMSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lNTI3MDZhYzYyOTY0MDY4YTZkNDI2NzFlN2YzZDAyZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczNzM2MTIsLTEyMi40MjQwNDg0MDAwMDAwMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzhmN2JjY2RlOTVhZjQ3NTc4NTBmZmEyY2RjNTNmNmNhID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzUyNzhmNjc0OGIzMDQ2ODdiNmUwMWUyYmExNTBlZDMyID0gJCgnPGRpdiBpZD0iaHRtbF81Mjc4ZjY3NDhiMzA0Njg3YjZlMDFlMmJhMTUwZWQzMiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+Njg6IE1JU1NJT04gU1QgJiBISUdITEFORCBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzhmN2JjY2RlOTVhZjQ3NTc4NTBmZmEyY2RjNTNmNmNhLnNldENvbnRlbnQoaHRtbF81Mjc4ZjY3NDhiMzA0Njg3YjZlMDFlMmJhMTUwZWQzMik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2U1MjcwNmFjNjI5NjQwNjhhNmQ0MjY3MWU3ZjNkMDJmLmJpbmRQb3B1cChwb3B1cF84ZjdiY2NkZTk1YWY0NzU3ODUwZmZhMmNkYzUzZjZjYSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8xYjk2ZWRkNGFmYWE0MTRkOWI5YzAzYzJkMjczODI2YyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3NTI4MzgsLTEyMi40MTU5MzcxMDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2YzZjg2ODY4ODY0NjRhZTY5MzdmNGUzNjg3ZmE4MmYwID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzdiMzg1Mjg1ZmI1NDQ5YjM5MWQ0YTUyNzliZjhiZDhhID0gJCgnPGRpdiBpZD0iaHRtbF83YjM4NTI4NWZiNTQ0OWIzOTFkNGE1Mjc5YmY4YmQ4YSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+Njk6IE1JU1NJT04gU1QgJiAxMFRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9mM2Y4Njg2ODg2NDY0YWU2OTM3ZjRlMzY4N2ZhODJmMC5zZXRDb250ZW50KGh0bWxfN2IzODUyODVmYjU0NDliMzkxZDRhNTI3OWJmOGJkOGEpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8xYjk2ZWRkNGFmYWE0MTRkOWI5YzAzYzJkMjczODI2Yy5iaW5kUG9wdXAocG9wdXBfZjNmODY4Njg4NjQ2NGFlNjkzN2Y0ZTM2ODdmYTgyZjApOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZmQ1Y2M0YThiZGQxNGViNDliMzYwZTY5MTVlMDZhYTQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MDk0MjExLC0xMjIuNDUwOTEyOTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF81ZjEyMjI5MDM4YjA0ZDQxODQ1MGY5ZmFlNjAxODgwZCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF82MmFiZjI2NmU0ZGU0Y2Y5YTY2OGJkMjUwYzhlZmUxNSA9ICQoJzxkaXYgaWQ9Imh0bWxfNjJhYmYyNjZlNGRlNGNmOWE2NjhiZDI1MGM4ZWZlMTUiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjcwOiBNSVNTSU9OIFNUICYgTEFXUkVOQ0UgQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF81ZjEyMjI5MDM4YjA0ZDQxODQ1MGY5ZmFlNjAxODgwZC5zZXRDb250ZW50KGh0bWxfNjJhYmYyNjZlNGRlNGNmOWE2NjhiZDI1MGM4ZWZlMTUpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9mZDVjYzRhOGJkZDE0ZWI0OWIzNjBlNjkxNWUwNmFhNC5iaW5kUG9wdXAocG9wdXBfNWYxMjIyOTAzOGIwNGQ0MTg0NTBmOWZhZTYwMTg4MGQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNmVmZGM1NTFhYzA1NDRiMDk2MjYzZmRjYzRlOWRlZDYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTcyNzQxMDAwMDAwMDQsLTEyMi40NDA0NTI3OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzg0ZDk2OTdkZjg5NjRlMDZiMGNjOTU0YzA4MGYwZmJkID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzUyY2RmMGE4MWYxMzQzZmY4ZTEzY2RiOGExMmQ2ZWFhID0gJCgnPGRpdiBpZD0iaHRtbF81MmNkZjBhODFmMTM0M2ZmOGUxM2NkYjhhMTJkNmVhYSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NzE6IE1JU1NJT04gU1QgJiBBTUFaT04gQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF84NGQ5Njk3ZGY4OTY0ZTA2YjBjYzk1NGMwODBmMGZiZC5zZXRDb250ZW50KGh0bWxfNTJjZGYwYTgxZjEzNDNmZjhlMTNjZGI4YTEyZDZlYWEpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl82ZWZkYzU1MWFjMDU0NGIwOTYyNjNmZGNjNGU5ZGVkNi5iaW5kUG9wdXAocG9wdXBfODRkOTY5N2RmODk2NGUwNmIwY2M5NTRjMDgwZjBmYmQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMWY2OGNkMzE3Mzc4NGE3NjgwZTAzZWNlMzliZjkzMDUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODkxNzksLTEyMi4zOTgzMzc5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNWRlZTZhNTk1MmY3NDZhZWJiMDRlYjNhM2M0ZWZjMjYgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYjA5MDAxYjIxNGY3NDgwOTk0ODhjNTU1MDk1ZmVjZWIgPSAkKCc8ZGl2IGlkPSJodG1sX2IwOTAwMWIyMTRmNzQ4MDk5NDg4YzU1NTA5NWZlY2ViIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij43MjogTUlTU0lPTiBTVCAmIEVDS0VSIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF81ZGVlNmE1OTUyZjc0NmFlYmIwNGViM2EzYzRlZmMyNi5zZXRDb250ZW50KGh0bWxfYjA5MDAxYjIxNGY3NDgwOTk0ODhjNTU1MDk1ZmVjZWIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8xZjY4Y2QzMTczNzg0YTc2ODBlMDNlY2UzOWJmOTMwNS5iaW5kUG9wdXAocG9wdXBfNWRlZTZhNTk1MmY3NDZhZWJiMDRlYjNhM2M0ZWZjMjYpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYWZkMDVhZTQ0ZTAzNDlkNWE5ZjM0ZTkzMmJmMjVjMzcgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODg5ODY1LC0xMjIuMzk4NTg2MV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzU4YzM5ZmE5OWVjMzQzM2ZhNDk2NzJhYzAyZDQ5NmM3ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzg1YzU0NzcyYjg4MzQ1MTRiYzJkNzExYTg5ODQ3ZGM5ID0gJCgnPGRpdiBpZD0iaHRtbF84NWM1NDc3MmI4ODM0NTE0YmMyZDcxMWE4OTg0N2RjOSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+NzM6IE1JU1NJT04gU1QgJiBTSEFXIEFMWTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfNThjMzlmYTk5ZWMzNDMzZmE0OTY3MmFjMDJkNDk2Yzcuc2V0Q29udGVudChodG1sXzg1YzU0NzcyYjg4MzQ1MTRiYzJkNzExYTg5ODQ3ZGM5KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfYWZkMDVhZTQ0ZTAzNDlkNWE5ZjM0ZTkzMmJmMjVjMzcuYmluZFBvcHVwKHBvcHVwXzU4YzM5ZmE5OWVjMzQzM2ZhNDk2NzJhYzAyZDQ5NmM3KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzMxZTQ3MmEyNTNiNzQ3M2Y5NmY5MjcxYmExMzEzMzA3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI3NjU2LC0xMjIuNDMyNDcyNjk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF81YmYxZWI5ODk0ZDU0MDg2OTdiZDJiNDM3ZjkzNTBmMyA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF85ODY0NDUwMGI0MzA0MzRlOWMzMmZlYTgyZTM3NzQzOSA9ICQoJzxkaXYgaWQ9Imh0bWxfOTg2NDQ1MDBiNDMwNDM0ZTljMzJmZWE4MmUzNzc0MzkiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjc0OiBNSVNTSU9OIFNUICYgQVZBTE9OIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfNWJmMWViOTg5NGQ1NDA4Njk3YmQyYjQzN2Y5MzUwZjMuc2V0Q29udGVudChodG1sXzk4NjQ0NTAwYjQzMDQzNGU5YzMyZmVhODJlMzc3NDM5KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMzFlNDcyYTI1M2I3NDczZjk2ZjkyNzFiYTEzMTMzMDcuYmluZFBvcHVwKHBvcHVwXzViZjFlYjk4OTRkNTQwODY5N2JkMmI0MzdmOTM1MGYzKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzc4ZjczMTY3Yjk3MzQ2YmJiMTliMzMyMzE1OGRkMmIxID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI2NTk3MiwtMTIyLjQzMzM4NTEwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfZDZjODE2MDI0NDI1NDQwMDk2MDFkNjM5ZjIwMjkwMWYgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYmExMTY3NzI0MzAxNDkzYTlmNjE3MGYzYTFmZGYwMmMgPSAkKCc8ZGl2IGlkPSJodG1sX2JhMTE2NzcyNDMwMTQ5M2E5ZjYxNzBmM2ExZmRmMDJjIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij43NTogTUlTU0lPTiBTVCAmIEZSQU5DSVMgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2Q2YzgxNjAyNDQyNTQ0MDA5NjAxZDYzOWYyMDI5MDFmLnNldENvbnRlbnQoaHRtbF9iYTExNjc3MjQzMDE0OTNhOWY2MTcwZjNhMWZkZjAyYyk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzc4ZjczMTY3Yjk3MzQ2YmJiMTliMzMyMzE1OGRkMmIxLmJpbmRQb3B1cChwb3B1cF9kNmM4MTYwMjQ0MjU0NDAwOTYwMWQ2MzlmMjAyOTAxZik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl82Mzc1MmIzNWM1Mzk0OWFhYTkzNWFkZjYxZjJiYmM0NiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3MzQ4ODYsLTEyMi40MTgwNTIxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNTYzZGYzNmYwODkwNDQ3ZTkyNGI0ZjlhODZhZWVmNWYgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfZWNmN2NkNDRjMTY1NDgyY2I4YjE0YTAzY2MwMDVlNGYgPSAkKCc8ZGl2IGlkPSJodG1sX2VjZjdjZDQ0YzE2NTQ4MmNiOGIxNGEwM2NjMDA1ZTRmIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij43NjogTUlTU0lPTiBTVCAmIExBRkFZRVRURSBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfNTYzZGYzNmYwODkwNDQ3ZTkyNGI0ZjlhODZhZWVmNWYuc2V0Q29udGVudChodG1sX2VjZjdjZDQ0YzE2NTQ4MmNiOGIxNGEwM2NjMDA1ZTRmKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfNjM3NTJiMzVjNTM5NDlhYWE5MzVhZGY2MWYyYmJjNDYuYmluZFBvcHVwKHBvcHVwXzU2M2RmMzZmMDg5MDQ0N2U5MjRiNGY5YTg2YWVlZjVmKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzdlYjYwYzQ3MWU5NzRmNzdiYzQ5ZDliNjZiYzA2M2VhID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzgzMDkwNCwtMTIyLjQwNjAzNzgwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNjA2YmJkZjljZTIxNDcxN2I1YmJlOTQ1Mjk2Mzk0M2MgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMTQ4MGY5NWMyMWMxNDVmOTljNzA2NzcwZTM5ZTEzNmEgPSAkKCc8ZGl2IGlkPSJodG1sXzE0ODBmOTVjMjFjMTQ1Zjk5YzcwNjc3MGUzOWUxMzZhIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij43NzogTUlTU0lPTiBTVCAmIEpFU1NJRSBXRVNUIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF82MDZiYmRmOWNlMjE0NzE3YjViYmU5NDUyOTYzOTQzYy5zZXRDb250ZW50KGh0bWxfMTQ4MGY5NWMyMWMxNDVmOTljNzA2NzcwZTM5ZTEzNmEpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl83ZWI2MGM0NzFlOTc0Zjc3YmM0OWQ5YjY2YmMwNjNlYS5iaW5kUG9wdXAocG9wdXBfNjA2YmJkZjljZTIxNDcxN2I1YmJlOTQ1Mjk2Mzk0M2MpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfY2Y3MzI2OTcxNzRkNGM2YmI1N2QxZjljNDIyOWUyNzkgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzUyNDYzLC0xMjIuNDI0ODYwOF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzYyYjdiZGYxNTk5NDRiYzdhYzE5NTg3ZWM4NGFlZDMzID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzQzZDI5ZDhjZTZiOTQwNDI4YzFkNWM4NzY2NjgxNzRkID0gJCgnPGRpdiBpZD0iaHRtbF80M2QyOWQ4Y2U2Yjk0MDQyOGMxZDVjODc2NjY4MTc0ZCIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+Nzg6IE1JU1NJT04gU1QgJiBDT0xMRUdFIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfNjJiN2JkZjE1OTk0NGJjN2FjMTk1ODdlYzg0YWVkMzMuc2V0Q29udGVudChodG1sXzQzZDI5ZDhjZTZiOTQwNDI4YzFkNWM4NzY2NjgxNzRkKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfY2Y3MzI2OTcxNzRkNGM2YmI1N2QxZjljNDIyOWUyNzkuYmluZFBvcHVwKHBvcHVwXzYyYjdiZGYxNTk5NDRiYzdhYzE5NTg3ZWM4NGFlZDMzKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzA1MjlmYTY0ZTM2NTQwZGViN2YxMmQ4MGUzNThjZTI4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzg1Mjg4MSwtMTIyLjQwMzQxODRdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF80YjRmY2FkOTk0YWY0ZjA3OTZhYTk4MWVlYWU4ZTExZiA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF80Y2QyODBjNGUwMWM0OGMyOTk5OTQ0NGMwZDcxMDI4OSA9ICQoJzxkaXYgaWQ9Imh0bWxfNGNkMjgwYzRlMDFjNDhjMjk5OTk0NDRjMGQ3MTAyODkiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjc5OiBNSVNTSU9OIFNUICYgMDNSRCBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfNGI0ZmNhZDk5NGFmNGYwNzk2YWE5ODFlZWFlOGUxMWYuc2V0Q29udGVudChodG1sXzRjZDI4MGM0ZTAxYzQ4YzI5OTk5NDQ0YzBkNzEwMjg5KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMDUyOWZhNjRlMzY1NDBkZWI3ZjEyZDgwZTM1OGNlMjguYmluZFBvcHVwKHBvcHVwXzRiNGZjYWQ5OTRhZjRmMDc5NmFhOTgxZWVhZThlMTFmKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzhjYTM4YWNmN2M4ODQ1MzViYzk4OTU3MjVmYzJiNjI1ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI1MTc1MiwtMTIyLjQzNDQ2MTVdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8xM2U5MWY0YWZjMTc0ODdhYmE3ZGIxMDkxZTZmOTk3OSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9jMTJjNWUyMzRjYTI0OTY2YTg3NWEwMDNiNmE0ZDAxMiA9ICQoJzxkaXYgaWQ9Imh0bWxfYzEyYzVlMjM0Y2EyNDk2NmE4NzVhMDAzYjZhNGQwMTIiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjgwOiBNSVNTSU9OIFNUICYgSEFSUklOR1RPTiBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfMTNlOTFmNGFmYzE3NDg3YWJhN2RiMTA5MWU2Zjk5Nzkuc2V0Q29udGVudChodG1sX2MxMmM1ZTIzNGNhMjQ5NjZhODc1YTAwM2I2YTRkMDEyKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfOGNhMzhhY2Y3Yzg4NDUzNWJjOTg5NTcyNWZjMmI2MjUuYmluZFBvcHVwKHBvcHVwXzEzZTkxZjRhZmMxNzQ4N2FiYTdkYjEwOTFlNmY5OTc5KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2ZkM2EzNzlkMDEyMDRiZjZhZWUxNTYzZjY3NmE5Yzg0ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzM0Mjk1MywtMTIyLjQyNTgxMjk5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfODdlODVjNmQ2MDFjNGUxOTgwNWFkMDZiNGNhYWIzYmIgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfZTMzNjExOWVmYzg0NDkwZTkwYzJkNDc0YTk0MjNjYTIgPSAkKCc8ZGl2IGlkPSJodG1sX2UzMzYxMTllZmM4NDQ5MGU5MGMyZDQ3NGE5NDIzY2EyIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij44MTogTUlTU0lPTiBTVCAmIENPTExFR0UgVEVSPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF84N2U4NWM2ZDYwMWM0ZTE5ODA1YWQwNmI0Y2FhYjNiYi5zZXRDb250ZW50KGh0bWxfZTMzNjExOWVmYzg0NDkwZTkwYzJkNDc0YTk0MjNjYTIpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9mZDNhMzc5ZDAxMjA0YmY2YWVlMTU2M2Y2NzZhOWM4NC5iaW5kUG9wdXAocG9wdXBfODdlODVjNmQ2MDFjNGUxOTgwNWFkMDZiNGNhYWIzYmIpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNzlkNWI5NTg4MTI4NDNmNWJlMTYxOGNlOTRkODIzN2QgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDU2MDM0OTk5OTk5OTQsLTEyMi40MTk4OTc1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfY2RlNTdmOTUzMjgzNDI4YzhiMThiZDE1MTU1NGVmYzggPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMTA1NGIyYzljODgzNDE2ZTg2NGZlZGJiN2RlYWI1MWMgPSAkKCc8ZGl2IGlkPSJodG1sXzEwNTRiMmM5Yzg4MzQxNmU4NjRmZWRiYjdkZWFiNTFjIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij44MjogTUlTU0lPTiBTVCAmIFZBTEVOQ0lBIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9jZGU1N2Y5NTMyODM0MjhjOGIxOGJkMTUxNTU0ZWZjOC5zZXRDb250ZW50KGh0bWxfMTA1NGIyYzljODgzNDE2ZTg2NGZlZGJiN2RlYWI1MWMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl83OWQ1Yjk1ODgxMjg0M2Y1YmUxNjE4Y2U5NGQ4MjM3ZC5iaW5kUG9wdXAocG9wdXBfY2RlNTdmOTUzMjgzNDI4YzhiMThiZDE1MTU1NGVmYzgpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMTAzOTA1YTkzMjlkNDJlNmE5Yjk5MjE2ZmYwYzZmZTMgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzAwMDk3MDAwMDAwMDQsLTEyMi40MzAxMTYwOTk5OTk5OF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzAzODVhZGRiNDJhZjRjODA5MWUzNjAwMDAzNmRmOWYwID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzY3OGU0ZjNmNDUzMjRmMDg4N2QzN2E4YzY0Mjc1ZTcyID0gJCgnPGRpdiBpZD0iaHRtbF82NzhlNGYzZjQ1MzI0ZjA4ODdkMzdhOGM2NDI3NWU3MiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+ODM6IE1JU1NJT04gU1QgJiBORVkgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzAzODVhZGRiNDJhZjRjODA5MWUzNjAwMDAzNmRmOWYwLnNldENvbnRlbnQoaHRtbF82NzhlNGYzZjQ1MzI0ZjA4ODdkMzdhOGM2NDI3NWU3Mik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzEwMzkwNWE5MzI5ZDQyZTZhOWI5OTIxNmZmMGM2ZmUzLmJpbmRQb3B1cChwb3B1cF8wMzg1YWRkYjQyYWY0YzgwOTFlMzYwMDAwMzZkZjlmMCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8xNTNkZTM2NTU1NTM0YzRmOWU4MWViY2UyOTIwMzVlMCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3MzA2NzIsLTEyMi40MTg3MjAwOTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzUyOTViNDQwMDYzMjRkMDRiMDlkNTYyOTdiN2IwZjEzID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2UwNmIyM2YxNzYxNjQxNjI4YzAyZTE5MjQ1ZDJlNTJjID0gJCgnPGRpdiBpZD0iaHRtbF9lMDZiMjNmMTc2MTY0MTYyOGMwMmUxOTI0NWQyZTUyYyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+ODQ6IE1JU1NJT04gU1QgJiAxMlRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF81Mjk1YjQ0MDA2MzI0ZDA0YjA5ZDU2Mjk3YjdiMGYxMy5zZXRDb250ZW50KGh0bWxfZTA2YjIzZjE3NjE2NDE2MjhjMDJlMTkyNDVkMmU1MmMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8xNTNkZTM2NTU1NTM0YzRmOWU4MWViY2UyOTIwMzVlMC5iaW5kUG9wdXAocG9wdXBfNTI5NWI0NDAwNjMyNGQwNGIwOWQ1NjI5N2I3YjBmMTMpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMzQ4MmRkODE3OWNhNGNjOGI5ODIzYzEzNDQ0MWFjNjYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NTA2NDgyLC0xMjIuNDE4MzE1OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzM5ZWJhM2EyODBmMDQ3YWRiN2QyZTQ4MDAwZDcxY2NhID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2QwYzc1MTJiZmFhMTRlYzJhOGRmZDQwNzViNDA5YTEzID0gJCgnPGRpdiBpZD0iaHRtbF9kMGM3NTEyYmZhYTE0ZWMyYThkZmQ0MDc1YjQwOWExMyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+ODU6IE1JU1NJT04gU1QgJiAyNVRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8zOWViYTNhMjgwZjA0N2FkYjdkMmU0ODAwMGQ3MWNjYS5zZXRDb250ZW50KGh0bWxfZDBjNzUxMmJmYWExNGVjMmE4ZGZkNDA3NWI0MDlhMTMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8zNDgyZGQ4MTc5Y2E0Y2M4Yjk4MjNjMTM0NDQxYWM2Ni5iaW5kUG9wdXAocG9wdXBfMzllYmEzYTI4MGYwNDdhZGI3ZDJlNDgwMDBkNzFjY2EpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZjM1NzEwYzBiMjA0NDdlZThlZDhiNTlhYzVmYzZiMzUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODIxMjU3LC0xMjIuNDA3MjcyNzk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9kODgxYzhlNmRmNzY0YzcyYjVjMmM5MTQ3YjRjMmZkZCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9kNjMwMjYzODUxOGU0OTdkYmQ0OThlOTA0OThhOTk0NSA9ICQoJzxkaXYgaWQ9Imh0bWxfZDYzMDI2Mzg1MThlNDk3ZGJkNDk4ZTkwNDk4YTk5NDUiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjg2OiBNSVNTSU9OIFNUICYgTUFSWSBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfZDg4MWM4ZTZkZjc2NGM3MmI1YzJjOTE0N2I0YzJmZGQuc2V0Q29udGVudChodG1sX2Q2MzAyNjM4NTE4ZTQ5N2RiZDQ5OGU5MDQ5OGE5OTQ1KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfZjM1NzEwYzBiMjA0NDdlZThlZDhiNTlhYzVmYzZiMzUuYmluZFBvcHVwKHBvcHVwX2Q4ODFjOGU2ZGY3NjRjNzJiNWMyYzkxNDdiNGMyZmRkKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzczZjg3N2FjYzM0OTQzNTA5MGMyYmI1N2VhZWY5NDY1ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzkyNTI5NywtMTIyLjM5NDA5MDVdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF80ZTM2NjIyZDliYTk0MjFiYmEwZjBjYTE0NThkNDI0YiA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF80N2Y5MTgyMDgyN2U0ZjgzOTEwMDE0YTNhYjYyMjRkYSA9ICQoJzxkaXYgaWQ9Imh0bWxfNDdmOTE4MjA4MjdlNGY4MzkxMDAxNGEzYWI2MjI0ZGEiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjg3OiBNSVNTSU9OIFNUICYgU1BFQVIgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzRlMzY2MjJkOWJhOTQyMWJiYTBmMGNhMTQ1OGQ0MjRiLnNldENvbnRlbnQoaHRtbF80N2Y5MTgyMDgyN2U0ZjgzOTEwMDE0YTNhYjYyMjRkYSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzczZjg3N2FjYzM0OTQzNTA5MGMyYmI1N2VhZWY5NDY1LmJpbmRQb3B1cChwb3B1cF80ZTM2NjIyZDliYTk0MjFiYmEwZjBjYTE0NThkNDI0Yik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wOTcxOTIxODhhZjE0Mzg4ODgzOGVjODFmZmIzMDJmMCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxNTc2NzQsLTEyMi40NDE1ODM1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMWFmMGJhNTZlZDE4NGEwMWEzOGMzZGI2ZjY3NzZjYmQgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfOTIxOWZjMmEyMzM2NDQzN2E2YmM4NTE3ZjJlOGVlYmEgPSAkKCc8ZGl2IGlkPSJodG1sXzkyMTlmYzJhMjMzNjQ0MzdhNmJjODUxN2YyZThlZWJhIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij44ODogTUlTU0lPTiBTVCAmIFJPTFBIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF8xYWYwYmE1NmVkMTg0YTAxYTM4YzNkYjZmNjc3NmNiZC5zZXRDb250ZW50KGh0bWxfOTIxOWZjMmEyMzM2NDQzN2E2YmM4NTE3ZjJlOGVlYmEpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8wOTcxOTIxODhhZjE0Mzg4ODgzOGVjODFmZmIzMDJmMC5iaW5kUG9wdXAocG9wdXBfMWFmMGJhNTZlZDE4NGEwMWEzOGMzZGI2ZjY3NzZjYmQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMGQzMGY5ZTkzMTZmNDk2YzkxNzJhNjQ4NDBiN2E5Y2QgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43Nzg5OTA1MDAwMDAwMDYsLTEyMi40MTIyMDI1OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzc5NzZhYTllODY1OTQ1YmM5YjBlZjU1ZmU5ZTEyN2MwID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzI3N2ExZjExMmJmNDRiY2ZiZDY1M2YzOTBlOGViNjg2ID0gJCgnPGRpdiBpZD0iaHRtbF8yNzdhMWYxMTJiZjQ0YmNmYmQ2NTNmMzkwZThlYjY4NiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+ODk6IE1JU1NJT04gU1QgJiAwN1RIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF83OTc2YWE5ZTg2NTk0NWJjOWIwZWY1NWZlOWUxMjdjMC5zZXRDb250ZW50KGh0bWxfMjc3YTFmMTEyYmY0NGJjZmJkNjUzZjM5MGU4ZWI2ODYpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8wZDMwZjllOTMxNmY0OTZjOTE3MmE2NDg0MGI3YTljZC5iaW5kUG9wdXAocG9wdXBfNzk3NmFhOWU4NjU5NDViYzliMGVmNTVmZTllMTI3YzApOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNjBmODI3YWM5ZWI2NDRiNGI3MWQ2ZDE1NjVjOTIyYzEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjQzMDk5OTk5OTk5OTYsLTEyMi40MzUxODk5OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2E2YmRkN2Q5MjA0ODQ3Y2I4YTgwYTdiODEzODgyMzQ4ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzczMTRjNTM0ZjVlZjRlYzNiOThlZmNkYWI1MWM2MTdmID0gJCgnPGRpdiBpZD0iaHRtbF83MzE0YzUzNGY1ZWY0ZWMzYjk4ZWZjZGFiNTFjNjE3ZiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+OTA6IE1JU1NJT04gU1QgJiBOT1JUT04gU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2E2YmRkN2Q5MjA0ODQ3Y2I4YTgwYTdiODEzODgyMzQ4LnNldENvbnRlbnQoaHRtbF83MzE0YzUzNGY1ZWY0ZWMzYjk4ZWZjZGFiNTFjNjE3Zik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzYwZjgyN2FjOWViNjQ0YjRiNzFkNmQxNTY1YzkyMmMxLmJpbmRQb3B1cChwb3B1cF9hNmJkZDdkOTIwNDg0N2NiOGE4MGE3YjgxMzg4MjM0OCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl81NWE3YjMzOWIwMzg0OTNkODc5MmVhMWQ3OWViMDE3OSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyMzA0OTgsLTEyMi40MzYwNjgwOTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2VmZTQxNjJlOTg4YzQ1MTQ4Mzg1MzFmZWFlNGM0MjZmID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzI4NjY3OTk3NDQ4YTQ3MTA4YjVjMDY1OGI0ZDM4YTFjID0gJCgnPGRpdiBpZD0iaHRtbF8yODY2Nzk5NzQ0OGE0NzEwOGI1YzA2NThiNGQzOGExYyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+OTE6IE1JU1NJT04gU1QgJiBSVVRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9lZmU0MTYyZTk4OGM0NTE0ODM4NTMxZmVhZTRjNDI2Zi5zZXRDb250ZW50KGh0bWxfMjg2Njc5OTc0NDhhNDcxMDhiNWMwNjU4YjRkMzhhMWMpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl81NWE3YjMzOWIwMzg0OTNkODc5MmVhMWQ3OWViMDE3OS5iaW5kUG9wdXAocG9wdXBfZWZlNDE2MmU5ODhjNDUxNDgzODUzMWZlYWU0YzQyNmYpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMTAxZTFjYzc2MDQ1NGY2NmEyOTYzOTFmZmQwODIwOTAgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjQ2ODc4OTk5OTk5OSwtMTIyLjQzNDgyNDNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9iNmM3ZWM2YzVhMjk0MWQyOGMzNDk3ZDE5YmYyOTAzZiA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9mYTQ0OWMzZDhlMjY0MWJhYmEzOWE0OGUyYjQyZDVkZiA9ICQoJzxkaXYgaWQ9Imh0bWxfZmE0NDljM2Q4ZTI2NDFiYWJhMzlhNDhlMmI0MmQ1ZGYiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjkyOiBNSVNTSU9OIFNUICYgQlJBWklMIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYjZjN2VjNmM1YTI5NDFkMjhjMzQ5N2QxOWJmMjkwM2Yuc2V0Q29udGVudChodG1sX2ZhNDQ5YzNkOGUyNjQxYmFiYTM5YTQ4ZTJiNDJkNWRmKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMTAxZTFjYzc2MDQ1NGY2NmEyOTYzOTFmZmQwODIwOTAuYmluZFBvcHVwKHBvcHVwX2I2YzdlYzZjNWEyOTQxZDI4YzM0OTdkMTliZjI5MDNmKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzcwMzIzYjA1YWJiNjQ1ZjBhOTIwZDgzZWVhYjgwOWU4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzEyNzgyNiwtMTIyLjQ0NDYwMTU5OTk5OTk4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNWE4ZTlhZGQ2YTE5NDVhNWI2MzZlNTRjZDM2Y2ZjZWIgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYjQxOGJmYjdkYTljNDQyNWJlMzA0OGFiZmQ4Mzg5NDkgPSAkKCc8ZGl2IGlkPSJodG1sX2I0MThiZmI3ZGE5YzQ0MjViZTMwNDhhYmZkODM4OTQ5IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij45MzogTUlTU0lPTiBTVCAmIEdVVFRFTkJFUkcgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzVhOGU5YWRkNmExOTQ1YTViNjM2ZTU0Y2QzNmNmY2ViLnNldENvbnRlbnQoaHRtbF9iNDE4YmZiN2RhOWM0NDI1YmUzMDQ4YWJmZDgzODk0OSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzcwMzIzYjA1YWJiNjQ1ZjBhOTIwZDgzZWVhYjgwOWU4LmJpbmRQb3B1cChwb3B1cF81YThlOWFkZDZhMTk0NWE1YjYzNmU1NGNkMzZjZmNlYik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl85Yjc2MmUxZmFmYTU0MmQ3YTJiZDFmODEyMzE1NjQzYyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc1Mzg0NTUwMDAwMDAwNCwtMTIyLjQxODYxOTQwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNTg0OWIyZmQ2Yzc5NDQzODg5MDBiYWI0OWMzY2M3OGEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYmVlNDRiZGFhODc4NDI5ZTliYzg5N2UwNDY3NTgyMjIgPSAkKCc8ZGl2IGlkPSJodG1sX2JlZTQ0YmRhYTg3ODQyOWU5YmM4OTdlMDQ2NzU4MjIyIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij45NDogTUlTU0lPTiBTVCAmIDIzUkQgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzU4NDliMmZkNmM3OTQ0Mzg4OTAwYmFiNDljM2NjNzhhLnNldENvbnRlbnQoaHRtbF9iZWU0NGJkYWE4Nzg0MjllOWJjODk3ZTA0Njc1ODIyMik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzliNzYyZTFmYWZhNTQyZDdhMmJkMWY4MTIzMTU2NDNjLmJpbmRQb3B1cChwb3B1cF81ODQ5YjJmZDZjNzk0NDM4ODkwMGJhYjQ5YzNjYzc4YSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8zY2U3OTdmOTJiMGE0Yjg5ODMzNTM4Y2JhZTM1YmIwMiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczOTc3OTI5OTk5OTk5NSwtMTIyLjQyMzY3NTQ5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMjA4Mzc0MmFiYjQ1NGQxOWJjYjQ0MDU5ZmQxNDllNjggPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMjZhZTE2YWJjN2QyNDZkYjk2OWU3ODIwMGU3ODlkODAgPSAkKCc8ZGl2IGlkPSJodG1sXzI2YWUxNmFiYzdkMjQ2ZGI5NjllNzgyMDBlNzg5ZDgwIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij45NTogTUlTU0lPTiBTVCAmIFJBTkRBTEwgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzIwODM3NDJhYmI0NTRkMTliY2I0NDA1OWZkMTQ5ZTY4LnNldENvbnRlbnQoaHRtbF8yNmFlMTZhYmM3ZDI0NmRiOTY5ZTc4MjAwZTc4OWQ4MCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzNjZTc5N2Y5MmIwYTRiODk4MzM1MzhjYmFlMzViYjAyLmJpbmRQb3B1cChwb3B1cF8yMDgzNzQyYWJiNDU0ZDE5YmNiNDQwNTlmZDE0OWU2OCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl84OWFhYWIyZjVlM2M0ZTkxOTMyODRlYjQzZmUwZmUyYyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyMzE1Mjg5OTk5OTk5NSwtMTIyLjQzNjAwMTc5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfZDAwNGIyYmM1YzM3NGEwY2FlODkxZmRhMTZlMTYxZmQgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMDMyNzZmMjA1ZmQzNGRhODgyOWU0Y2FkNWUwMDExY2IgPSAkKCc8ZGl2IGlkPSJodG1sXzAzMjc2ZjIwNWZkMzRkYTg4MjllNGNhZDVlMDAxMWNiIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij45NjogTUlTU0lPTiBTVCAmIFBFUlNJQSBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2QwMDRiMmJjNWMzNzRhMGNhZTg5MWZkYTE2ZTE2MWZkLnNldENvbnRlbnQoaHRtbF8wMzI3NmYyMDVmZDM0ZGE4ODI5ZTRjYWQ1ZTAwMTFjYik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzg5YWFhYjJmNWUzYzRlOTE5MzI4NGViNDNmZTBmZTJjLmJpbmRQb3B1cChwb3B1cF9kMDA0YjJiYzVjMzc0YTBjYWU4OTFmZGExNmUxNjFmZCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8zNjg4ZTc3OTllYjk0MjgwYTQ4YTY1ZjYyMGE1ZjIxNiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc2NTA1NjYsLTEyMi40MTk2OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9lOGFjY2FmZDFlZWY0MDdlYjM2MzRlZWZmMjI2OGMzZSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF9mODg0MmJlZDAyZTc0YWYyYTZlMmEyYThiNjBjNzUwOSA9ICQoJzxkaXYgaWQ9Imh0bWxfZjg4NDJiZWQwMmU3NGFmMmE2ZTJhMmE4YjYwYzc1MDkiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjk3OiBNSVNTSU9OIFNUICYgMTZUSCBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfZThhY2NhZmQxZWVmNDA3ZWIzNjM0ZWVmZjIyNjhjM2Uuc2V0Q29udGVudChodG1sX2Y4ODQyYmVkMDJlNzRhZjJhNmUyYTJhOGI2MGM3NTA5KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMzY4OGU3Nzk5ZWI5NDI4MGE0OGE2NWY2MjBhNWYyMTYuYmluZFBvcHVwKHBvcHVwX2U4YWNjYWZkMWVlZjQwN2ViMzYzNGVlZmYyMjY4YzNlKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzE5ZDFkODljYjFjZDRlMmNiNmIyYzE4MzliOWViZmFhID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzg1MTM4MjAwMDAwMDA2LC0xMjIuNDAzNTA1Nzk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8zNDhlMDhkMWFlNGI0NTM0OWNlMzE3OWJiN2E2NzEyZSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8yNmY4MmE5YTMyMDg0NTJmOWY3NGViMDQ4MWMxYTUwMSA9ICQoJzxkaXYgaWQ9Imh0bWxfMjZmODJhOWEzMjA4NDUyZjlmNzRlYjA0ODFjMWE1MDEiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjk4OiBNSVNTSU9OIFNUICYgWUVSQkEgQlVFTkEgTE48L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzM0OGUwOGQxYWU0YjQ1MzQ5Y2UzMTc5YmI3YTY3MTJlLnNldENvbnRlbnQoaHRtbF8yNmY4MmE5YTMyMDg0NTJmOWY3NGViMDQ4MWMxYTUwMSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzE5ZDFkODljYjFjZDRlMmNiNmIyYzE4MzliOWViZmFhLmJpbmRQb3B1cChwb3B1cF8zNDhlMDhkMWFlNGI0NTM0OWNlMzE3OWJiN2E2NzEyZSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9iZjMyMDAyNjI2Nzg0MGFlYjY0YjU4YTMxNWM0NzY5NyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcwOTY4OCwtMTIyLjQ1MDEyNDNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF81N2JhYmY0MmU0NzI0N2UwOGMxNGI4MDM0NDRkOTkyOCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF80NGE1OWI1ZDhhZTU0OGQ2OWM4ZjU2NmUzNGQ4NDhiZiA9ICQoJzxkaXYgaWQ9Imh0bWxfNDRhNTliNWQ4YWU1NDhkNjljOGY1NjZlMzRkODQ4YmYiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjk5OiBNSVNTSU9OIFNUICYgTEFVUkEgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzU3YmFiZjQyZTQ3MjQ3ZTA4YzE0YjgwMzQ0NGQ5OTI4LnNldENvbnRlbnQoaHRtbF80NGE1OWI1ZDhhZTU0OGQ2OWM4ZjU2NmUzNGQ4NDhiZik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2JmMzIwMDI2MjY3ODQwYWViNjRiNThhMzE1YzQ3Njk3LmJpbmRQb3B1cChwb3B1cF81N2JhYmY0MmU0NzI0N2UwOGMxNGI4MDM0NDRkOTkyOCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl84OGNhNzA1MWM0MzQ0NmM2ODkyNDgxZmI2MmNhZTMwMyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc2MTg3NjksLTEyMi40MTk3OTczXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfOWVlOWJmMjlhMmRiNGVmODkwM2JmNDIzNmFjMjE0MTEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfZjAwZmFmYTE3Yjk2NGQ2ZWIwMGM0Y2M3MDg5OWRhYzEgPSAkKCc8ZGl2IGlkPSJodG1sX2YwMGZhZmExN2I5NjRkNmViMDBjNGNjNzA4OTlkYWMxIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMDA6IE1JU1NJT04gU1QgJiAxOFRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF85ZWU5YmYyOWEyZGI0ZWY4OTAzYmY0MjM2YWMyMTQxMS5zZXRDb250ZW50KGh0bWxfZjAwZmFmYTE3Yjk2NGQ2ZWIwMGM0Y2M3MDg5OWRhYzEpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl84OGNhNzA1MWM0MzQ0NmM2ODkyNDgxZmI2MmNhZTMwMy5iaW5kUG9wdXAocG9wdXBfOWVlOWJmMjlhMmRiNGVmODkwM2JmNDIzNmFjMjE0MTEpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYTNiNzlkNDY3ZmY5NDNjN2IyYTU1Nzk3Nzc5ZjBhNDMgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjI0NjM3MDAwMDAwMDYsLTEyMi40MzY1MDk1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNTU4OTcwMDRhZGZhNGI4YWJiOTFjZDI4YmZmNDhlYzMgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfYTRkYzJlZDA3YzUxNDI0ZmJhZjU0NTBhYWVhZTlkMTggPSAkKCc8ZGl2IGlkPSJodG1sX2E0ZGMyZWQwN2M1MTQyNGZiYWY1NDUwYWFlYWU5ZDE4IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMDE6IE1JU1NJT04gU1QgJiBMRU8gU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzU1ODk3MDA0YWRmYTRiOGFiYjkxY2QyOGJmZjQ4ZWMzLnNldENvbnRlbnQoaHRtbF9hNGRjMmVkMDdjNTE0MjRmYmFmNTQ1MGFhZWFlOWQxOCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2EzYjc5ZDQ2N2ZmOTQzYzdiMmE1NTc5Nzc3OWYwYTQzLmJpbmRQb3B1cChwb3B1cF81NTg5NzAwNGFkZmE0YjhhYmI5MWNkMjhiZmY0OGVjMyk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8yYTViOTMxMzI5NzU0MzY5YThiNWE4ZGIzZWRlZjQwZSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0NjE4NTQ5OTk5OTk5NiwtMTIyLjQxOTUyNDA5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYmMzZDJjNjI1ZjVhNGM5Mzg5MzQwOWVkZTJlMzE0MGQgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMjhlNzhmMTdhZDg5NDIxNzkxMzFlMjYzYjYwNDEzMzUgPSAkKCc8ZGl2IGlkPSJodG1sXzI4ZTc4ZjE3YWQ4OTQyMTc5MTMxZTI2M2I2MDQxMzM1IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMDI6IE1JU1NJT04gU1QgJiBQT1dFUlMgQVZFPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9iYzNkMmM2MjVmNWE0YzkzODkzNDA5ZWRlMmUzMTQwZC5zZXRDb250ZW50KGh0bWxfMjhlNzhmMTdhZDg5NDIxNzkxMzFlMjYzYjYwNDEzMzUpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl8yYTViOTMxMzI5NzU0MzY5YThiNWE4ZGIzZWRlZjQwZS5iaW5kUG9wdXAocG9wdXBfYmMzZDJjNjI1ZjVhNGM5Mzg5MzQwOWVkZTJlMzE0MGQpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMTMyYjJiZjBmNTc4NDJmMmE1OWI4NWNiOGQxMGZlNmMgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjU3NzM0LC0xMjIuNDM0MDAzOF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzMzZDhkMWQ4ZDhiMTQyZjhhNDJkMGJkMzIzMzM2ZWIwID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzM5Mjc1MzJkMmYyODQ2MmFhZmUxYzk2Y2Q1ZmIwOWM4ID0gJCgnPGRpdiBpZD0iaHRtbF8zOTI3NTMyZDJmMjg0NjJhYWZlMWM5NmNkNWZiMDljOCIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTAzOiBNSVNTSU9OIFNUICYgU0FOVEEgUk9TQSBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzMzZDhkMWQ4ZDhiMTQyZjhhNDJkMGJkMzIzMzM2ZWIwLnNldENvbnRlbnQoaHRtbF8zOTI3NTMyZDJmMjg0NjJhYWZlMWM5NmNkNWZiMDljOCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzEzMmIyYmYwZjU3ODQyZjJhNTliODVjYjhkMTBmZTZjLmJpbmRQb3B1cChwb3B1cF8zM2Q4ZDFkOGQ4YjE0MmY4YTQyZDBiZDMyMzMzNmViMCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9jOWQ5ZmRmZjJlMGQ0OTk2YWJkN2Y2ZTI1ZDQ2NTE4ZCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxMzM4NTcsLTEyMi40NDM5NjU1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYTFlY2IwOGZhODUzNGRjY2JkYmZlNDdmNjcxNWU3NjAgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfZjkwY2ZjNTRlNTgxNDM3NGFmZDU1MDRmOGM3ZDcyM2IgPSAkKCc8ZGl2IGlkPSJodG1sX2Y5MGNmYzU0ZTU4MTQzNzRhZmQ1NTA0ZjhjN2Q3MjNiIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMDQ6IE1JU1NJT04gU1QgJiBGTE9SRU5USU5FIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYTFlY2IwOGZhODUzNGRjY2JkYmZlNDdmNjcxNWU3NjAuc2V0Q29udGVudChodG1sX2Y5MGNmYzU0ZTU4MTQzNzRhZmQ1NTA0ZjhjN2Q3MjNiKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfYzlkOWZkZmYyZTBkNDk5NmFiZDdmNmUyNWQ0NjUxOGQuYmluZFBvcHVwKHBvcHVwX2ExZWNiMDhmYTg1MzRkY2NiZGJmZTQ3ZjY3MTVlNzYwKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzAwYTY5NjE1YWM3YjQ2OTNhN2U5NTg4ZTJhYTRlMzk2ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzEyNjMzOSwtMTIyLjQ0NDc2OTk5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfNmRhMmRhZGNmODE1NGU2YjkwMDI5M2UzZWFkY2U1MzAgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfNTA2YjlmNjc1ZTY2NDlkNWIyYjNmN2FiMzMzNDEwNjkgPSAkKCc8ZGl2IGlkPSJodG1sXzUwNmI5ZjY3NWU2NjQ5ZDViMmIzZjdhYjMzMzQxMDY5IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMDU6IE1JU1NJT04gU1QgJiBGT09URSBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzZkYTJkYWRjZjgxNTRlNmI5MDAyOTNlM2VhZGNlNTMwLnNldENvbnRlbnQoaHRtbF81MDZiOWY2NzVlNjY0OWQ1YjJiM2Y3YWIzMzM0MTA2OSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzAwYTY5NjE1YWM3YjQ2OTNhN2U5NTg4ZTJhYTRlMzk2LmJpbmRQb3B1cChwb3B1cF82ZGEyZGFkY2Y4MTU0ZTZiOTAwMjkzZTNlYWRjZTUzMCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9hMTY4NzNmZWY3OTA0MDY5ODg2ZjZlMTg5MzZhNzk0MSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczMzY4NDYwMDAwMDAwNCwtMTIyLjQyNjM5Nl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2FjNDdhMjU2OTU1ZjRiOGY4ZWI2MDE0OGVkMmRhZjIzID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzhkZjhiMWRhNzY5OTQ1ZTQ5NDMwYTViYzUxNDhkMTlhID0gJCgnPGRpdiBpZD0iaHRtbF84ZGY4YjFkYTc2OTk0NWU0OTQzMGE1YmM1MTQ4ZDE5YSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTA2OiBNSVNTSU9OIFNUICYgQk9TV09SVEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2FjNDdhMjU2OTU1ZjRiOGY4ZWI2MDE0OGVkMmRhZjIzLnNldENvbnRlbnQoaHRtbF84ZGY4YjFkYTc2OTk0NWU0OTQzMGE1YmM1MTQ4ZDE5YSk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2ExNjg3M2ZlZjc5MDQwNjk4ODZmNmUxODkzNmE3OTQxLmJpbmRQb3B1cChwb3B1cF9hYzQ3YTI1Njk1NWY0YjhmOGViNjAxNDhlZDJkYWYyMyk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl85MTczYjcyZGExY2Q0MDhlYjAzNWU2ZjlkZmRmNmY0MyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyNjQwMzQ5OTk5OTk5NiwtMTIyLjQzMzQxMThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF9jNWU3MjMwNDA1NGY0YjZhODZmNzE4YjRmZjdiNjRjNCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF83M2M4ZTJiMTMzNTY0YzQ5ODZiN2YxNTUyNDk4MjkyYiA9ICQoJzxkaXYgaWQ9Imh0bWxfNzNjOGUyYjEzMzU2NGM0OTg2YjdmMTU1MjQ5ODI5MmIiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjEwNzogTUlTU0lPTiBTVCAmIEVYQ0VMU0lPUiBBVkU8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2M1ZTcyMzA0MDU0ZjRiNmE4NmY3MThiNGZmN2I2NGM0LnNldENvbnRlbnQoaHRtbF83M2M4ZTJiMTMzNTY0YzQ5ODZiN2YxNTUyNDk4MjkyYik7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzkxNzNiNzJkYTFjZDQwOGViMDM1ZTZmOWRmZGY2ZjQzLmJpbmRQb3B1cChwb3B1cF9jNWU3MjMwNDA1NGY0YjZhODZmNzE4YjRmZjdiNjRjNCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl80YzEwNjNmZjYzY2M0MmZiYjkzZTBiYmViODlhYmZkNSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3NTksLTEyMi40MTUxNTI5MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzcyNjFlZDdkOTlmNDQwZDFhYmMyZTUyYmFmNGRlMGFiID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzU0NzM3Mjk2MWI5MzQwMGU5OTJkZDc5OGU3Y2Q1Y2EzID0gJCgnPGRpdiBpZD0iaHRtbF81NDczNzI5NjFiOTM0MDBlOTkyZGQ3OThlN2NkNWNhMyIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTA4OiBNSVNTSU9OIFNUICYgV0FTSEJVUk4gU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzcyNjFlZDdkOTlmNDQwZDFhYmMyZTUyYmFmNGRlMGFiLnNldENvbnRlbnQoaHRtbF81NDczNzI5NjFiOTM0MDBlOTkyZGQ3OThlN2NkNWNhMyk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzRjMTA2M2ZmNjNjYzQyZmJiOTNlMGJiZWI4OWFiZmQ1LmJpbmRQb3B1cChwb3B1cF83MjYxZWQ3ZDk5ZjQ0MGQxYWJjMmU1MmJhZjRkZTBhYik7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9mODdmMWE3MDRmZGM0ODM5YjEzY2Y4YWI0MDgwNjdmMSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxMDQyNDQsLTEyMi40NDgzMTk3XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYTkxMTU1YWY4MDMxNDQ1MGEzMzg3N2Y0M2M5ZjA5YTEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfNDcxYzYyMWQxMTZmNDYwODg4MWNjMzBhZjc0NzkwNGEgPSAkKCc8ZGl2IGlkPSJodG1sXzQ3MWM2MjFkMTE2ZjQ2MDg4ODFjYzMwYWY3NDc5MDRhIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMDk6IE1JU1NJT04gU1QgJiBNT1JTRSBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYTkxMTU1YWY4MDMxNDQ1MGEzMzg3N2Y0M2M5ZjA5YTEuc2V0Q29udGVudChodG1sXzQ3MWM2MjFkMTE2ZjQ2MDg4ODFjYzMwYWY3NDc5MDRhKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfZjg3ZjFhNzA0ZmRjNDgzOWIxM2NmOGFiNDA4MDY3ZjEuYmluZFBvcHVwKHBvcHVwX2E5MTE1NWFmODAzMTQ0NTBhMzM4NzdmNDNjOWYwOWExKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzI0YjY1NjU4MDFiODRlZjFhNGEzZDA1ZTUzYmE4ZjQ3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzc4MTMsLTEyMi40MTIzMjc4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYzMzMjBjYWJkOTE0NDA1YTgzMWFlYzA3YjdhMTRiMjUgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMjI3NDQ2MDQyMjZiNGUxZTg2MzY2MDg5MDg3YThhZTEgPSAkKCc8ZGl2IGlkPSJodG1sXzIyNzQ0NjA0MjI2YjRlMWU4NjM2NjA4OTA4N2E4YWUxIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMTA6IE1JU1NJT04gU1QgJiBBTkdFTE9TIEFMWTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYzMzMjBjYWJkOTE0NDA1YTgzMWFlYzA3YjdhMTRiMjUuc2V0Q29udGVudChodG1sXzIyNzQ0NjA0MjI2YjRlMWU4NjM2NjA4OTA4N2E4YWUxKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMjRiNjU2NTgwMWI4NGVmMWE0YTNkMDVlNTNiYThmNDcuYmluZFBvcHVwKHBvcHVwX2MzMzIwY2FiZDkxNDQwNWE4MzFhZWMwN2I3YTE0YjI1KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2RjZjZmMTVkZjI0ZTRmNTJhMGMxNzIxNzI4YWUyMmQzID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzQwNjQ4OTAwMDAwMDA0LC0xMjIuNDIzMTA1MjAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF81MjNlNWY0YjAxNTE0NTA5ODU1OTdkNTVlMjBkZjFjOCA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF8wZWY2YWE0ZGI2YjM0YzJlYjAyYzZlMjhhMTg2OWEzNyA9ICQoJzxkaXYgaWQ9Imh0bWxfMGVmNmFhNGRiNmIzNGMyZWIwMmM2ZTI4YTE4NjlhMzciIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjExMTogTUlTU0lPTiBTVCAmIEJST09LIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF81MjNlNWY0YjAxNTE0NTA5ODU1OTdkNTVlMjBkZjFjOC5zZXRDb250ZW50KGh0bWxfMGVmNmFhNGRiNmIzNGMyZWIwMmM2ZTI4YTE4NjlhMzcpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9kY2Y2ZjE1ZGYyNGU0ZjUyYTBjMTcyMTcyOGFlMjJkMy5iaW5kUG9wdXAocG9wdXBfNTIzZTVmNGIwMTUxNDUwOTg1NTk3ZDU1ZTIwZGYxYzgpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZjIxNWRhZDE4MDMwNDBlOGI0MmU1MjFiZjFjNTYxM2IgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NzYwMzEyMDAwMDAwMDYsLTEyMi40MTQ3NDI3MDAwMDAwMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwXzU2ZGY1N2YxMThhMDRjYzE5ZGY3YTIzOGFmMWI2Njg1ID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzY5NjI2OTkzYjk0YTRlZWNiYmVmMWUxOWMxNzhkYzZmID0gJCgnPGRpdiBpZD0iaHRtbF82OTYyNjk5M2I5NGE0ZWVjYmJlZjFlMTljMTc4ZGM2ZiIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTEyOiBNSVNTSU9OIFNUICYgMDlUSCBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfNTZkZjU3ZjExOGEwNGNjMTlkZjdhMjM4YWYxYjY2ODUuc2V0Q29udGVudChodG1sXzY5NjI2OTkzYjk0YTRlZWNiYmVmMWUxOWMxNzhkYzZmKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfZjIxNWRhZDE4MDMwNDBlOGI0MmU1MjFiZjFjNTYxM2IuYmluZFBvcHVwKHBvcHVwXzU2ZGY1N2YxMThhMDRjYzE5ZGY3YTIzOGFmMWI2Njg1KTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2M3ZmRmOTIzZWQ1NjRmMjNiNmJkYmE5YTc5NWU1YzIwID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzEzNjA0Nzk5OTk5OTksLTEyMi40NDM3MzU5MDAwMDAwMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2I5OGMxM2YxZjdhMDQ4MjI5NWIxMjViZDUwMmExYjVmID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sXzEzNTc0M2ZlYjM0ZjQ2ZDdhZDZkMTMyNWFlYzczMmVlID0gJCgnPGRpdiBpZD0iaHRtbF8xMzU3NDNmZWIzNGY0NmQ3YWQ2ZDEzMjVhZWM3MzJlZSIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTEzOiBNSVNTSU9OIFNUICYgT1RUQVdBIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYjk4YzEzZjFmN2EwNDgyMjk1YjEyNWJkNTAyYTFiNWYuc2V0Q29udGVudChodG1sXzEzNTc0M2ZlYjM0ZjQ2ZDdhZDZkMTMyNWFlYzczMmVlKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfYzdmZGY5MjNlZDU2NGYyM2I2YmRiYTlhNzk1ZTVjMjAuYmluZFBvcHVwKHBvcHVwX2I5OGMxM2YxZjdhMDQ4MjI5NWIxMjViZDUwMmExYjVmKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2U2YmUxNjljZTcxMzRlMGJhMzYxZDFiODQyMzc3MjhkID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzE1NTg2NiwtMTIyLjQ0MTcyNzQ5OTk5OTk4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfMTMzZTNlNGUyMGViNGJiNTgyM2MyYjJmZmIzYjJiYTEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMjVkZjFhY2RlZmU4NDNkOWEyN2YyNDNjMTc0MTFkMTcgPSAkKCc8ZGl2IGlkPSJodG1sXzI1ZGYxYWNkZWZlODQzZDlhMjdmMjQzYzE3NDExZDE3IiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMTQ6IE1JU1NJT04gU1QgJiBOSUFHQVJBIEFWRTwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfMTMzZTNlNGUyMGViNGJiNTgyM2MyYjJmZmIzYjJiYTEuc2V0Q29udGVudChodG1sXzI1ZGYxYWNkZWZlODQzZDlhMjdmMjQzYzE3NDExZDE3KTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfZTZiZTE2OWNlNzEzNGUwYmEzNjFkMWI4NDIzNzcyOGQuYmluZFBvcHVwKHBvcHVwXzEzM2UzZTRlMjBlYjRiYjU4MjNjMmIyZmZiM2IyYmExKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzAyNjQ0YzAzNjRjOTRkMTNhZmI1OTJhNTRhNGQwYzczID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzA4Nzg1MSwtMTIyLjQ1Mjc2OTQwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYmZhZWEyYTJhZDg3NGViM2I3YzQ3NzgyZjRlMzIxNzEgPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfODk4N2FiNzZjNGZkNDkxOWEwMmM0NDk0MTk4NWZkY2EgPSAkKCc8ZGl2IGlkPSJodG1sXzg5ODdhYjc2YzRmZDQ5MTlhMDJjNDQ5NDE5ODVmZGNhIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMTU6IE1JU1NJT04gU1QgJiBBQ1RPTiBTVDwvZGl2PicpWzBdOwogICAgICAgICAgICAgICAgcG9wdXBfYmZhZWEyYTJhZDg3NGViM2I3YzQ3NzgyZjRlMzIxNzEuc2V0Q29udGVudChodG1sXzg5ODdhYjc2YzRmZDQ5MTlhMDJjNDQ5NDE5ODVmZGNhKTsKICAgICAgICAgICAgCgogICAgICAgICAgICBtYXJrZXJfMDI2NDRjMDM2NGM5NGQxM2FmYjU5MmE1NGE0ZDBjNzMuYmluZFBvcHVwKHBvcHVwX2JmYWVhMmEyYWQ4NzRlYjNiN2M0Nzc4MmY0ZTMyMTcxKTsKCiAgICAgICAgICAgIAogICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzY1ZjI3NmRiZmNkNjQ2M2JiM2Q2NjM5M2ExNzYyYWJmID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzgyODIzNSwtMTIyLjQwNjc1ODhdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCiAgICAgICAgICAgIHZhciBwb3B1cF8wNzQ3NDA2YWE0ZDA0MDQ1YTNkMDhkMzZmMjhhYWJlYSA9IEwucG9wdXAoe21heFdpZHRoOiAnMzAwJ30pOwoKICAgICAgICAgICAgCiAgICAgICAgICAgICAgICB2YXIgaHRtbF83OTk4MjMzZTJmZDY0MDY0YTRhNDhmNjNlMDY1MDhlOCA9ICQoJzxkaXYgaWQ9Imh0bWxfNzk5ODIzM2UyZmQ2NDA2NGE0YTQ4ZjYzZTA2NTA4ZTgiIHN0eWxlPSJ3aWR0aDogMTAwLjAlOyBoZWlnaHQ6IDEwMC4wJTsiPjExNjogTUlTU0lPTiBTVCAmIDA1VEggU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwXzA3NDc0MDZhYTRkMDQwNDVhM2QwOGQzNmYyOGFhYmVhLnNldENvbnRlbnQoaHRtbF83OTk4MjMzZTJmZDY0MDY0YTRhNDhmNjNlMDY1MDhlOCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyXzY1ZjI3NmRiZmNkNjQ2M2JiM2Q2NjM5M2ExNzYyYWJmLmJpbmRQb3B1cChwb3B1cF8wNzQ3NDA2YWE0ZDA0MDQ1YTNkMDhkMzZmMjhhYWJlYSk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lMzc3YjE1MDA3NzY0NTRkYjE1ZDAwMTk4NTgzZjNlYSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc1MjI0NTgsLTEyMi40MTg0NjIyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAogICAgICAgICAgICB2YXIgcG9wdXBfYzJiOTBmNDhhNjQ2NDI3N2EzNWMxY2EwZTE5MmMwODggPSBMLnBvcHVwKHttYXhXaWR0aDogJzMwMCd9KTsKCiAgICAgICAgICAgIAogICAgICAgICAgICAgICAgdmFyIGh0bWxfMGFhNjIzNzU1ZGQ2NGJmM2ExM2Y4NjJjOGNjYWFlMGUgPSAkKCc8ZGl2IGlkPSJodG1sXzBhYTYyMzc1NWRkNjRiZjNhMTNmODYyYzhjY2FhZTBlIiBzdHlsZT0id2lkdGg6IDEwMC4wJTsgaGVpZ2h0OiAxMDAuMCU7Ij4xMTc6IE1JU1NJT04gU1QgJiAyNFRIIFNUPC9kaXY+JylbMF07CiAgICAgICAgICAgICAgICBwb3B1cF9jMmI5MGY0OGE2NDY0Mjc3YTM1YzFjYTBlMTkyYzA4OC5zZXRDb250ZW50KGh0bWxfMGFhNjIzNzU1ZGQ2NGJmM2ExM2Y4NjJjOGNjYWFlMGUpOwogICAgICAgICAgICAKCiAgICAgICAgICAgIG1hcmtlcl9lMzc3YjE1MDA3NzY0NTRkYjE1ZDAwMTk4NTgzZjNlYS5iaW5kUG9wdXAocG9wdXBfYzJiOTBmNDhhNjQ2NDI3N2EzNWMxY2EwZTE5MmMwODgpOwoKICAgICAgICAgICAgCiAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYWJhZTE5ZDA5MDZmNDAxN2E2MDVlYTljMmU5YjJiODkgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NzU2MDk5LC0xMjIuNDE1NTE3OF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKICAgICAgICAgICAgdmFyIHBvcHVwX2U0MDFkOGRkMDE5MjRiMDJiZDIwNGRhNzhkZjdhZWJkID0gTC5wb3B1cCh7bWF4V2lkdGg6ICczMDAnfSk7CgogICAgICAgICAgICAKICAgICAgICAgICAgICAgIHZhciBodG1sX2U0MzQxODhlZDEwZjRjYTZiYTIzODhjN2Q5OWQ3NDM4ID0gJCgnPGRpdiBpZD0iaHRtbF9lNDM0MTg4ZWQxMGY0Y2E2YmEyMzg4YzdkOTlkNzQzOCIgc3R5bGU9IndpZHRoOiAxMDAuMCU7IGhlaWdodDogMTAwLjAlOyI+MTE4OiBNSVNTSU9OIFNUICYgR1JBQ0UgU1Q8L2Rpdj4nKVswXTsKICAgICAgICAgICAgICAgIHBvcHVwX2U0MDFkOGRkMDE5MjRiMDJiZDIwNGRhNzhkZjdhZWJkLnNldENvbnRlbnQoaHRtbF9lNDM0MTg4ZWQxMGY0Y2E2YmEyMzg4YzdkOTlkNzQzOCk7CiAgICAgICAgICAgIAoKICAgICAgICAgICAgbWFya2VyX2FiYWUxOWQwOTA2ZjQwMTdhNjA1ZWE5YzJlOWIyYjg5LmJpbmRQb3B1cChwb3B1cF9lNDAxZDhkZDAxOTI0YjAyYmQyMDRkYTc4ZGY3YWViZCk7CgogICAgICAgICAgICAKICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8xZDIyZWVlYzUwNWE0MzY1YjA5MDZhOWEyMGYyM2E1MCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxODQ3Nzg5OTk5OTk5NiwtMTIyLjQzOTUzNTU5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl80MzY0ODY2YWRlMjM0YTg5OTY4NDVjNGY1NDczMmFhMCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc4NzQ1NjEsLTEyMi40MDA1MjM0MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNDI3M2ZjODA2YmMwNDM3MDliMmRmZDc5ZmRhMTQ4Y2UgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODk5NTQzLC0xMjIuMzk3NTE0MDk5OTk5OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzZlNGMwMTZlNDMyYjRmM2U5MGJkNDY3OTE1YWFhZjhjID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzU4ODQyOSwtMTIyLjQxODk5ODNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzZhODc4YjQxZDU0ZjRiMDg5MjJjNDlmMDk3NDk1ZGMxID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzkwNDU0NzAwMDAwMDA1LC0xMjIuMzk2NzI2Mzk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzg1OGM2YzI0NDYwZjQ2NjBhZmQ2YzdiYmFhMzZkZjRlID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzY5OTUzNiwtMTIyLjQxOTk2MjNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzQzZGIwZjU2OTEyYzQ0OTM5NzFjMTJlMjdjNTZiZWEwID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzY5MDYzMSwtMTIyLjQyMDA3MjNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzE4MjEwODUzODFmNDQzMWU4MTA4NWE2MTlhNmFjNTg5ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzkxMTU5MiwtMTIyLjM5NTgyNjIwMDAwMDAyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wMjQ3NTg4ZTAyMWQ0N2NhODY3MmNlYzMzYmRjNzA3OCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0NTYwMzQ5OTk5OTk5NCwtMTIyLjQxOTg5NzVdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzkzMzQ1NGZiMmUyODRmZTViMGFmYWNkYTY4ZTVmMTQ2ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzMzOTUzMTk5OTk5OTksLTEyMi40MjYxNDIwOTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZWUxMDljNDY5ZTk3NDFjY2E3NWQ0MWM1ODBlMzFhZmYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTc2NzU0LC0xMjIuNDQwMTQ5MjAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzIzZGJjMjMxY2VmNTQ2M2ZhOTVlYzQ4MjQ1NDU4YmE4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzg4Mzc0NCwtMTIyLjM5OTM2MjJdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzdlYzdlYzcwYmIwZjQ2MDBiMTVhOTgwNmIzOTk1NzI4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzgzMDkwNCwtMTIyLjQwNjAzNzgwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8zYzA2MmY1NzE0M2U0OTk4OTlkMDE5N2ExZjBkMGYxYSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyODc0ODUsLTEyMi40MzEzMTk3OTk5OTk5OF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMThiNmVlMzZmMjZkNDljNDhiNzg5ZmE1ODdiZGU0ZGEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTQ2MDY1LC0xMjIuNDQyNjY5N10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZTU3OWUxM2U1YTE5NDEyMzg4ZTIxZWQ0YmQ1ZDY3NTAgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDIzODY5LC0xMjIuNDIxOTgyMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZDhmOWRmN2RiYjU0NDkxNmEwODk5MTEwZTU4NDM2MTQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43OTE4NDg0LC0xMjIuMzk0OTU1M10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNjNkZTVmMjIyMmIyNDgyYzliOWIyZjQ0NDAxOWExODQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzA3NTgyMDAwMDAwMDQsLTEyMi40MjkyNDE5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9iYzllMzg5YzUzYjk0N2I1YWJhMTIxNmM0YzMyMTVmMCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc2NzEzNzc5OTk5OTk5LC0xMjIuNDIwNjAzMjAwMDAwMDJdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzg1NWQzZTc5NTE1MjRkZWNiM2I3NjI2MzkxMjg2ZGU3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzQxNzUwNTk5OTk5OTk2LC0xMjIuNDIyMzkxNzAwMDAwMDJdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzNmMTU2YjIwYzkwNzRhMTViNGFhOGRjOTk3ZGVhZGExID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI4MjUxOSwtMTIyLjQzMTgwNjAwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lMGJlNGY2YzhhNDM0YjAzYTI2MWM5NzAwYTc3Nzg5OCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyNzcxNjYsLTEyMi40MzI0MDM3OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNzg0ZTg0ZGIzZmQzNGZkNGI1YjIzNzQyOGQ1OGEwOGEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzY3NDk2LC0xMjIuNDI0MjQ4NDAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2E0MzcxNzBlYmFkNTRiMDA4NzNlZWFjMmZkZDhmMTY3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzA5NTU2MiwtMTIyLjQ1MDUxOTNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzA3OTNhZWU2Y2NmYjRiMTliMWE2MGM3N2NkNjFkYzFjID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzkzMjMxNzAwMDAwMDEsLTEyMi4zOTMyMDQ3XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9jNzYxNzNkZmJjNDQ0MWMyYWExZmJhMmIwMmIyM2MxNiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcwODMwNTIwMDAwMDAwNSwtMTIyLjQ1NDE5MzZdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzFkYWQ5ODdlYWIwODRlYzFhMjkyYzVjYmVhODc2MjE0ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzYyNzEzODk5OTk5OTk0LC0xMjIuNDE5NDcwNV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfY2RlNDZiNmQ4YTJhNDRjMThmYTc2ZDFhZjdhZDA0ZDMgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODc4MzU3OTk5OTk5OTYsLTEyMi40MDAyMDEzXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wY2RhZTJjODk5NWY0ZDYxOWE3NGZkNmU2YzVhMzY2MCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc4MjIxMTcwMDAwMDAwNSwtMTIyLjQwNzE2NTVdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzEyNDlmNDQ1MGRmZDRmMjNhMDg3MDcxMWNjZDk3YTc1ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzE2NDA3Mzk5OTk5OTk0LC0xMjIuNDQxMDg2NTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzgzZTRmZDU3YzYyYjQ0NzI5NjBmNDhlMWU3N2ZlMWQ3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzQxMDM3NSwtMTIyLjQyMjg1MzFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzk5YTg5ZTk5MzE2ZjRhMDM5OWM0NzMxM2ZjNTQ1OTRhID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzU1NDQ0NzAwMDAwMDA1LC0xMjIuNDE4Nzc2MDk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzQ0YTY1ZTRjMjI1ODRmNTViMWJiYTQ1NWIzZWIwMDU1ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzgwNzk1NiwtMTIyLjQwOTExMzQwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9mYmM5YThhNDlmNjA0NTAxYTBhYjJkZjMzMGFkM2M0ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0Njc4MTQsLTEyMi40MTkxNDE3MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMzg5NjU1Njg2MGZmNGVjNGFiOWM5NzJhMjU4OWQ1ZGYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NjM0NDQ2LC0xMjIuNDE5NTQyMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfY2MzM2NkMDJmYzE4NGZkZWI3Njg4MTBkYjU1YzEwNzggPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjkyMzA3LC0xMjIuNDMwODQ0N10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMDA5NTUzYzI1NzBlNDQwMmFiMmMyNjdlNjQ0MmJiZTUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTQ0OCwtMTIyLjQ0MjY0XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl85MzliYzM4YmQ2ZTI0NTY4YjJiM2MzOWVmNDhjOWJkMCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0OTA0NCwtMTIyLjQxODE2NTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzYyYzlhMDBhMzA5NDQwZjk5ZThjODQxM2I3NzE5YWVkID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzQzMTM5LC0xMjIuNDIxNDk3MV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZTMzZTI4NzY2ODBlNDZkNDhhNWZkNTQyYTEyNmQ4YmMgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjcxODIyLC0xMjIuNDMyOTM1Mjk5OTk5OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzhhZDg2MTBkMGFkZDRlNjdhODAwNDMwZjdmZTNjNDA3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzEwNzUzNCwtMTIyLjQ0NzcwNzRdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2IzYTBmNTlkOWQ3YTRhNjU5NWRkYjgwOGJkNDIwNGYxID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzc3MzI5MzAwMDAwMDEsLTEyMi40MTM1MDM3XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl83NDBlMDg1MWRkOTM0MzRmOTkxYTRhZDg4OGFkMmE2MSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3MTAxLC0xMjIuNDE5NjYzNF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNGZjNmM0ZmU3OTE1NDEyNmI4NTRhYTY5ZmViYmIwNjUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43OTM4NDMzLC0xMjIuMzkyNDQxOV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfN2RmZTViODU2NjMwNGY2YmI1NjI5ODVhMDNkYzUyN2IgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NjQ3OTYyMDAwMDAwMDYsLTEyMi40MTk5ODg5OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNDhhOGQyOGZiZmNhNDJiZGEwNzc0NDNiNjRiYjM4NGEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43Mzg4NDM1LC0xMjIuNDIzOTc5MV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfOGM3OTFmZGMzOTZiNDQ2ZDljNjJiNTYyYTMzNDZkYjEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjM5MDIsLTEyMi40MzU0MDIyMDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNmI0NjI2MzQ0YjNmNGQ1OTliMDJlMWZkYzM1Nzk3YjEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NDI4MzEyMDAwMDAwMDUsLTEyMi40MjE2OTU0MDAwMDAwMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMzU4Y2FkNGM0OWQ1NDY4Njg1ZWYwNTEwYzQ1MTkzOTggPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTU0NDY0LC0xMjIuNDQxODMzNl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfOGI0MGRiMDI2Y2MxNGUyY2E2OWI1OGM3MTUwNjc4MDYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MDk5NTkwMDAwMDAwMDUsLTEyMi40NDkzNTRdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzg5Y2IxZTc0MzU5YTQ0NmI5NmUzYTllOGE2MDRlZTM0ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzc0MzMyNSwtMTIyLjQxNzEzNzJdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzU3NDkxMjAyYmY1ZDQ3MmE4Y2VlZWFjN2YwODVhNmE4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzQyMjA3NCwtMTIyLjQyMjA5NzIwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8zY2U5ZDM0YTY1ZDI0ZGU1YWNmOGIxODk2OGUwZWU2NCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc4NDY0LC0xMjIuNDAzOTNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzhkYjBlZTJmOTkwYjRkYTliYmRkYTg5N2E3NTkyM2E4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzIwMDM2NywtMTIyLjQzODM1MDQ5OTk5OTk4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lOGNmNTVmYzA2NjU0MDZjOGU4MDUzNDY4OGE2ODg1NyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3NjcxMzQsLTEyMi40MTQxMjM5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl85NTNlMjY2ZmVhZmY0Y2YxOWU2NjNlMjRhMGQ2ZjU2YiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0NDAzOCwtMTIyLjQyMDkxNTA5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl85NWE2MDcxNzQxNTY0N2RhYjNhMmEwNGNkZWJkNDlhYyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyMTU4MDgsLTEyMi40MzcxNzY1OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfY2JiNTNlZjc0Mjc0NGIxODhhOWM2MDgzY2I0NTZmNjYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjExNjk1OTk5OTk5OTYsLTEyMi40Mzc0ODg5OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNjNiMmFmYTE4MmI4NDM2YmFkOTM4Njk0ZGZkZjg2YzYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODY4MzkyLC0xMjIuNDAxMjc4NDk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzNiNTA1ZjQ0OGM4ZDQxYWU4Zjk2ZTNjZjY4NGM1YTAyID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzU2Nzk4NzAwMDAwMDA0LC0xMjIuNDIwMzQ0Nzk5OTk5OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzliNDk1YjM4M2YxOTRjYjc5NWM3MTg1MzA2OTlhNzYxID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzYzMDYzNzAwMDAwMDA0LC0xMjIuNDE5NTA0NF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNTA2Mzk4MWZiMTkwNDFhNGFlMDE1NWY1N2Y0MTJiZjAgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTE2MDU2MDAwMDAwMDYsLTEyMi40NDYxMTQ5OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNzBiMjllYTgzZWYxNGE1MWE5NGYxMTU5ZDVmYTIyZDcgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43Mjk4ODMsLTEyMi40MzAyNDIyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wMjk2Y2Q5MjM3MjE0MDc4YTA4MzQ4ZGQ0YmZlMTEyNSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczNTYyMDAwMDAwMDAwNCwtMTIyLjQyNDddLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzY0YzRhYzg5NWYxMzRlZDBhZWUxMjU3MzMxYjA0MjRlID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzExNDY3OTk5OTk5OTk2LC0xMjIuNDQ2MjkwOTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzAyZTg1YmMyMzBjYTQ0YjY4NzBiZjc3N2NmYWRjNjk3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI0MDI3ODk5OTk5OTk2LC0xMjIuNDM1MzI5NTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzY3M2YyMDZlM2ZiYjQ4MzBiNjUwZWI2MDE1YzFlMTQxID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzY4Mjg0MjAwMDAwMDA0LC0xMjIuNDIwMDA3NF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZmNjZjkxODMxYTJlNGQ0MzhmMmIwYmRlYjIyYTk3OWQgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MTM5NTI4OTk5OTk5OTUsLTEyMi40NDMzNzUyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9kNGM3MTUxNmZlYzI0ZmNiYWQ0NjMxNWYwZTljZTc0YSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczNzM2MTIsLTEyMi40MjQwNDg0MDAwMDAwMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfODExZTExZGY5ZTNkNDE5Zjg5Zjk3MjI5ZWUwMDBiNDIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NzUyODM4LC0xMjIuNDE1OTM3MTAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzI5MGYyZTNjNDk5ZTQ2OGRiY2M5NjAwNmU5ZThhNGI5ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzA5NDIxMSwtMTIyLjQ1MDkxMjk5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8xNjI2Y2EyODMwZGE0OGRkOGNkMGY5YTYzYTBmOWY3ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxNzI3NDEwMDAwMDAwNCwtMTIyLjQ0MDQ1Mjc5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl81ODNlODc4NzdkYWE0MDgwODA5MDI3ZDk3M2RiZmRmOSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc4OTE3OSwtMTIyLjM5ODMzNzldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzc1YTdhMzBkZGNkYzQ1Y2I4NDA3ZGM3MTkzMTA5ZTU1ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzg4OTg2NSwtMTIyLjM5ODU4NjFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzJiYjNiOThjZWU3NzQwNTM4ZjNiZjBhZDVkMWQ4MmQyID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI3NjU2LC0xMjIuNDMyNDcyNjk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2I0MGJlODlkNGEyNDRmNTRiMTViZmNkYWE5NDNlZTdmID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI2NTk3MiwtMTIyLjQzMzM4NTEwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9hNDRhYmM4ZTY4MzA0ZDE1OWMyOTNlNTM5YTI0MTIxZSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3MzQ4ODYsLTEyMi40MTgwNTIxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl82Yjg1NTA3ODlhNGM0MWIyOTllZWJjZTI3YmZjOGU2NiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc4MzA5MDQsLTEyMi40MDYwMzc4MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNWQyOGQ3NDIwMjViNGE3MWI5NjRkYmI0ZjBmYjU0OTYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzUyNDYzLC0xMjIuNDI0ODYwOF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNTU5M2NjOGE2MTY5NDM0Y2FjM2U2ZGI5YzA2M2Q5YmUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODUyODgxLC0xMjIuNDAzNDE4NF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMTFjMmE3M2I3MmFjNDc5ZGI5OTI3MzAwOTU0MzM5M2QgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjUxNzUyLC0xMjIuNDM0NDYxNV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMjEwYzU0ZDRiYzJhNDIyMzhjM2UxMTM3MjczYWM0ZmYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzQyOTUzLC0xMjIuNDI1ODEyOTk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzhiNjNjMmFiZmFjNDRkMzk4YjhiYjk3MmIyMjYxYjg3ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzQ1NjAzNDk5OTk5OTk0LC0xMjIuNDE5ODk3NV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYWIxMDE4NGRlNWZiNDFhYmEwNmIzMTNkODg2ZWZhYjkgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzAwMDk3MDAwMDAwMDQsLTEyMi40MzAxMTYwOTk5OTk5OF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMjVkOWI3YTA2Y2ZlNGFiYTgyYjgyMjk4YzA4ODhlZDYgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NzMwNjcyLC0xMjIuNDE4NzIwMDk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2NhMDE2ZDc0NWQ0YjQzMDNiNzFhYTUyNGZkNzk1YTVlID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzUwNjQ4MiwtMTIyLjQxODMxNTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzQ1YTVmYmMwZjg5ODQ2Yjk4YWMzY2Y3MGVmYTc5YWEyID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzgyMTI1NywtMTIyLjQwNzI3Mjc5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9hOGJkMWE3MGE0NzI0MzBjOGVjYzU5NWExZjkyYWU5NSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc5MjUyOTcsLTEyMi4zOTQwOTA1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl81NGMyNzQ2OGEwMDQ0Y2ZjODRlZmI4ZDQxMmViMjFiMSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxNTc2NzQsLTEyMi40NDE1ODM1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8wZDE5NjcxYmJiNjE0ZDMxOGNiMmI0NDQ5NDdlODBhMyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3ODk5MDUwMDAwMDAwNiwtMTIyLjQxMjIwMjU5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl84ZWUwMGE3ZGM2NjQ0NjJjYmYxY2EyMzgyY2VmOGQyMiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyNDMwOTk5OTk5OTk5NiwtMTIyLjQzNTE4OTk5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9kNTEzZTY1MzE3MTc0NDk5YTQzYTVkOWE2MGEyNDI5ZiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyMzA0OTgsLTEyMi40MzYwNjgwOTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfZWY1NDg0NDQxYjI5NDA3NThjMTY2ODQ2M2NhNjYwNDEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjQ2ODc4OTk5OTk5OSwtMTIyLjQzNDgyNDNdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzkxZmM2YTAwYWY5NjQwMDRhZTY2ZTcyZmQ5MDZkYzYzID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzEyNzgyNiwtMTIyLjQ0NDYwMTU5OTk5OTk4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9mZmJkODcyMmM3NDc0Mjk0ODhhNGRlMWVlY2JmNmNlZCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc1Mzg0NTUwMDAwMDAwNCwtMTIyLjQxODYxOTQwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8xMTBlZDllMWE1ZWM0MjM2YWI5ZDYxNmJjMTA1YTNkNCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjczOTc3OTI5OTk5OTk5NSwtMTIyLjQyMzY3NTQ5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl81Mjc0YjNkNGYyODA0ODY5YmEzMjhjN2Y3NDQzN2E3OCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyMzE1Mjg5OTk5OTk5NSwtMTIyLjQzNjAwMTc5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl82MWFkYmNmMTRiNzQ0MzAxOWRhOGZmZTUzNjNkYWYyOCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc2NTA1NjYsLTEyMi40MTk2OThdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2MyZmYwN2Y4NGI4MjQ4NjU5N2UxMWMxMDNhOTQxZTdjID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzg1MTM4MjAwMDAwMDA2LC0xMjIuNDAzNTA1Nzk5OTk5OTldLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzMxODRkMGY2YTkzYzRkMTg5NWE0OWViYzFhM2JkYTE2ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzA5Njg4LC0xMjIuNDUwMTI0M10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYjBkZGFhNjAyYWJhNGFlZjg5ZDcwMTBlNTgxZTE2ODEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NjE4NzY5LC0xMjIuNDE5Nzk3M10sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYWJhMTZjNjE0ODU2NDFjNTllOTcxZTNlMmQyZjc0OTAgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MjI0NjM3MDAwMDAwMDYsLTEyMi40MzY1MDk1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8zNjlhMDU0OWQ2NDM0YmI3YjgxNDBjZmE4YzNiYjVlMSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0NjE4NTQ5OTk5OTk5NiwtMTIyLjQxOTUyNDA5OTk5OTk5XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl81ZmFiZmZiZmFlYmU0YTU3OTY0YjczNTAzMDM2NTJlYSA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcyNTc3MzQsLTEyMi40MzQwMDM4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl85YzY1NzZkNjk5MTg0ZGMzYWUyNGM3ODhhMWU4OGY1NyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxMzM4NTcsLTEyMi40NDM5NjU1XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9jMGM0OTk4ZDNmMWY0Mjk1ODIwYjQyYzY2NTFiNTEyYiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxMjYzMzksLTEyMi40NDQ3Njk5OTk5OTk5OV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMWMzNzhhMTdmZWY2NGI2YmI5NDA5ODk1ZDM2ZGI2NjIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43MzM2ODQ2MDAwMDAwMDQsLTEyMi40MjYzOTZdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2ZiNzkxZTljNzE0MjRlYmM4ZWUxMjdiNmRlZWU1Nzk4ID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzI2NDAzNDk5OTk5OTk2LC0xMjIuNDMzNDExOF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfYjY4OTNiMGJlNGVjNDQ3OTg5MGVjZTAzZThhOTYyMzUgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NzU5LC0xMjIuNDE1MTUyOTAwMDAwMDFdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzA1YmY1YjUwZTVhNDRmNmQ4ZTgzYTBkMjllNjJkNzFjID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzEwNDI0NCwtMTIyLjQ0ODMxOTddLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyX2M0YTBmN2FkYTdlZDQxY2Y5YTRiODIzMGViZWZlMjUxID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzc4MTMsLTEyMi40MTIzMjc4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl8zMzExNGZmZjg5NWE0MjQxYjQzMTUwMTdmNmM2YjcxOCA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc0MDY0ODkwMDAwMDAwNCwtMTIyLjQyMzEwNTIwMDAwMDAxXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl82OTVmZTBkZDYyOTk0OGRiOGQ4M2JmMmI5ZjIwZGE0MiA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3Ljc3NjAzMTIwMDAwMDAwNiwtMTIyLjQxNDc0MjcwMDAwMDAyXSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl9lYmFmMWYwZDdmYWU0NDE5ODJiNDQxY2ExOTIyN2E3MyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcxMzYwNDc5OTk5OTk5LC0xMjIuNDQzNzM1OTAwMDAwMDJdLAogICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBMLkljb24uRGVmYXVsdCgpCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgKQogICAgICAgICAgICAgICAgLmFkZFRvKG1hcF85MjIwNzA1NzI4MTI0MWU4OTNmMjRjZDc5ZDk4NDYyOSk7CiAgICAgICAgICAgIAogICAgCgogICAgICAgICAgICB2YXIgbWFya2VyXzA5YzE5ZTkxNzI1YzQ1OTc4M2U1Yzc3YzQyOWMyYzFhID0gTC5tYXJrZXIoCiAgICAgICAgICAgICAgICBbMzcuNzE1NTg2NiwtMTIyLjQ0MTcyNzQ5OTk5OTk4XSwKICAgICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgTC5JY29uLkRlZmF1bHQoKQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgICkKICAgICAgICAgICAgICAgIC5hZGRUbyhtYXBfOTIyMDcwNTcyODEyNDFlODkzZjI0Y2Q3OWQ5ODQ2MjkpOwogICAgICAgICAgICAKICAgIAoKICAgICAgICAgICAgdmFyIG1hcmtlcl81NDg3NmYxMTIxYzM0MzUyOTA0ZDEyODk3ZjhlYzIzNyA9IEwubWFya2VyKAogICAgICAgICAgICAgICAgWzM3LjcwODc4NTEsLTEyMi40NTI3Njk0MDAwMDAwMV0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfNzQ2ODM5N2NjNTRiNGI0MGExZTkzNmNhZmIyODE0OTkgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43ODI4MjM1LC0xMjIuNDA2NzU4OF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMDNlYWYyYjQ5YTZmNDMwMzlhY2U3NDY0MTdiYTBhYmIgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NTIyNDU4LC0xMjIuNDE4NDYyMl0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCiAgICAKCiAgICAgICAgICAgIHZhciBtYXJrZXJfMDdiNjMxOWM5MDNmNGJlZGEyMzJmZWViNTZjMDU4OTEgPSBMLm1hcmtlcigKICAgICAgICAgICAgICAgIFszNy43NzU2MDk5LC0xMjIuNDE1NTE3OF0sCiAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgaWNvbjogbmV3IEwuSWNvbi5EZWZhdWx0KCkKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICApCiAgICAgICAgICAgICAgICAuYWRkVG8obWFwXzkyMjA3MDU3MjgxMjQxZTg5M2YyNGNkNzlkOTg0NjI5KTsKICAgICAgICAgICAgCjwvc2NyaXB0Pg==" style="position:absolute;width:100%;height:100%;left:0;top:0;border:none !important;" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe></div></div>




```python
Employee_Addresses = pd.read_csv("Employee_Addresses.csv")
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-1-1ce72cdb118f> in <module>()
    ----> 1 Employee_Addresses = pd.read_csv("Employee_Addresses.csv")
    

    NameError: name 'pd' is not defined



```python
Employee_Addresses.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 2191 entries, 0 to 2190
    Data columns (total 2 columns):
    address        2191 non-null object
    employee_id    2191 non-null int64
    dtypes: int64(1), object(1)
    memory usage: 34.3+ KB
    


```python
Employee_Addresses.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>address</th>
      <th>employee_id</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>98 Edinburgh St, San Francisco, CA 94112, USA</td>
      <td>206</td>
    </tr>
    <tr>
      <th>1</th>
      <td>237 Accacia St, Daly City, CA 94014, USA</td>
      <td>2081</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1835 Folsom St, San Francisco, CA 94103, USA</td>
      <td>178</td>
    </tr>
    <tr>
      <th>3</th>
      <td>170 Cambridge St, San Francisco, CA 94134, USA</td>
      <td>50</td>
    </tr>
    <tr>
      <th>4</th>
      <td>16 Roanoke St, San Francisco, CA 94131, USA</td>
      <td>1863</td>
    </tr>
  </tbody>
</table>
</div>




```python
#Slicing the Employee_Addresses dataset into 4 smaller datasets to process fast
Address_set1 = Employee_Addresses.iloc[0:500]
Address_set2 = Employee_Addresses.iloc[501:1000]
Address_set3 = Employee_Addresses.iloc[1001:1500]
Address_set4 = Employee_Addresses.iloc[1501:len(Employee_Addresses)]

```


```python
#Find the coordinates of set 1
loc_latitude = []
loc_longtitude = []
loc_address = []

for address in Address_set1.address:
    try:
        inputAddress = address
        location = g.geocode(inputAddress, timeout=15)
        loc_latitude.append(location.latitude)
        loc_longtitude.append(location.longitude)
        loc_address.append(inputAddress)
    except:
        loc_latitude.append('None')
        loc_longtitude.append('None')
        loc_address.append(inputAddress)


Address_set_1 = pd.DataFrame({'address':loc_address, 'employee_id':employee_id,
                             'latitude':loc_latitude, 'longtitude':loc_longtitude})
```


```python
#Find the coordinates of set 2
loc_latitude = []
loc_longtitude = []
loc_address = []

for address in Address_set2.address:
    try:
        inputAddress = address
        location = g.geocode(inputAddress, timeout=15)
        loc_latitude.append(location.latitude)
        loc_longtitude.append(location.longitude)
        loc_address.append(inputAddress)
    except:
        loc_latitude.append('None')
        loc_longtitude.append('None')
        loc_address.append(inputAddress)


Address_set_2 = pd.DataFrame({'address':loc_address, 'employee_id':employee_id,
                             'latitude':loc_latitude, 'longtitude':loc_longtitude})
```


```python
#Find the coordinates of set 3
loc_latitude = []
loc_longtitude = []
loc_address = []

for address in Address_set3.address:
    try:
        inputAddress = address
        location = g.geocode(inputAddress, timeout=15)
        loc_latitude.append(location.latitude)
        loc_longtitude.append(location.longitude)
        loc_address.append(inputAddress)
    except:
        loc_latitude.append('None')
        loc_longtitude.append('None')
        loc_address.append(inputAddress)


Address_set_3 = pd.DataFrame({'address':loc_address, 'employee_id':employee_id,
                             'latitude':loc_latitude, 'longtitude':loc_longtitude})
```


```python
#Find the coordinates of set 4
loc_latitude = []
loc_longtitude = []
loc_address = []

for address in Address_set4.address:
    try:
        inputAddress = address
        location = g.geocode(inputAddress, timeout=15)
        loc_latitude.append(location.latitude)
        loc_longtitude.append(location.longitude)
        loc_address.append(inputAddress)
    except:
        loc_latitude.append('None')
        loc_longtitude.append('None')
        loc_address.append(inputAddress)


Address_set_4 = pd.DataFrame({'address':loc_address, 'employee_id':employee_id,
                             'latitude':loc_latitude, 'longtitude':loc_longtitude})
```


```python
Address_list = [Address_set_1,Address_set_2,Address_set_3,Address_set_4]
```


```python
#combine all the subsets into one complete dataset
Employee_Address_whole_set = pd.concat(Address_list)
```


```python
#Add the employee_address to the bus stop map
for index, row in Employee_Address_whole_set.iterrows():
    folium.Marker([row['latitude'], row['longtitude']].values.tolist(),
                    icon=folium.Icon(color='green')).add_to(bus_stop_map)  
   
```
